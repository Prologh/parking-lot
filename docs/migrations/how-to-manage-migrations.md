# How to manage EF Core migrations from CLI

Database context for this application is stored in `Data` project, but database configuration code **is not**. It is located in the main web application project called `WebApp` in `Startup.cs` file. In order to use EF Core migration tool, you'll need to connect these two locations and tell the tool about it:

 1. Proceed to the `Data` project directory;
 2. Run migration based command with option `-s` standing for **startup project** like below:

```bash
dotnet ef migrations list -s "../WebApp"
```
