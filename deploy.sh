#!/bin/bash

host=$1
username=$2
password=$3
local_directory=$4
destination_directory=$5

echo "Updating packages."
apt-get update --quiet
echo "Packages successfully updated."

echo "Installing lftp."
apt-get install lftp --yes --quiet
echo "lftp successfully installed."

echo "Executing deploy script."
echo "Connecting to $host."



lftp $host << EOF

echo "Connected."
echo "Authenticating as $username."
user $username $password

echo "Proceeding to $destination_directory."
cd $destination_directory

echo "Moving App_Offline.htm to the web application root directory."
put $local_directory/wwwroot/App_Offline.htm

echo "Waiting 15 seconds for any running processes to shut down."
sleep 15s

echo "Uploading new files and directories from $local_directory to $destination_directory."
mirror --reverse --delete --exclude App_Offline.htm $local_directory .
echo "Uploading completed."

echo "Removing App_Offline.htm from the web application root directory."
rm App_Offline.htm

echo "Closing connection."
exit

EOF



echo "Connection closed."
echo "Successfully executed deploy script."
