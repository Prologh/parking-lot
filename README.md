# Parking Lot

README / 23 November 2024

[Official Website](http://parking-lot.azurewebsites.net/)

-----

## Introduction

Parking Lot is a sample web application for managing parking lot.

## Technical information

Name | Value
--- | ---
Main programming language | C#
Framework used | ASP.NET Core 8.0

## License

Licensed under MIT. Read full license [here](https://gitlab.com/Prologh/parking-lot/raw/master/LICENSE).

## Credits

**Piotr Wosiek** | [GitLab](https://gitlab.com/Prologh) | [GitHub](https://github.com/Prologh)

### Acknowledgements

Project icon based on one made by [Freepik](https://www.freepik.com/) from [www.flaticon.com](https://www.flaticon.com/).
