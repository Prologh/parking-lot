﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ParkingLot.Application.Logging.Stores;
using ParkingLot.WebApp.Logging.Builders.Abstractions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ParkingLot.WebApp.Middleware.Logging;

/// <summary>
/// Saves logs based on incoming HTTP requests.
/// </summary>
public class RequestLoggingMiddleware
{
    public const string StaticFilesResponseKey = "static-files-response";

    private readonly RequestDelegate _next;

    /// <summary>
    /// Initializes a new instance of the <see cref="RequestLoggingMiddleware"/>
    /// class.
    /// </summary>
    /// <param name="next">
    /// A function that can process an HTTP request.
    /// </param>
    public RequestLoggingMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    /// <summary>
    /// Asynchronously invokes the request logging middleware.
    /// </summary>
    public async Task Invoke(
        HttpContext context,
        IRequestLogStore store,
        IOptionsSnapshot<RequestLoggingOptions> options,
        ILogger<RequestLoggingMiddleware> logger,
        IRequestLogBuilder requestLogBuilder)
    {
        var created = DateTimeOffset.Now;
        var stopwatch = Stopwatch.StartNew();

        await _next(context);

        stopwatch.Stop();

        if (ShouldSkipLogging(context, options.Value))
        {
            logger.LogDebug("Skipping request logging due to configuration.");

            return;
        }

        var requestLog = requestLogBuilder.BuildRequestLog(context, created, stopwatch.Elapsed);
        logger.LogDebug("Saving request log.");

        store.Add(requestLog);

        logger.LogDebug("Request log saved.");
    }

    private bool ShouldSkipLogging(HttpContext context, RequestLoggingOptions options)
    {
        return (!options.LogClientErrorRequests && IsClientSideErrorStatusCode(context.Response.StatusCode))
            || (!options.LogServerErrorRequests && IsServerSideErrorStatusCode(context.Response.StatusCode))
            || (!options.LogStaticFilesRequests && IsStaticFilesRequest(context.Items));
    }

    private bool IsClientSideErrorStatusCode(int statusCode)
    {
        return statusCode >= 400 && statusCode <= 499;
    }

    private bool IsServerSideErrorStatusCode(int statusCode)
    {
        return statusCode >= 500 && statusCode <= 599;
    }

    private bool IsStaticFilesRequest(IDictionary<object, object> items)
    {
        return items.ContainsKey(StaticFilesResponseKey);
    }
}
