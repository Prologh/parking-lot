﻿namespace ParkingLot.WebApp.Middleware.Logging;

/// <summary>
/// Represents settings for <see cref="RequestLoggingMiddleware"/>.
/// </summary>
public class RequestLoggingOptions
{
    /// <summary>
    /// Initializes a new instance of the <see cref="RequestLoggingOptions"/>
    /// class.
    /// </summary>
    public RequestLoggingOptions()
    {
        LogClientErrorRequests = true;
        LogServerErrorRequests = true;
        LogStaticFilesRequests = true;
    }

    /// <summary>
    /// Gets or sets a value indicating whether
    /// request logger should log client side error requests. Setting this option
    /// to <see langword="true"/> indicates that all unsuccessful by client side
    /// requests (e.g. with response code 400, 404) will be logged.
    /// <para/>
    /// Defaults to <see langword="true"/>.
    /// </summary>
    public bool LogClientErrorRequests { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether
    /// request logger should log server side error requests. Setting this option
    /// to <see langword="true"/> indicates that all unsuccessful by server side
    /// requests (e.g. with response code 500, 503) will be logged.
    /// <para/>
    /// Defaults to <see langword="true"/>.
    /// </summary>
    public bool LogServerErrorRequests { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether
    /// logger should log requests handled by static files middleware.
    /// <para/>
    /// Defaults to <see langword="true"/>.
    /// </summary>
    public bool LogStaticFilesRequests { get; set; }
}
