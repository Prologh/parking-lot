﻿using System.Collections.Generic;

namespace ParkingLot.WebApp.Middleware.Robots;

public class RobotsOptions
{
    public RobotsOptions()
    {
        DisallowedNames = new();
    }

    public Dictionary<string, string> DisallowedNames { get; set; }
}
