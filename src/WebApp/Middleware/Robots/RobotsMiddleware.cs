using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ParkingLot.WebApp.Middleware.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot.WebApp.Middleware.Robots;

public class RobotsMiddleware
{
    private readonly IOptions<RobotsOptions> _robotsOptions;
    private readonly RequestDelegate _next;

    public RobotsMiddleware(RequestDelegate requestDelegate, IOptions<RobotsOptions> robotsOptions)
    {
        _next = requestDelegate ?? throw new ArgumentNullException(nameof(requestDelegate));
        _robotsOptions = robotsOptions ?? throw new ArgumentNullException(nameof(robotsOptions));
    }

    public async Task Invoke(
        HttpContext context,
        ILogger<RobotsMiddleware> logger)
    {
        if (context.Request.Path.Equals("/robots.txt", StringComparison.OrdinalIgnoreCase))
        {
            logger.LogDebug("Returning robots.txt file.");

            context.Response.ContentType = "text/plain";
            await context.Response.WriteAsync(
@"User-agent: *
Disallow: /
");

            context.Items.Add(RequestLoggingMiddleware.StaticFilesResponseKey, "true");

            return;
        }

        var userAgent = context.Request.Headers.UserAgent.ToString();
        var disallowedBotNames = _robotsOptions.Value.DisallowedNames.Keys;
        var isOneOfDisallowedBotNames = disallowedBotNames.Any(botName => userAgent.Contains(botName, StringComparison.InvariantCultureIgnoreCase));
        if (isOneOfDisallowedBotNames)
        {
            context.Response.StatusCode = StatusCodes.Status403Forbidden;
            context.Response.Headers.Add("X-Reason", "Bots are not allowed.");

            return;
        }

        var userLanguages = context.Request.Headers.AcceptLanguage.SelectMany(x => x.Split(','));
        var userLanguageIsChinese = userLanguages.Any(x => x.StartsWith("zh", StringComparison.InvariantCultureIgnoreCase));
        if (userLanguageIsChinese)
        {
            context.Response.StatusCode = StatusCodes.Status403Forbidden;
            context.Response.Headers.Add("X-Reason", "Chinese language is not allowed due to strong bot association.");

            return;
        }

        await _next(context);

        return;
    }
}
