﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using ParkingLot.WebApp.Middleware.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ParkingLot.WebApp.Middleware.SiteMap;

/// <summary>
/// A middleware generating XML site map document.
/// </summary>
public class SiteMapMiddleware
{
    private readonly RequestDelegate _next;

    /// <summary>
    /// Initializes a new instance of the <see cref="SiteMapMiddleware"/>
    /// class.
    /// </summary>
    /// <param name="next">
    /// A function that can process an HTTP request.
    /// </param>
    public SiteMapMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    /// <summary>
    /// Asynchronously invokes the site map middleware.
    /// </summary>
    public async Task Invoke(
        HttpContext context,
        ILogger<SiteMapMiddleware> logger)
    {
        if (!context.Request.Path.Equals("/sitemap.xml", StringComparison.OrdinalIgnoreCase))
        {
            await _next(context);

            return;
        }

        logger.LogDebug("Genereting site map.");

        await WriteSiteMapXmlToResponseBodyAsync(context);
        context.Items.Add(RequestLoggingMiddleware.StaticFilesResponseKey, "true");

        logger.LogDebug("Site map generated.");
    }

    private async Task WriteSiteMapXmlToResponseBodyAsync(HttpContext context)
    {
        context.Response.ContentType = MediaTypeNames.Application.Xml;

        var now = DateTimeOffset.UtcNow;
        var sitePrefix = $"{context.Request.Scheme}://{context.Request.Host}";
        var sites = new List<Uri>
        {
            new Uri($"{sitePrefix}/"),
            new Uri($"{sitePrefix}/Levels"),
            new Uri($"{sitePrefix}/Logging"),
            new Uri($"{sitePrefix}/Spaces"),
            new Uri($"{sitePrefix}/Vehicles"),
            new Uri($"{sitePrefix}/History/Charts"),
            new Uri($"{sitePrefix}/History/Spaces"),
            new Uri($"{sitePrefix}/History/Vehicles"),
        };
        var xmlWriterSettings = new XmlWriterSettings
        {
            Indent = true,
            Encoding = Encoding.UTF8,
        };

        using var memoryStream = new MemoryStream();
        using var xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSettings);
        xmlWriter.WriteStartDocument();
        xmlWriter.WriteStartElement(prefix: null, localName: "urlset", ns: "http://www.sitemaps.org/schemas/sitemap/0.9");

        foreach (var siteUri in sites)
        {
            xmlWriter.WriteStartElement(prefix: null, localName: "url", ns: null);
            xmlWriter.WriteElementString(prefix: null, localName: "loc", ns: null, value: siteUri.ToString());
            xmlWriter.WriteElementString(prefix: null, localName: "lastmod", ns: null, value: now.ToString("yyyy-MM-dd"));
            xmlWriter.WriteEndElement();
        }

        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndDocument();
        xmlWriter.Flush();
        memoryStream.Position = 0;

        using var streamReader = new StreamReader(memoryStream, leaveOpen: true);
        var xmlSiteMap = streamReader.ReadToEnd();

        await context.Response.WriteAsync(xmlSiteMap, Encoding.UTF8);
    }
}
