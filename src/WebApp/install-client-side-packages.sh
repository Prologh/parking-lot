#!/bin/bash

echo "Updating system packages."
apt-get update --quiet --yes
echo "Packages successfully updated."

echo "Installing nodejs."
curl -sL https://deb.nodesource.com/setup_22.x | bash -
apt-get install --quiet --yes nodejs
echo "nodejs successfully installed."

echo "Installing npm."
npm install --global npm
echo "npm successfully installed."

echo "Installing gulp."
npm install --global gulp gulp-cli
echo "gulp successfully installed."

echo "Installing client side packages."
npm install
echo "Client side packages successfully installed."
