﻿using Microsoft.AspNetCore.Mvc.Razor;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ParkingLot.WebApp.Mvc.Razor;

/// <summary>
/// Expanding class for view location paths.
/// </summary>
public class ViewLocationExpander : IViewLocationExpander
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ViewLocationExpander"/> class.
    /// </summary>
    public ViewLocationExpander()
    {

    }

    /// <summary>
    /// Used to specify the locations that the view engine should search to
    /// locate views.
    /// </summary>
    /// <param name="context"></param>
    /// <param name="viewLocations">
    /// Collection of current view location path patterns.
    /// </param>
    /// <returns>
    /// Adds additional view location path patterns.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="context"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="viewLocations"/> is <see langword="null"/>.
    /// </exception>
    public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
    {
        if (context == null)
        {
            throw new ArgumentNullException(nameof(context));
        }

        if (viewLocations == null)
        {
            throw new ArgumentNullException(nameof(viewLocations));
        }

        /*
         * Note:
         * {0} = action name
         * {1} = controller name
         * {2} = area name
         */

        var newViewLocations = new string[]
        {
            // Example: '/Views/Home/_Partials/FooBar.cshtml'
            "/Views/{1}/_Partials/{0}.cshtml",
        };

        // Add new locations *AFTER* MVC default locations.
        return viewLocations.Union(newViewLocations);
    }

    /// <summary>
    /// Populates the context with additional view location paths.
    /// </summary>
    /// <param name="context"></param>
    public void PopulateValues(ViewLocationExpanderContext context)
    {
        context.Values["customviewlocation"] = nameof(ViewLocationExpander);
    }
}
