﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
using System;

namespace ParkingLot.WebApp.Mvc.ModelBinding.Metadata;

/// <summary>
/// Provides custom display metadata.
/// </summary>
public class CustomDisplayMetadataProvider : IDisplayMetadataProvider
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CustomDisplayMetadataProvider"/>
    /// class.
    /// </summary>
    public CustomDisplayMetadataProvider()
    {

    }

    /// <summary>
    /// Sets the values for properties of <see cref="DisplayMetadataProviderContext.DisplayMetadata"/>.
    /// </summary>
    /// <param name="context">
    /// The <see cref="DisplayMetadataProviderContext"/>.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="context"/> is <see langword="null"/>.
    /// </exception>
    public void CreateDisplayMetadata(DisplayMetadataProviderContext context)
    {
        if (context == null)
        {
            throw new ArgumentNullException(nameof(context));
        }

        // TODO:
        // Add custom display metadata logic.
        //context.DisplayMetadata
        //    .AdditionalValues
        //    .Add("KEY", "VALUE");
    }
}
