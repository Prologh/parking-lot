﻿using Fluorite.Strainer.Models;
using Fluorite.Strainer.Models.Metadata;
using Fluorite.Strainer.Models.Sorting;
using Fluorite.Strainer.Services.Configuration;
using Fluorite.Strainer.Services.Metadata;
using Fluorite.Strainer.Services.Sorting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using ParkingLot.Application.Strainer.Mapping;
using ParkingLot.Extensions.Routing;
using System;
using System.Linq;

namespace ParkingLot.WebApp.Mvc.TagHelpers;

/// <summary>
/// <see cref="ITagHelper"/> implementation targeting every &lt;a&gt; tag
/// with an <c>asp-sort-for</c> attribute.
/// </summary>
[HtmlTargetElement(AnchorLinkHtmlElementName, Attributes = OrderByAttributeName)]
public class StrainerAnchorSortingTagHelper : TagHelper
{
    private const string PageNumberParameterName = nameof(IStrainerModel.Page);
    private const string SortsQueryParameterName = nameof(IStrainerModel.Sorts);

    private const string AnchorLinkHtmlElementName = "a";
    private const string OrderByAttributeName = "asp-order-by";
    private const string PropertyNameAttributeName = "asp-model-name";
    private const string HyperlinkAttributeName = "href";

    private readonly IMetadataFacade _metadataFacade;
    private readonly IStrainerTypeMapper _strainerTypeMapper;
    private readonly IConfigurationCustomMethodsProvider _customMethodsProvider;
    private readonly ISortingWayFormatter _sortingWayFormatter;
    private readonly IUrlHelperFactory _urlHelperFactory;

    public StrainerAnchorSortingTagHelper(
        IMetadataFacade metadataFacade,
        IStrainerTypeMapper strainerTypeMapper,
        IConfigurationCustomMethodsProvider customMethodsProvider,
        ISortingWayFormatter sortingWayFormatter,
        IUrlHelperFactory urlHelperFactory)
    {
        _metadataFacade = metadataFacade;
        _strainerTypeMapper = strainerTypeMapper;
        _customMethodsProvider = customMethodsProvider;
        _sortingWayFormatter = sortingWayFormatter;
        _urlHelperFactory = urlHelperFactory;
    }

    /// <summary>
    /// Gets the order of tag helper. Used in scheduling the tag helper execution.
    /// </summary>
    public override int Order => int.MaxValue;

    /// <summary>
    /// An expression leading to model property describing sort order.
    /// </summary>
    [HtmlAttributeName(OrderByAttributeName)]
    public ModelExpression OrderBy { get; set; }

    /// <summary>
    /// Gets the request query.
    /// </summary>
    protected IQueryCollection RequestQuery => ViewContext.HttpContext.Request.Query;

    /// <summary>
    /// Member name against which targeted sort order rule is associated (optional).
    /// </summary>
    [HtmlAttributeName(PropertyNameAttributeName)]
    public string PropertyName { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="Microsoft.AspNetCore.Mvc.Rendering.ViewContext"/>
    /// for the current request.
    /// </summary>
    [HtmlAttributeNotBound]
    [ViewContext]
    public ViewContext ViewContext { get; set; }

    /// <summary>
    /// Synchronously executes the <see cref="TagHelper"/> with the given
    /// context and output.
    /// <para/>
    /// Adds the href attribute according to associated sort order
    /// to specified HTML &lt;a&gt; tag.
    /// </summary>
    /// <param name="context">
    /// Contains information associated with the current HTML tag.
    /// </param>
    /// <param name="output">
    /// A stateful HTML element used to generate an HTML tag.
    /// </param>
    public override void Process(TagHelperContext context, TagHelperOutput output)
    {
        if (context == null)
        {
            throw new ArgumentNullException(nameof(context));
        }

        if (output == null)
        {
            throw new ArgumentNullException(nameof(output));
        }

        if (OrderBy == null)
        {
            throw new ArgumentNullException(nameof(OrderBy));
        }

        if (!output.Attributes.ContainsName(HyperlinkAttributeName))
        {
            var containerType = GetContainerType();
            var modelType = GetSortOrderModelType();
            var propertyName = GetPropertyName();

            var allMetadatas = _metadataFacade.GetMetadatas(modelType);
            var metadatas = allMetadatas
                .Where(propertyMetada =>
                    propertyName.Equals(
                        propertyMetada.DisplayName ?? propertyMetada.Name,
                        StringComparison.OrdinalIgnoreCase));

            if (!metadatas.Any())
            {
                if (!_customMethodsProvider.GetCustomSortMethods().TryGetValue(modelType, out var typeCustomSortMethods))
                {
                    throw new InvalidOperationException(
                        $"No sort order rule found for {propertyName} member of " +
                        $"{OrderBy.Metadata.ContainerType.FullName}.\n" +
                        $"Please register a sort order rules for that type " +
                        $"in order to use it in this tag helper.");
                }

                if (!typeCustomSortMethods.TryGetValue(propertyName, out var customSortMethod))
                {
                    throw new InvalidOperationException(
                        $"No sort order rule found for {propertyName} member of " +
                        $"{OrderBy.Metadata.ContainerType.FullName}.\n" +
                        $"Please register a sort order rule for that member " +
                        $"in order to use it in this tag helper.");
                }
            }

            var defaultSortingPropertyMetadata = allMetadatas.FirstOrDefault(m => m.IsDefaultSorting);
            if (defaultSortingPropertyMetadata == null)
            {
                throw new InvalidOperationException(
                    $"No default sort order rule found for type " +
                    $"{OrderBy.Metadata.ContainerType.FullName}.\n" +
                    $"Please register a default sort order rule for that type " +
                    $"in order to use it in this tag helper.");
            }

            var querySortValue = GetSortValueFromQuery();
            var currentSortingWay = GetCurrentSortingWay(defaultSortingPropertyMetadata, querySortValue);
            var isCurrentSortOrder = CheckIfProcessingCurrentSortOrder(containerType, propertyName, querySortValue);

            if (isCurrentSortOrder)
            {
                AddCssClass(output, currentSortingWay);
            }

            var sortQueryValue = GetNewSortQueryValue(propertyName, currentSortingWay, isCurrentSortOrder);
            AddHref(output, sortQueryValue);
        }
    }

    private void AddCssClass(TagHelperOutput output, SortingWay sortingWay)
    {
        string cssClassName = null;

        switch (sortingWay)
        {
            case SortingWay.Ascending:
                cssClassName = "ascending";
                break;
            case SortingWay.Descending:
                cssClassName = "descending";
                break;
            case SortingWay.Unknown:
                throw new ArgumentException("Cannot operate on unkown sorting way.");
        }

        if (!output.Attributes.ContainsName("class"))
        {
            output.Attributes.SetAttribute("class", cssClassName);

            return;
        }

        var attribute = output.Attributes["class"];
        output.Attributes.SetAttribute("class", $"{attribute.Value} {cssClassName}");
    }

    private void AddHref(TagHelperOutput output, string sortQueryValue)
    {
        if (output.Attributes.ContainsName(HyperlinkAttributeName))
        {
            output.Attributes.RemoveAll(HyperlinkAttributeName);
            output.Attributes.Add(HyperlinkAttributeName, "#");
        }

        var actionName = ViewContext.RouteData.GetAction();
        var queryData = RequestQuery.Where(pair =>
                !pair.Key.Equals(PageNumberParameterName, StringComparison.OrdinalIgnoreCase)
                && !pair.Key.Equals(SortsQueryParameterName, StringComparison.OrdinalIgnoreCase))
            .ToDictionary(pair => pair.Key, pair => pair.Value.ToString());
        queryData[SortsQueryParameterName] = sortQueryValue;
        var url = _urlHelperFactory
            .GetUrlHelper(ViewContext)
            .Action(actionName, queryData);
        output.Attributes.Add(HyperlinkAttributeName, url);
    }

    private SortingWay GetCurrentSortingWay(
        IPropertyMetadata defaultSortingPropertyMetadata,
        string input)
    {
        var sortingWay = _sortingWayFormatter.GetSortingWay(input);

        if (sortingWay != SortingWay.Unknown)
        {
            return sortingWay;
        }

        if (defaultSortingPropertyMetadata.IsDefaultSortingDescending)
        {
            return SortingWay.Descending;
        }
        else
        {
            return SortingWay.Ascending;
        }
    }

    private bool CheckIfProcessingCurrentSortOrder(Type containerType, string propertyName, string querySortValue)
    {
        if (string.IsNullOrEmpty(querySortValue))
        {
            var mappedType = _strainerTypeMapper.Map(containerType);
            var defaultProperty = _metadataFacade.GetDefaultMetadata(mappedType);
            var defaultPropertyName = defaultProperty.DisplayName ?? defaultProperty.Name;

            return propertyName.Equals(defaultPropertyName, StringComparison.OrdinalIgnoreCase);
        }
        else
        {
            var sortingWay = _sortingWayFormatter.GetSortingWay(querySortValue);
            var unformattedSortValue = _sortingWayFormatter.Unformat(querySortValue, sortingWay);

            return unformattedSortValue.Equals(propertyName, StringComparison.OrdinalIgnoreCase);
        }
    }

    private string GetNewSortQueryValue(string propertyName, SortingWay sortingWay, bool isCurrentColumn)
    {
        if (sortingWay == SortingWay.Unknown)
        {
            throw new ArgumentException("Cannot operate on unkown sorting way.");
        }

        if (isCurrentColumn)
        {
            return sortingWay switch
            {
                SortingWay.Ascending => _sortingWayFormatter.Format(propertyName, SortingWay.Descending),
                SortingWay.Descending => _sortingWayFormatter.Format(propertyName, SortingWay.Ascending),
                _ => throw new ArgumentException("Cannot operate on unkown sorting way."),
            };
        }

        return _sortingWayFormatter.Format(propertyName, sortingWay);
    }

    private string GetPropertyName() => PropertyName ?? OrderBy.Metadata.Name;

    private Type GetSortOrderModelType()
    {
        return _strainerTypeMapper.Map(OrderBy.Metadata.ContainerType);
    }

    private Type GetContainerType()
    {
        return OrderBy.ModelExplorer.Container.ModelType;
    }

    private string GetSortValueFromQuery()
    {
        return RequestQuery
            .FirstOrDefault(pair => pair.Key.Equals(SortsQueryParameterName, StringComparison.OrdinalIgnoreCase))
            .Value
            .ToString();
    }
}
