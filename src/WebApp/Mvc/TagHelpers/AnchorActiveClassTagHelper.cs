﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using ParkingLot.Extensions.Routing;
using System;
using System.Linq;

namespace ParkingLot.WebApp.Mvc.TagHelpers;

/// <summary>
/// <see cref="ITagHelper"/> implementation targeting every &lt;a&gt; tag
/// with an <c>asp-add-active-class</c> attribute.
/// </summary>
[HtmlTargetElement(AnchorLinkHtmlElementName, Attributes = AddActiveClassAttributeName)]
public class AnchorActiveClassTagHelper : TagHelper
{
    private const string AnchorLinkHtmlElementName = "a";
    private const string DefaultActiveClassValue = "active";
    private const string CssClassAttributeName = "class";
    private const string AddActiveClassAttributeName = "asp-add-active-class";
    private const string AreaAttributeName = "asp-area";
    private const string ControllerAttributeName = "asp-controller";
    private const string ActionAttributeName = "asp-action";

    /// <summary>
    /// Gets the order of tag helper. Used in scheduling the tag helper execution.
    /// </summary>
    public override int Order => int.MaxValue;

    /// <summary>
    /// A <see cref="bool"/> value indicating whether an CSS class should
    /// be add when current &lt;a&gt; tag leads to currently viewed resource.
    /// </summary>
    [HtmlAttributeName(AddActiveClassAttributeName)]
    public bool AddActiveClass { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="Microsoft.AspNetCore.Mvc.Rendering.ViewContext"/>
    /// for the current request.
    /// </summary>
    [HtmlAttributeNotBound]
    [ViewContext]
    public ViewContext ViewContext { get; set; }

    /// <summary>
    /// Synchronously executes the <see cref="TagHelper"/> with the given
    /// context and output. Adds the active class to class list to specified
    /// HTML &lt;a&gt; tag.
    /// </summary>
    /// <param name="context">
    /// Contains information associated with the current HTML tag.
    /// </param>
    /// <param name="output">
    /// A stateful HTML element used to generate an HTML tag.
    /// </param>
    public override void Process(TagHelperContext context, TagHelperOutput output)
    {
        if (context == null)
        {
            throw new ArgumentNullException(nameof(context));
        }

        if (output == null)
        {
            throw new ArgumentNullException(nameof(output));
        }

        if (ViewContext == null)
        {
            throw new ArgumentNullException(nameof(ViewContext));
        }

        if (!AddActiveClass)
        {
            return;
        }

        // Route values
        var routeArea = GetRouteArea();
        var routeController = GetRouteController();

        // Anchor tag values
        var anchorArea = GetAttributeValue(context, AreaAttributeName);
        var anchorController = GetAttributeValue(context, ControllerAttributeName);

        if (routeArea == anchorArea
            && routeController == anchorController)
        {
            AddCssClassToOutput(context, output);
        }
    }

    private void AddCssClassToOutput(TagHelperContext context, TagHelperOutput output)
    {
        if (!output.Attributes.ContainsName(CssClassAttributeName))
        {
            output.Attributes.SetAttribute(CssClassAttributeName, DefaultActiveClassValue);
        }
        else
        {
            var cssClassValue = output.Attributes.First(a => a.Name == CssClassAttributeName).Value.ToString();
            output.Attributes.SetAttribute(CssClassAttributeName, $"{cssClassValue} {DefaultActiveClassValue}");
        }
    }

    private string GetAttributeValue(TagHelperContext context, string attributeName)
    {
        context.AllAttributes.TryGetAttribute(attributeName, out var tagHelperAttribute);

        return tagHelperAttribute?.Value?.ToString().Trim() ?? string.Empty;
    }

    private string GetRouteArea()
    {
        return ViewContext.RouteData.GetArea()?.Trim() ?? string.Empty;
    }

    private string GetRouteController()
    {
        return ViewContext.RouteData.GetController()?.Trim() ?? string.Empty;
    }
}
