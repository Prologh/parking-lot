﻿using System;

namespace ParkingLot.WebApp.ViewModels;

/// <summary>
/// Represents view model for exception.
/// </summary>
public class ExceptionVM
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ExceptionVM"/> class.
    /// </summary>
    public ExceptionVM()
    {

    }

    /// <summary>
    /// Gets or sets the related exception.
    /// </summary>
    public Exception Exception { get; set; }

    /// <summary>
    /// Gets or sets end user-friendly exception message.
    /// </summary>
    public string Message { get; set; }
}
