﻿namespace ParkingLot.WebApp.ViewModels;

/// <summary>
/// Represents view model for HTTP status code response.
/// </summary>
public class HttpStatusCodeVM
{
    /// <summary>
    /// Initializes a new instance of the <see cref="HttpStatusCodeVM"/>
    /// class.
    /// </summary>
    public HttpStatusCodeVM()
    {

    }

    /// <summary>
    /// Gets or sets the <see cref="int"/> representation of <see cref="System.Net.HttpStatusCode"/>.
    /// </summary>
    public int HttpStatusCode { get; set; }

    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether current HTTP
    /// status code indicate an error code between 400 and 599.
    /// </summary>
    public bool IsError => HttpStatusCode >= 400 && HttpStatusCode <= 599;

    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether current HTTP
    /// status code indicate a client side error code between 400 and 499.
    /// </summary>
    public bool IsClientSideError => HttpStatusCode >= 400 && HttpStatusCode <= 499;

    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether current HTTP
    /// status code indicate a server side error code between 500 and 599.
    /// </summary>
    public bool IsServerSideError => HttpStatusCode >= 500 && HttpStatusCode <= 599;

    /// <summary>
    /// Gets or sets end user-friendly exception message.
    /// </summary>
    public string Message { get; set; }
}
