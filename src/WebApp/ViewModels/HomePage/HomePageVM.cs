﻿using ParkingLot.Application.Levels.ViewModels;
using System.Collections.Generic;

namespace ParkingLot.WebApp.ViewModels.HomePage;

/// <summary>
/// Represents a view model for home page.
/// </summary>
public class HomePageVM
{
    /// <summary>
    /// Initializes a new instance of the <see cref="HomePageVM"/> class.
    /// </summary>
    public HomePageVM()
    {
        LevelMetadata = new List<LevelMetadataVM>();
    }

    /// <summary>
    /// Gets or sets a list of level metadata.
    /// </summary>
    public IList<LevelMetadataVM> LevelMetadata { get; set; }
}
