﻿using Microsoft.Extensions.Localization;
using ParkingLot.Application.Http.Humanizers;
using ParkingLot.Application.Http.Providers;
using ParkingLot.Application.Http.Validators;

namespace ParkingLot.WebApp.Http.Providers;

/// <summary>
/// Provides methods for getting error messages from HTTP status codes.
/// </summary>
public class HttpStatusCodeMessageProvider : IHttpStatusCodeMessageProvider
{
    private readonly IStringLocalizer _localizer;
    private readonly IHttpStatusCodeHumanizer _humanizer;
    private readonly IHttpStatusCodeValidator _validator;

    public HttpStatusCodeMessageProvider(
        IStringLocalizer<HttpStatusCodeMessageProvider> localizer,
        IHttpStatusCodeHumanizer humanizer,
        IHttpStatusCodeValidator validator)
    {
        _localizer = localizer;
        _humanizer = humanizer;
        _validator = validator;
    }

    /// <summary>
    /// Gets the error message based on its http status code.
    /// </summary>
    /// <param name="httpStatusCode">
    /// HTTP status code represented as <see cref="int"/>.
    /// </param>
    /// <returns>
    /// Proper message indiciting http status code.
    /// </returns>
    public string GetHttpStatusCodeMessage(int httpStatusCode)
    {
        if (_validator.CheckIfValid(httpStatusCode))
        {
            return _humanizer.HumanizeHttpStatusCode(httpStatusCode);
        }
        else
        {
            // Not a valid HTTP status code.
            return _localizer["UnknownHttpStatusCode"];
        }
    }
}
