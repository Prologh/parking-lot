﻿using ParkingLot.Application.Strainer;
using ParkingLot.WebApp.ListView.Interfaces;
using System;

namespace ParkingLot.WebApp.ListView.Providers;

public class PropertyBasisTypeProvider : IPropertyBasisTypeProvider
{
    public PropertyBasisType GetBasisType(Type type)
    {
        if (type is null)
        {
            throw new ArgumentNullException(nameof(type));
        }

        type = Nullable.GetUnderlyingType(type) ?? type;
        var typeCode = Type.GetTypeCode(type);

        if (type == typeof(DateTimeOffset))
        {
            return PropertyBasisType.DateTime;
        }

        if (type == typeof(TimeSpan))
        {
            return PropertyBasisType.String;
        }

        return typeCode switch
        {
            TypeCode.Empty => throw new InvalidOperationException($"Unable to match type ${type.Name} to a type code."),
            TypeCode.Object => throw new NotSupportedException($"Type {type.Name} is not supported for automatic property registration."),
            TypeCode.DBNull => throw new NotSupportedException($"Type {type.Name} is not supported for automatic property registration."),
            TypeCode.Boolean => PropertyBasisType.Boolean,
            TypeCode.Char => throw new NotSupportedException($"Type {type.Name} is not supported for automatic property registration."),
            TypeCode.SByte
                or TypeCode.Byte
                or TypeCode.Int16
                or TypeCode.UInt16
                or TypeCode.Int32
                or TypeCode.UInt32
                or TypeCode.Int64
                or TypeCode.UInt64 => PropertyBasisType.Integer,
            TypeCode.Single
                or TypeCode.Double
                or TypeCode.Decimal => PropertyBasisType.Number,
            TypeCode.DateTime => PropertyBasisType.DateTime,
            TypeCode.String => PropertyBasisType.String,
            _ => throw new InvalidOperationException($"Unable to match type ${type.Name} to a type code."),
        };
    }
}
