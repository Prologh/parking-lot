﻿using ParkingLot.Application.Strainer;
using ParkingLot.WebApp.ListView.Interfaces;
using ParkingLot.WebApp.ListView.Models;
using System;
using System.Linq.Expressions;

namespace ParkingLot.WebApp.ListView.Builders;

public class ListViewModuleBooleanPropertyBuilder<T, TProperty> : ListViewModulePropertyBuilder<T, TProperty>,
    IListViewModuleBooleanPropertyBuilder<T, TProperty>
    where T : class
{
    public ListViewModuleBooleanPropertyBuilder(PropertyDescriptor propertyDescriptor, Expression<Func<T, TProperty>> selector)
        : base(propertyDescriptor, selector)
    {
        propertyDescriptor.BasisType = BasicType;
    }

    public PropertyBasisType BasicType => PropertyBasisType.Boolean;
}
