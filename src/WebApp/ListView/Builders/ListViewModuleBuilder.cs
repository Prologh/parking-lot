﻿using ParkingLot.Application.Expressions;
using ParkingLot.Application.Strainer;
using ParkingLot.WebApp.ListView.Interfaces;
using ParkingLot.WebApp.ListView.Models;
using System;
using System.Linq.Expressions;

namespace ParkingLot.WebApp.ListView.Builders;

public class ListViewModuleBuilder<T> : IListViewModuleBuilder<T>
    where T : class
{
    private readonly IListViewModule<T> _listViewModule;
    private readonly IPropertyNameExtractor _propertyNameExtractor;
    private readonly IPropertyInfoExtractor _propertyInfoExtractor;
    private readonly IPropertyBasisTypeProvider _propertyBasisTypeProvider;

    public ListViewModuleBuilder(
        IListViewModule<T> listViewModule,
        IPropertyNameExtractor propertyNameExtractor,
        IPropertyInfoExtractor propertyInfoExtractor,
        IPropertyBasisTypeProvider propertyBasisTypeProvider)
    {
        _listViewModule = listViewModule;
        _propertyNameExtractor = propertyNameExtractor;
        _propertyInfoExtractor = propertyInfoExtractor;
        _propertyBasisTypeProvider = propertyBasisTypeProvider;
    }

    public IListViewModuleBuilder<T> AutoRegisterAllProperties()
    {
        var properties = _propertyInfoExtractor.GetAllPublicSettableInstanceProperties<T>();

        foreach (var propertyInfo in properties)
        {
            var basisType = _propertyBasisTypeProvider.GetBasisType(propertyInfo.PropertyType);
            var propertyDescriptor = new PropertyDescriptor
            {
                Name = propertyInfo.Name,
                BasisType = basisType,
            };

            _listViewModule.PropertyDescriptors[propertyInfo.Name] = propertyDescriptor;
        }

        return this;
    }

    public IListViewModulePropertyBuilder<T, TProperty> Property<TProperty>(Expression<Func<T, TProperty>> selector)
    {
        if (selector is null)
        {
            throw new ArgumentNullException(nameof(selector));
        }

        var propertyName = _propertyNameExtractor.GetPropertyName(selector);
        var propertyDescriptor = new PropertyDescriptor
        {
            Name = propertyName,
            BasisType = PropertyBasisType.Unknown,
        };

        _listViewModule.PropertyDescriptors[propertyName] = propertyDescriptor;

        return new ListViewModulePropertyBuilder<T, TProperty>(propertyDescriptor, selector);
    }
}
