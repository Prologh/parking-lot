﻿using ParkingLot.WebApp.ListView.Interfaces;
using ParkingLot.WebApp.ListView.Models;
using System;
using System.Linq.Expressions;

namespace ParkingLot.WebApp.ListView.Builders;

public class ListViewModuleDateTimePropertyBuilder<T, TProperty> : ListViewModulePropertyBuilder<T, TProperty>,
    IListViewModuleDateTimePropertyBuilder<T, TProperty>
    where T : class
{
    public ListViewModuleDateTimePropertyBuilder(PropertyDescriptor propertyDescriptor, Expression<Func<T, TProperty>> selector)
        : base(propertyDescriptor, selector)
    {
    }
}
