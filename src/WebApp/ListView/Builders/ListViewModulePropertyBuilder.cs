﻿using ParkingLot.Application.Strainer;
using ParkingLot.WebApp.ListView.Interfaces;
using ParkingLot.WebApp.ListView.Models;
using System;
using System.Linq.Expressions;

namespace ParkingLot.WebApp.ListView.Builders;

public class ListViewModulePropertyBuilder<T, TProperty> : IListViewModulePropertyBuilder<T, TProperty>
    where T : class
{
    private readonly PropertyDescriptor _propertyDescriptor;
    private readonly Expression<Func<T, TProperty>> _selector;

    public ListViewModulePropertyBuilder(
        PropertyDescriptor propertyDescriptor,
        Expression<Func<T, TProperty>> selector)
    {
        _propertyDescriptor = propertyDescriptor ?? throw new ArgumentNullException(nameof(propertyDescriptor));
        _selector = selector ?? throw new ArgumentNullException(nameof(selector));
    }

    public IListViewModulePropertyBuilder<T, TProperty> IsBooleanBased() => ChangeBasis(PropertyBasisType.Boolean);

    public IListViewModulePropertyBuilder<T, TProperty> IsDateTimeBased() => ChangeBasis(PropertyBasisType.DateTime);

    public IListViewModulePropertyBuilder<T, TProperty> IsIntegerBased() => ChangeBasis(PropertyBasisType.Integer);

    public IListViewModulePropertyBuilder<T, TProperty> IsNumberBased() => ChangeBasis(PropertyBasisType.Number);

    public IListViewModulePropertyBuilder<T, TProperty> IsStringBased() => ChangeBasis(PropertyBasisType.String);

    private IListViewModulePropertyBuilder<T, TProperty> ChangeBasis(PropertyBasisType newPropertyBasisType)
    {
        _propertyDescriptor.BasisType = newPropertyBasisType;

        return this;
    }
}
