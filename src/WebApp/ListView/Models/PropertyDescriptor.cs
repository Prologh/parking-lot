﻿using ParkingLot.Application.Strainer;
using ParkingLot.WebApp.ListView.Interfaces;

namespace ParkingLot.WebApp.ListView.Models;

public class PropertyDescriptor : IPropertyDescriptor
{
    public string Name { get; init; }

    public bool IsBooleanBased => BasisType == PropertyBasisType.Boolean;

    public bool IsDateTimeBased => BasisType == PropertyBasisType.DateTime;

    public bool IsIntegerBased => BasisType == PropertyBasisType.Integer;

    public bool IsNumberBased => BasisType == PropertyBasisType.Number;

    public bool IsStringBased => BasisType == PropertyBasisType.String;

    public PropertyBasisType BasisType { get; set; }
}
