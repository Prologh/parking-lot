﻿using ParkingLot.Application.Vehicles.ViewModels;
using ParkingLot.WebApp.ListView.Abstractions;
using ParkingLot.WebApp.ListView.Interfaces;

namespace ParkingLot.WebApp.ListView;

public class VehiclesListViewModule : BaseListViewModule<VehicleVM>
{
    public override void Load(IListViewModuleBuilder<VehicleVM> builder)
    {
        builder.AutoRegisterAllProperties();
    }
}
