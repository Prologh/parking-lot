﻿using ParkingLot.Application.Logging.ViewModels;
using ParkingLot.WebApp.ListView.Abstractions;
using ParkingLot.WebApp.ListView.Interfaces;

namespace ParkingLot.WebApp.ListView;

public class LoggingListViewModule : BaseListViewModule<RequestLogVM>
{
    public override void Load(IListViewModuleBuilder<RequestLogVM> builder)
    {
        builder.AutoRegisterAllProperties();
    }
}
