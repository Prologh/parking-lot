﻿using ParkingLot.WebApp.ListView.Interfaces;
using System;
using System.Collections.Generic;

namespace ParkingLot.WebApp.ListView.Configuration;

public class ListViewConfiguration : IListViewConfiguration
{
    public ListViewConfiguration(IReadOnlyDictionary<Type, IReadOnlyDictionary<string, IPropertyDescriptor>> propertyDescriptors)
    {
        PropertyDescriptors = propertyDescriptors ?? throw new ArgumentNullException(nameof(propertyDescriptors));
    }

    public IReadOnlyDictionary<Type, IReadOnlyDictionary<string, IPropertyDescriptor>> PropertyDescriptors { get; }
}
