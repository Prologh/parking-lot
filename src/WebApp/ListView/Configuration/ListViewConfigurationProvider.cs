﻿using ParkingLot.WebApp.ListView.Interfaces;
using System;

namespace ParkingLot.WebApp.ListView.Configuration;

public class ListViewConfigurationProvider : IListViewConfigurationProvider
{
    private readonly IListViewConfiguration _listViewConfiguration;

    public ListViewConfigurationProvider(IListViewConfiguration listViewConfiguration)
    {
        _listViewConfiguration = listViewConfiguration ?? throw new ArgumentNullException(nameof(listViewConfiguration));
    }

    public IListViewConfiguration GetConfiguration() => _listViewConfiguration;
}
