﻿using ParkingLot.Application.SpaceHistory.ViewModels;
using ParkingLot.WebApp.ListView.Abstractions;
using ParkingLot.WebApp.ListView.Interfaces;

namespace ParkingLot.WebApp.ListView;

public class SpaceHistoryListViewModule : BaseListViewModule<SpaceHistoryEntryVM>
{
    public override void Load(IListViewModuleBuilder<SpaceHistoryEntryVM> builder)
    {
        builder.AutoRegisterAllProperties();

        builder
            .Property(x => x.Space.Number)
            .IsIntegerBased();
    }
}
