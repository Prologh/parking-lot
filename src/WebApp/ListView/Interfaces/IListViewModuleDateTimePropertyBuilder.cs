﻿using System;

namespace ParkingLot.WebApp.ListView.Interfaces;

public interface IListViewModuleDateTimePropertyBuilder<T, TProperty> : IListViewModulePropertyBuilder<T, TProperty>
    where T : class
{
}
