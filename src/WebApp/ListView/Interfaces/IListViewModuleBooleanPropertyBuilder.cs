﻿namespace ParkingLot.WebApp.ListView.Interfaces;

public interface IListViewModuleBooleanPropertyBuilder<T, TProperty> : IListViewModulePropertyBuilder<T, TProperty>
    where T : class
{
}
