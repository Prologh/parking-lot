﻿namespace ParkingLot.WebApp.ListView.Interfaces;

public interface IListViewModuleNumberPropertyBuilder<T, TProperty> : IListViewModulePropertyBuilder<T, TProperty>
    where T : class
{
}
