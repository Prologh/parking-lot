﻿namespace ParkingLot.WebApp.ListView.Interfaces;

public interface IListViewModule<T> : IListViewModule
    where T : class
{
    void Load(IListViewModuleBuilder<T> builder);
}
