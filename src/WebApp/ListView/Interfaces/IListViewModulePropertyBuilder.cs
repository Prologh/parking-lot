﻿namespace ParkingLot.WebApp.ListView.Interfaces;

public interface IListViewModulePropertyBuilder<T, TProperty>
    where T : class
{
    IListViewModulePropertyBuilder<T, TProperty> IsBooleanBased();

    IListViewModulePropertyBuilder<T, TProperty> IsDateTimeBased();

    IListViewModulePropertyBuilder<T, TProperty> IsIntegerBased();

    IListViewModulePropertyBuilder<T, TProperty> IsNumberBased();

    IListViewModulePropertyBuilder<T, TProperty> IsStringBased();
}
