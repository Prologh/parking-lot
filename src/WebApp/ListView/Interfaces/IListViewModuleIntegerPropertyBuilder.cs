﻿namespace ParkingLot.WebApp.ListView.Interfaces;

public interface IListViewModuleIntegerPropertyBuilder<T, TProperty> : IListViewModulePropertyBuilder<T, TProperty>
    where T : class
{
}
