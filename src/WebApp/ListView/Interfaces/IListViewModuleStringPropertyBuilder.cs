﻿namespace ParkingLot.WebApp.ListView.Interfaces;

public interface IListViewModuleStringPropertyBuilder<T> : IListViewModulePropertyBuilder<T, string>
    where T : class
{
}
