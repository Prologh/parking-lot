﻿using System.Collections.Generic;

namespace ParkingLot.WebApp.ListView.Interfaces;

public interface IListViewModule
{
    IDictionary<string, IPropertyDescriptor> PropertyDescriptors { get; }
}
