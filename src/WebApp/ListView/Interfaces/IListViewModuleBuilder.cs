﻿using System;
using System.Linq.Expressions;

namespace ParkingLot.WebApp.ListView.Interfaces;

public interface IListViewModuleBuilder<T>
    where T : class
{
    IListViewModuleBuilder<T> AutoRegisterAllProperties();

    IListViewModulePropertyBuilder<T, TProperty> Property<TProperty>(Expression<Func<T, TProperty>> selector);
}
