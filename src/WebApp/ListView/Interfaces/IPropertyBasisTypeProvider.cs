﻿using ParkingLot.Application.Strainer;
using System;

namespace ParkingLot.WebApp.ListView.Interfaces;

public interface IPropertyBasisTypeProvider
{
    PropertyBasisType GetBasisType(Type type);
}
