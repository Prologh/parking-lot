﻿namespace ParkingLot.WebApp.ListView.Interfaces;

public interface IListViewConfigurationProvider
{
    IListViewConfiguration GetConfiguration();
}
