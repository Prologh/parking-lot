﻿using System;
using System.Collections.Generic;

namespace ParkingLot.WebApp.ListView.Interfaces;

public interface IListViewConfiguration
{
    IReadOnlyDictionary<Type, IReadOnlyDictionary<string, IPropertyDescriptor>> PropertyDescriptors { get; }
}
