﻿using ParkingLot.Application.Strainer;

namespace ParkingLot.WebApp.ListView.Interfaces;

public interface IPropertyDescriptor
{
    string Name { get; }

    bool IsBooleanBased { get; }

    bool IsDateTimeBased { get; }

    bool IsIntegerBased { get; }

    bool IsNumberBased { get; }

    bool IsStringBased { get; }

    PropertyBasisType BasisType { get; }
}
