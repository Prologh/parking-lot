﻿using ParkingLot.Application.Spaces.ViewModels;
using ParkingLot.WebApp.ListView.Abstractions;
using ParkingLot.WebApp.ListView.Interfaces;

namespace ParkingLot.WebApp.ListView;

public class SpacesListViewModule : BaseListViewModule<SpaceListItemVM>
{
    public override void Load(IListViewModuleBuilder<SpaceListItemVM> builder)
    {
        builder.AutoRegisterAllProperties();
    }
}
