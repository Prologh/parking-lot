﻿using ParkingLot.WebApp.ListView.Interfaces;
using System.Collections.Generic;

namespace ParkingLot.WebApp.ListView.Abstractions;

public abstract class BaseListViewModule<T> : IListViewModule<T>
    where T : class
{
    protected BaseListViewModule()
    {
        PropertyDescriptors = new Dictionary<string, IPropertyDescriptor>();
    }

    public IDictionary<string, IPropertyDescriptor> PropertyDescriptors { get; }

    public abstract void Load(IListViewModuleBuilder<T> builder);
}
