﻿using ParkingLot.Application.Levels.ViewModels;
using ParkingLot.WebApp.ListView.Abstractions;
using ParkingLot.WebApp.ListView.Interfaces;

namespace ParkingLot.WebApp.ListView;

public class LevelsListViewModule : BaseListViewModule<LevelListItemVM>
{
    public override void Load(IListViewModuleBuilder<LevelListItemVM> builder)
    {
        builder.AutoRegisterAllProperties();
    }
}
