﻿using ParkingLot.Application.VehicleHistory.ViewModels;
using ParkingLot.WebApp.ListView.Abstractions;
using ParkingLot.WebApp.ListView.Interfaces;

namespace ParkingLot.WebApp.ListView;

public class VehicleHistoryListViewModule : BaseListViewModule<VehicleHistoryEntryVM>
{
    public override void Load(IListViewModuleBuilder<VehicleHistoryEntryVM> builder)
    {
        builder.AutoRegisterAllProperties();

        builder
            .Property(x => x.Vehicle.LicensePlate)
            .IsStringBased();
    }
}
