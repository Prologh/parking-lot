﻿using AutoMapper;
using ParkingLot.Application.Logging.InputModels;
using ParkingLot.Application.Logging.Requests;

namespace ParkingLot.WebApp.Mapping.Logging;

public class LoggingProfile : Profile
{
    public LoggingProfile()
    {
        CreateMap<DeleteRequestLogInputModel, DeleteRequestLogRequest>();
    }
}
