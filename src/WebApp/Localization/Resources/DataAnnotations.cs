﻿namespace ParkingLot.WebApp.Localization.Resources;

/// <summary>
/// Dummy class. Anchor for related resource files with the same name.
/// <para/>
/// This class cannot be inherited.
/// </summary>
public sealed class DataAnnotations
{

}
