﻿using Microsoft.AspNetCore.Routing;
using System;

namespace ParkingLot.Extensions.Routing;

/// <summary>
/// Provides extension methods for <see cref="RouteData"/>.
/// </summary>
public static class RouteDataExtensions
{
    /// <summary>
    /// Gets the action name from the route data dictionary.
    /// </summary>
    /// <param name="routeData">
    /// Current <see cref="RouteData"/> instance.
    /// </param>
    /// <returns>
    /// The name of action.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="routeData"/> is <see langword="null"/>.
    /// </exception>
    public static string GetAction(this RouteData routeData)
    {
        if (routeData == null)
        {
            throw new ArgumentNullException(nameof(routeData));
        }

        return routeData.Values["action"]?.ToString();
    }

    /// <summary>
    /// Gets the area name from the route data dictionary.
    /// </summary>
    /// <param name="routeData">
    /// Current <see cref="RouteData"/> instance.
    /// </param>
    /// <returns>
    /// The name of area.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="routeData"/> is <see langword="null"/>.
    /// </exception>
    public static string GetArea(this RouteData routeData)
    {
        if (routeData == null)
        {
            throw new ArgumentNullException(nameof(routeData));
        }

        return routeData.Values["area"]?.ToString();
    }

    /// <summary>
    /// Gets the controller name from the route data dictionary.
    /// </summary>
    /// <param name="routeData">
    /// Current <see cref="RouteData"/> instance.
    /// </param>
    /// <returns>
    /// The name of controller.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="routeData"/> is <see langword="null"/>.
    /// </exception>
    public static string GetController(this RouteData routeData)
    {
        if (routeData == null)
        {
            throw new ArgumentNullException(nameof(routeData));
        }

        return routeData.Values["controller"]?.ToString();
    }
}
