﻿using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace ParkingLot.Extensions.DependencyInjection;

/// <summary>
/// Provides extension methods for adding Fluent Validation to <see cref="IMvcBuilder"/>.
/// </summary>
public static class FluentValidationMvcBuilderExtensions
{
    /// <summary>
    /// Adds Fluent Validation services to the specified <see cref="IMvcBuilder"/>
    /// with registered validators from default validators assembly.
    /// </summary>
    /// <param name="builder">
    /// Current <see cref="IMvcBuilder"/> instance.
    /// </param>
    /// <returns>
    /// An instance of <see cref="IMvcBuilder"/> with added Fluent Validation
    /// and registered validator, so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="builder"/> is <see langword="null"/>.
    /// </exception>
    public static IMvcBuilder AddFluentValidationWithValidators(this IMvcBuilder builder)
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }

        return builder.AddFluentValidation(options =>
        {
            var assemblies = AppDomain.CurrentDomain
                .GetAssemblies()
                .Where(a => a.GetName().Name.StartsWith(
                    Constants.ApplicationName.Replace(" ", string.Empty)));

            options.RegisterValidatorsFromAssemblies(assemblies);
        });
    }

    /// <summary>
    /// Adds Fluent Validation services to the specified <see cref="IMvcBuilder"/>
    /// with registered validators from assembly containing <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T">
    /// The type which containing assembly will be scanned for any public,
    /// non-abstract types that inherit from <see cref="FluentValidation.AbstractValidator{T}"/>.
    /// </typeparam>
    /// <param name="builder">
    /// Current <see cref="IMvcBuilder"/> instance.
    /// </param>
    /// <returns>
    /// An instance of <see cref="IMvcBuilder"/> with added Fluent Validation
    /// and registered validator, so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="builder"/> is <see langword="null"/>.
    /// </exception>
    public static IMvcBuilder AddFluentValidationWithValidatorsFrom<T>(this IMvcBuilder builder)
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }

        return builder.AddFluentValidation(options =>
        {
            options.RegisterValidatorsFromAssemblyContaining<T>();
        });
    }

    /// <summary>
    /// Adds Fluent Validation services to the specified <see cref="IMvcBuilder"/>
    /// with registered validators from assembly containing provided
    /// <paramref name="type"/>.
    /// </summary>
    /// <param name="builder">
    /// Current <see cref="IMvcBuilder"/> instance.
    /// </param>
    /// <param name="type">
    /// The type which containing assembly will be scanned for any public,
    /// non-abstract types that inherit from <see cref="FluentValidation.AbstractValidator{T}"/>.
    /// </param>
    /// <returns>
    /// An instance of <see cref="IMvcBuilder"/> with added Fluent Validation
    /// and registered validator, so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="builder"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="type"/> is <see langword="null"/>.
    /// </exception>
    public static IMvcBuilder AddFluentValidationWithValidatorsFrom(this IMvcBuilder builder, Type type)
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }

        if (type == null)
        {
            throw new ArgumentNullException(nameof(type));
        }

        return builder.AddFluentValidation(options =>
        {
            options.RegisterValidatorsFromAssemblyContaining(type);
        });
    }
}
