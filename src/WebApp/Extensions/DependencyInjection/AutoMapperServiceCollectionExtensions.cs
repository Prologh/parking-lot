﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace ParkingLot.Extensions.DependencyInjection;

/// <summary>
/// Provides extension methods for adding <see cref="Mapper"/> to
/// <see cref="IServiceCollection"/>.
/// </summary>
public static class AutoMapperServiceCollectionExtensions
{
    /// <summary>
    /// Add <see cref="Mapper"/> functionalities to the <see cref="IServiceCollection"/>
    /// and adds predefined set of mapping profiles.
    /// </summary>
    /// <param name="services">
    /// The <see cref="IServiceCollection"/> instance.
    /// </param>
    /// <returns>
    /// Returns <see cref="IMvcBuilder"/> with added <see cref="Mapper"/>
    /// functionalities so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection AddAutoMapperWithProfiles(this IServiceCollection services)
    {
        if (services == null)
        {
            throw new ArgumentNullException(nameof(services));
        }

        var assemblies = AppDomain.CurrentDomain
            .GetAssemblies()
            .Where(a => a.GetName().Name.StartsWith(
                Constants.ApplicationName.Replace(" ", string.Empty)));

        return services.AddAutoMapper(assemblies);
    }
}
