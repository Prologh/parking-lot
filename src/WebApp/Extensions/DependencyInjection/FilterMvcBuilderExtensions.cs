﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ParkingLot.Extensions.DependencyInjection;

/// <summary>
/// Provides extension methods for adding new filters
/// to <see cref="Microsoft.AspNetCore.Mvc.MvcOptions"/>
/// inside <see cref="IMvcBuilder"/>.
/// </summary>
public static class FilterMvcBuilderExtensions
{
    /// <summary>
    /// Adds the <typeparamref name="TFilter"/> to the filters collection.
    /// </summary>
    /// <param name="builder">
    /// Current <see cref="IMvcBuilder"/> instance.
    /// </param>
    /// <returns>
    /// Returns <see cref="IMvcBuilder"/> with added
    /// <typeparamref name="TFilter"/> to the filters collection
    /// so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="builder"/> is <see langword="null"/>.
    /// </exception>
    public static IMvcBuilder AddFilter<TFilter>(this IMvcBuilder builder)
        where TFilter : IFilterMetadata
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }

        return builder.AddMvcOptions(options =>
        {
            options.Filters.Add<TFilter>();
        });
    }
}
