﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ParkingLot.Extensions.DependencyInjection;

/// <summary>
/// Provides extension methods for adding new model metadata details
/// providers to <see cref="Microsoft.AspNetCore.Mvc.MvcOptions"/>
/// inside <see cref="IMvcBuilder"/>.
/// </summary>
public static class ModelMetadataMvcBuilderExtensions
{
    /// <summary>
    /// Adds the <see cref="IMetadataDetailsProvider"/> to the model
    /// metadata details providers collection.
    /// </summary>
    /// <param name="builder">
    /// Current <see cref="IMvcBuilder"/> instance.
    /// </param>
    /// <param name="metadataProvider">
    /// The model metadata details provider.
    /// </param>
    /// <returns>
    /// Returns <see cref="IMvcBuilder"/> with added
    /// <see cref="IMetadataDetailsProvider"/> to the model metadata
    /// details providers so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="builder"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="metadataProvider"/> is <see langword="null"/>.
    /// </exception>
    public static IMvcBuilder AddModelMetadataDetailsProvider(this IMvcBuilder builder, IMetadataDetailsProvider metadataProvider)
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }

        if (metadataProvider == null)
        {
            throw new ArgumentNullException(nameof(metadataProvider));
        }

        return builder.AddMvcOptions(options =>
        {
            options.ModelMetadataDetailsProviders.Add(metadataProvider);
        });
    }
}
