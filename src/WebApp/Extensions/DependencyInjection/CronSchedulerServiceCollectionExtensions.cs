﻿using Microsoft.Extensions.DependencyInjection;
using ParkingLot.Extensions.Scheduler;
using ParkingLot.WebApp.Jobs;
using System;

namespace ParkingLot.Extensions.DependencyInjection;

public static class CronSchedulerServiceCollectionExtensions
{
    public static IServiceCollection AddCronSchedulerWithJobs(this IServiceCollection services)
    {
        if (services is null)
        {
            throw new ArgumentNullException(nameof(services));
        }

        services.AddScheduler(builder =>
        {
            builder.AddScopedJob<BatchSaveRequestLogsJob>(cron: "0/15 * * * * ?");
            builder.AddScopedJob<RemoveOldRequestLogsJob>(cron: "0 0 * ? * *");
        });

        return services;
    }
}
