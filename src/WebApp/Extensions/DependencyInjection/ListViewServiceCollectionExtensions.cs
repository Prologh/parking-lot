﻿using Fluorite.Extensions;
using Microsoft.Extensions.DependencyInjection;
using ParkingLot.Application.Expressions;
using ParkingLot.WebApp.ListView.Builders;
using ParkingLot.WebApp.ListView.Configuration;
using ParkingLot.WebApp.ListView.Interfaces;
using ParkingLot.WebApp.ListView.Providers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ParkingLot.Extensions.DependencyInjection;

/// <summary>
/// Provides extension methods for adding ListView to <see cref="IServiceCollection"/>.
/// </summary>
public static class ListViewServiceCollectionExtensions
{
    /// <summary>
    /// Adds ListView services with configuration and with modules from
    /// local assembly.
    /// </summary>
    /// <param name="services">
    /// The current instance of <see cref="IServiceCollection"/>.
    /// </param>
    /// <returns>
    /// An instance of <see cref="IServiceCollection"/> with ListView
    /// services added, so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection AddListView(this IServiceCollection services)
    {
        if (services is null)
        {
            throw new ArgumentNullException(nameof(services));
        }

        services.AddTransient<IPropertyBasisTypeProvider, PropertyBasisTypeProvider>();

        var moduleTypes = AppDomain.CurrentDomain
            .GetAssemblies()
            .Where(a => a.GetName().Name.StartsWith(
                Constants.ApplicationName.Replace(" ", string.Empty)))
            .SelectMany(a => a.GetTypes())
            .Where(t => !t.IsAbstract && t.GetInterfaces().Any(x => x == typeof(IListViewModule)))
            .ToList();

        services.AddSingleton<IListViewConfigurationProvider, ListViewConfigurationProvider>(serviceProvider =>
        {
            var propertyNameExtractor = serviceProvider.GetRequiredService<IPropertyNameExtractor>();
            var propertyInfoExtractor = serviceProvider.GetRequiredService<IPropertyInfoExtractor>();
            var propertyBasisTypeProvider = serviceProvider.GetRequiredService<IPropertyBasisTypeProvider>();

            var listViewConfiguration = BuildListViewConfiguration(moduleTypes, propertyNameExtractor, propertyInfoExtractor, propertyBasisTypeProvider);

            return new ListViewConfigurationProvider(listViewConfiguration);
        });

        return services;
    }

    private static IListViewConfiguration BuildListViewConfiguration(
        List<Type> moduleTypes,
        IPropertyNameExtractor propertyNameExtractor,
        IPropertyInfoExtractor propertyInfoExtractor,
        IPropertyBasisTypeProvider propertyBasisTypeProvider)
    {
        var propertyDescriptors = moduleTypes
            .Select(type =>
            {
                var module = CreateModuleInstance(type);
                var modelType = GetModelTypeFromModuleType(type);

                LoadModule(module, propertyNameExtractor, propertyInfoExtractor, propertyBasisTypeProvider, modelType);

                return (modelType, module.PropertyDescriptors);
            })
            .ToDictionary(tuple => tuple.modelType, tuple => (IReadOnlyDictionary<string, IPropertyDescriptor>)tuple.PropertyDescriptors.ToReadOnly())
            .ToReadOnly();

        return new ListViewConfiguration(propertyDescriptors);
    }

    private static Type GetModelTypeFromModuleType(Type type)
    {
        return type.BaseType.GenericTypeArguments[0];
    }

    private static IListViewModule CreateModuleInstance(Type type)
    {
        try
        {
            return Activator.CreateInstance(type) as IListViewModule;
        }
        catch (Exception exception)
        {
            throw new InvalidOperationException(
                $"Unable to create instance of {type}. " +
                $"Ensure that type provides parameterless constructor.",
                exception);
        }
    }

    private static void LoadModule(
        IListViewModule listViewModule,
        IPropertyNameExtractor propertyNameExtractor,
        IPropertyInfoExtractor propertyInfoExtractor,
        IPropertyBasisTypeProvider propertyBasisTypeProvider,
        Type modelType)
    {
        var genericListViewModuleInterfaceType = listViewModule
            .GetType()
            .GetInterfaces()
            .FirstOrDefault(i =>
                i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IListViewModule<>));

        if (genericListViewModuleInterfaceType is null)
        {
            throw new InvalidOperationException(
                "Not generic list view modules are not supported. " +
                "Type " + listViewModule.GetType().Name);
        }

        var builderType = typeof(ListViewModuleBuilder<>).MakeGenericType(modelType);
        var builder = Activator.CreateInstance(builderType, listViewModule, propertyNameExtractor, propertyInfoExtractor, propertyBasisTypeProvider);
        var method = genericListViewModuleInterfaceType.GetMethod(nameof(IListViewModule<object>.Load));

        method.Invoke(listViewModule, new[] { builder });
    }
}
