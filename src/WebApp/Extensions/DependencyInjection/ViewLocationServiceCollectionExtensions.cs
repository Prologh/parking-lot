﻿using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace ParkingLot.Extensions.DependencyInjection;

/// <summary>
/// Provides extension methods for configuring the <see cref="RazorViewEngineOptions"/>
/// within <see cref="IServiceCollection"/>.
/// </summary>
public static class ViewLocationServiceCollectionExtensions
{
    /// <summary>
    /// Adds view location expander to the
    /// <see cref="RazorViewEngineOptions.ViewLocationExpanders"/>
    /// collection.
    /// </summary>
    /// <param name="services">
    /// Current <see cref="IServiceCollection"/> instance.
    /// </param>
    /// <param name="viewLocationExpander">
    /// An instance of <see cref="IViewLocationExpander"/>.
    /// </param>
    /// <returns>
    /// The <see cref="IServiceCollection"/> so that additional calls can
    /// be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="viewLocationExpander"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection AddViewLocationExpander(this IServiceCollection services, IViewLocationExpander viewLocationExpander)
    {
        if (services == null)
        {
            throw new ArgumentNullException(nameof(services));
        }

        if (viewLocationExpander == null)
        {
            throw new ArgumentNullException(nameof(viewLocationExpander));
        }

        return services.Configure<RazorViewEngineOptions>(options =>
        {
            options.ViewLocationExpanders.Add(viewLocationExpander);
        });
    }

    /// <summary>
    /// Adds view location expander to the
    /// <see cref="RazorViewEngineOptions.ViewLocationExpanders"/>
    /// collection.
    /// </summary>
    /// <typeparam name="TLocationExpander">
    /// The type implementing <see cref="IViewLocationExpander"/>.
    /// </typeparam>
    /// <param name="services">
    /// Current <see cref="IServiceCollection"/> instance.
    /// </param>
    /// <returns>
    /// The <see cref="IServiceCollection"/> so that additional calls can
    /// be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="InvalidOperationException">
    /// Unable to add/obtain view location expander to/from
    /// current service collection.
    /// </exception>
    public static IServiceCollection AddViewLocationExpander<TLocationExpander>(this IServiceCollection services)
        where TLocationExpander : class, IViewLocationExpander
    {
        if (services == null)
        {
            throw new ArgumentNullException(nameof(services));
        }

        services.AddScoped<IViewLocationExpander, TLocationExpander>();
        var viewLocationExpander = GetViewLocationExpander<TLocationExpander>(services);

        if (viewLocationExpander == null)
        {
            throw new InvalidOperationException(
                $"Unable to add/obtain view location expander of type " +
                $"{typeof(TLocationExpander).Name} from current service collection.");
        }

        return services.Configure<RazorViewEngineOptions>(options =>
        {
            options.ViewLocationExpanders.Add(viewLocationExpander);
        });
    }

    private static TLocationExpander GetViewLocationExpander<TLocationExpander>(this IServiceCollection services)
        where TLocationExpander : class, IViewLocationExpander
    {
        using (var serviceProvider = services.BuildServiceProvider())
        {
            return serviceProvider
                .GetServices<IViewLocationExpander>()
                .OfType<TLocationExpander>()
                .FirstOrDefault();
        }
    }
}
