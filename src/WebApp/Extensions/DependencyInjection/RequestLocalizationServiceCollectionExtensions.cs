﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Globalization;

namespace ParkingLot.Extensions.DependencyInjection;

/// <summary>
/// Provides extension methods for configuring the <see cref="RequestLocalizationOptions"/>
/// from <see cref="IServiceCollection"/>.
/// </summary>
public static class RequestLocalizationServiceCollectionExtensions
{
    /// <summary>
    /// Configures the <see cref="RequestLocalizationOptions"/> with
    /// default values.
    /// </summary>
    /// <param name="services">
    /// Current <see cref="IServiceCollection"/> instance.
    /// </param>
    /// <returns>
    /// The <see cref="IServiceCollection"/> so that additional calls can
    /// be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection ConfigureRequestLocalizationOptions(this IServiceCollection services)
    {
        if (services == null)
        {
            throw new ArgumentNullException(nameof(services));
        }

        services.Configure<RequestLocalizationOptions>(opts =>
        {
            var supportedCultures = new CultureInfo[]
            {
                new CultureInfo("en-GB"),
                new CultureInfo("en-US"),
                new CultureInfo("en"),
                new CultureInfo("pl-PL"),
                new CultureInfo("pl"),
            };

            opts.DefaultRequestCulture = new RequestCulture("en");
            opts.SupportedCultures = supportedCultures;
            opts.SupportedUICultures = supportedCultures;
        });

        return services;
    }

    /// <summary>
    /// Configures the <see cref="RequestLocalizationOptions"/>.
    /// </summary>
    /// <param name="services">
    /// Current <see cref="IServiceCollection"/> instance.
    /// </param>
    /// <param name="configureOptions">
    /// An action used to configure <see cref="RequestLocalizationOptions"/>.
    /// </param>
    /// <returns>
    /// The <see cref="IServiceCollection"/> so that additional calls can
    /// be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="configureOptions"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection ConfigureRequestLocalizationOptions(this IServiceCollection services, Action<RequestLocalizationOptions> configureOptions)
    {
        if (services == null)
        {
            throw new ArgumentNullException(nameof(services));
        }

        if (configureOptions == null)
        {
            throw new ArgumentNullException(nameof(configureOptions));
        }

        services.Configure(configureOptions);

        return services;
    }
}
