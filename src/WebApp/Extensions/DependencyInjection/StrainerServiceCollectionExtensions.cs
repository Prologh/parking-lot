﻿using Fluorite.Extensions.DependencyInjection;
using Fluorite.Strainer.Services.Modules;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace ParkingLot.Extensions.DependencyInjection;

/// <summary>
/// Provides extension methods for adding Strainer to <see cref="IServiceCollection"/>.
/// </summary>
public static class StrainerServiceCollectionExtensions
{
    /// <summary>
    /// Adds Strainer services with configuration and with modules from
    /// local assembly.
    /// </summary>
    /// <param name="services">
    /// The current instance of <see cref="IServiceCollection"/>.
    /// </param>
    /// <param name="configuration">
    /// The configuration used to set Strainer options.
    /// </param>
    /// <returns>
    /// An instance of <see cref="IServiceCollection"/> with Stariner
    /// services added, so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="configuration"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection AddStrainerWithLocalModules(
        this IServiceCollection services,
        IConfiguration configuration)
    {
        if (services is null)
        {
            throw new ArgumentNullException(nameof(services));
        }

        if (configuration is null)
        {
            throw new ArgumentNullException(nameof(configuration));
        }

        var strainerModuleTypes = AppDomain.CurrentDomain
            .GetAssemblies()
            .Where(a => a.GetName().Name.StartsWith(
                Constants.ApplicationName.Replace(" ", string.Empty)))
            .SelectMany(a => a.GetTypes())
            .Where(t => !t.IsAbstract && t.IsSubclassOf(typeof(StrainerModule)))
            .ToList();

        services.AddStrainer(configuration, strainerModuleTypes);

        return services;
    }
}
