﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using System;
using System.Reflection;

namespace ParkingLot.Extensions.DependencyInjection;

/// <summary>
/// Provides extension methods for configuring
/// <see cref="Microsoft.AspNetCore.Mvc.DataAnnotations.MvcDataAnnotationsLocalizationOptions"/>
/// from within <see cref="IMvcBuilder"/>.
/// </summary>
public static class DataAnnotationsLocalizationServiceCollectionExtensions
{
    /// <summary>
    /// Adds custom data annotations localization builded on top of an anchor type.
    /// </summary>
    /// <typeparam name="T">
    /// The type of an anchor class which points the location and assembly
    /// of resource files.
    /// </typeparam>
    /// <param name="builder">
    /// Current <see cref="IMvcBuilder"/> instance.
    /// </param>
    /// <returns>
    /// The <see cref="IMvcBuilder"/> so that additional calls can
    /// be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="builder"/> is <see langword="null"/>.
    /// </exception>
    public static IMvcBuilder AddDataAnnotationsLocalization<T>(this IMvcBuilder builder)
    {
        return builder.AddDataAnnotationsLocalization(typeof(T));
    }

    /// <summary>
    /// Adds custom data annotations localization builded on top of an anchor type.
    /// </summary>
    /// <param name="builder">
    /// Current <see cref="IMvcBuilder"/> instance.
    /// </param>
    /// <param name="anchorType">
    /// The type of an anchor class which points the location and assembly
    /// of resource files.
    /// </param>
    /// <returns>
    /// The <see cref="IMvcBuilder"/> so that additional calls can
    /// be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="builder"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="anchorType"/> is <see langword="null"/>.
    /// </exception>
    public static IMvcBuilder AddDataAnnotationsLocalization(this IMvcBuilder builder, Type anchorType)
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }

        if (anchorType == null)
        {
            throw new ArgumentNullException(nameof(anchorType));
        }

        builder.AddDataAnnotationsLocalization(options =>
        {
            var assemblyName = new AssemblyName(anchorType.GetTypeInfo().Assembly.FullName);
            var factory = builder.Services.BuildServiceProvider().GetService<IStringLocalizerFactory>();
            var localizer = factory.Create(anchorType.Name, assemblyName.Name);
            options.DataAnnotationLocalizerProvider = (type, f) => localizer;
        });

        return builder;
    }
}
