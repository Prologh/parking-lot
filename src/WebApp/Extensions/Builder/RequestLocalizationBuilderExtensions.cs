﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;

namespace ParkingLot.Extensions.Builder;

/// <summary>
/// Contains extensions methods for adding request localization to <see cref="IApplicationBuilder"/>.
/// </summary>
public static class RequestLocalizationBuilderExtensions
{
    /// <summary>
    /// Adds the <see cref="RequestLocalizationMiddleware"/> to
    /// automatically set culture information for requests based on
    /// information provided by the client.
    /// </summary>
    /// <param name="builder">
    /// Current <see cref="IApplicationBuilder"/> instance.
    /// </param>
    /// <returns>
    /// Returns <see cref="IApplicationBuilder"/> with added
    /// <see cref="RequestLocalizationMiddleware"/> to the request pipeline.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="builder"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="InvalidOperationException">
    /// No <see cref="RequestLocalizationOptions"/> registered in service
    /// collection.
    /// </exception>
    public static IApplicationBuilder UseDefaultRequestLocalization(this IApplicationBuilder builder)
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }

        var localizationOptions = builder.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
        if (localizationOptions == null)
        {
            throw new InvalidOperationException(
                $"No registered instance of {nameof(RequestLocalizationOptions)} " +
                $"found in the application service collection.\n" +
                $"In order to use default request localization, please " +
                $"register the {nameof(RequestLocalizationOptions)} " +
                $"within the service collection.");
        }

        return builder.UseRequestLocalization(localizationOptions.Value);
    }
}
