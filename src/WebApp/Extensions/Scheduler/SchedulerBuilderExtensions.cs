﻿using CronScheduler.Extensions.Scheduler;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ParkingLot.Extensions.Scheduler;

public static class SchedulerBuilderExtensions
{
    public static IServiceCollection AddScopedJob<TJob>(this SchedulerBuilder schedulerBuilder, string cron)
        where TJob : class, IScheduledJob
    {
        if (schedulerBuilder is null)
        {
            throw new ArgumentNullException(nameof(schedulerBuilder));
        }

        if (string.IsNullOrWhiteSpace(cron))
        {
            throw new ArgumentException($"'{nameof(cron)}' cannot be null or whitespace", nameof(cron));
        }

        schedulerBuilder.Services.AddScoped<TJob>();

        return schedulerBuilder.AddJob(
            factory: serviceProvider =>
            {
                var scope = serviceProvider.CreateScope();

                return scope.ServiceProvider.GetRequiredService<TJob>();
            },
            configure: opts =>
            {
                opts.CronSchedule = cron;
                opts.CronTimeZone = "UTC";
            });
    }
}
