﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ParkingLot.Persistance.Seeding;
using Serilog;
using System;

namespace ParkingLot.WebApp;

public class Program
{
    public static void Main(string[] args)
    {
        var appsettingsConfiguration = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddJsonFile(
                $"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json",
                optional: true,
                reloadOnChange: true)
            .Build();

        Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(appsettingsConfiguration)
            .CreateLogger();

        Log.Information("Starting web host.");

        try
        {
            using var host = CreateHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;

                try
                {
                    var initializer = serviceProvider.GetRequiredService<DatabaseInitializer>();

                    initializer.Initialize(migrate: false, ensureCreated: true);
                }
                catch (Exception exception)
                {
                    Log.Error(exception, "An error occurred while seeding the database.");

                    throw;
                }
            }

            host.Run();
        }
        catch (Exception exception)
        {
            Log.Fatal(exception, "Host terminated unexpectedly.");
        }
        finally
        {
            Log.CloseAndFlush();
        }
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .UseSerilog()
            .ConfigureWebHostDefaults(web =>
            {
                web.UseUrls("http://*:80/", "https://*:443/");
                web.UseStartup<Startup>();
            });
}
