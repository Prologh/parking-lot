﻿using CronScheduler.Extensions.Scheduler;
using Microsoft.Extensions.Logging;
using ParkingLot.Application;
using ParkingLot.Application.Logging.Repositories;
using ParkingLot.Application.Logging.Stores;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ParkingLot.WebApp.Jobs;

public class BatchSaveRequestLogsJob : IScheduledJob
{
    private readonly ILogger<BatchSaveRequestLogsJob> _logger;
    private readonly IRequestLogStore _requestLogStore;
    private readonly IRequestLogsRepository _requestLogsRepository;
    private readonly IUnitOfWork _unitOfWork;

    public BatchSaveRequestLogsJob(
        ILogger<BatchSaveRequestLogsJob> logger,
        IRequestLogStore requestLogStore,
        IRequestLogsRepository requestLogsRepository,
        IUnitOfWork unitOfWork)
    {
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        _requestLogStore = requestLogStore ?? throw new ArgumentNullException(nameof(requestLogStore));
        _requestLogsRepository = requestLogsRepository ?? throw new ArgumentNullException(nameof(requestLogsRepository));
        _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
    }

    public string Name => nameof(BatchSaveRequestLogsJob);

    public async Task ExecuteAsync(CancellationToken cancellationToken)
    {
        _logger.LogDebug("Batch adding accumulated request logs.");

        var requestLogs = _requestLogStore.GetAll();
        if (requestLogs.Count == 0)
        {
            _logger.LogDebug("Found no accumulated request logs to add.");

            return;
        }

        _logger.LogDebug("Adding {0} accumulated request logs.", requestLogs.Count);

        _requestLogsRepository.AddRange(requestLogs);

        var requestLogsAffected = await _unitOfWork.SaveChangesAsync();

        _requestLogStore.Clear();

        _logger.LogDebug("Request logs addded: {0}", requestLogsAffected);
    }
}
