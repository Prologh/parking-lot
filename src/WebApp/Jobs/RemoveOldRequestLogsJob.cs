﻿using CronScheduler.Extensions.Scheduler;
using Microsoft.Extensions.Logging;
using ParkingLot.Application;
using ParkingLot.Application.Logging.Queries;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ParkingLot.WebApp.Jobs;

public class RemoveOldRequestLogsJob : IScheduledJob
{
    private const int RequestLogsAllowedAgeInDays = 1;

    private readonly IUnitOfWork _unitOfWork;
    private readonly IRemoveOldRequestLogsQuery _removeOldRequestLogsQuery;
    private readonly ILogger<RemoveOldRequestLogsJob> _logger;

    public RemoveOldRequestLogsJob(
        IUnitOfWork unitOfWork,
        IRemoveOldRequestLogsQuery removeOldRequestLogsQuery,
        ILogger<RemoveOldRequestLogsJob> logger)
    {
        _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        _removeOldRequestLogsQuery = removeOldRequestLogsQuery ?? throw new ArgumentNullException(nameof(removeOldRequestLogsQuery));
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    }

    public string Name => nameof(RemoveOldRequestLogsJob);

    public async Task ExecuteAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Removing request logs older than {0} days.", RequestLogsAllowedAgeInDays);

        await _removeOldRequestLogsQuery.RemoveAsync(days: RequestLogsAllowedAgeInDays);

        var requestLogsAffected = await _unitOfWork.SaveChangesAsync();

        _logger.LogInformation("Request logs removed: {0}", requestLogsAffected);
    }
}
