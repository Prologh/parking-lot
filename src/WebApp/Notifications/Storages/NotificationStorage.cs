﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;
using ParkingLot.Application.Notifications.Models;
using ParkingLot.Application.Notifications.Storages.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ParkingLot.Application.Notifications.Storages;

public class NotificationStorage : INotificationStorage
{
    public const string NotificationSessionKey = "Notifications";

    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly ITempDataDictionaryFactory _tempDataDictionaryFactory;

    public NotificationStorage(
        IHttpContextAccessor httpContextAccessor,
        ITempDataDictionaryFactory tempDataDictionaryFactory)
    {
        _httpContextAccessor = httpContextAccessor;
        _tempDataDictionaryFactory = tempDataDictionaryFactory;
        TempData = _tempDataDictionaryFactory.GetTempData(_httpContextAccessor.HttpContext);
    }

    protected ITempDataDictionary TempData { get; }

    public bool HasAnyNotifications()
    {
        return TempData.ContainsKey(NotificationSessionKey);
    }

    public INotification GetNotification()
    {
        return GetNotifications().FirstOrDefault();
    }

    public IList<INotification> GetNotifications()
    {
        if (!HasAnyNotifications())
        {
            return null;
        }
        else
        {
            var tempDataEntry = TempData[NotificationSessionKey];

            return JsonConvert.DeserializeObject<Notification[]>(tempDataEntry.ToString());
        }
    }

    public bool RemoveNotifications()
    {
        return TempData.Remove(NotificationSessionKey);
    }

    public void SetNotification(INotification notification)
    {
        if (notification == null)
        {
            throw new ArgumentNullException(nameof(notification));
        }

        SetNotifications(new List<INotification> { notification });
    }

    public void SetNotifications(IEnumerable<INotification> notifications)
    {
        if (notifications == null)
        {
            throw new ArgumentNullException(nameof(notifications));
        }

        RemoveNotifications();
        TempData.Add(NotificationSessionKey, JsonConvert.SerializeObject(notifications));
    }
}
