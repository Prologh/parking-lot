﻿using Microsoft.AspNetCore.Http;
using ParkingLot.Domain.Logging;
using System;

namespace ParkingLot.WebApp.Logging.Builders.Abstractions;

/// <summary>
/// Provides means of formatting request logs.
/// </summary>
public interface IRequestLogBuilder
{
    /// <summary>
    /// Creates a single request log from provided data.
    /// </summary>
    /// <param name="httpContext">
    /// The <see cref="HttpContext"/> providing information about HTTP request
    /// and response.
    /// </param>
    /// <param name="created">
    /// Date and time when the request started.
    /// </param>
    /// <param name="duration">
    /// The duration of request execution.
    /// </param>
    /// <returns>
    /// An instance of <see cref="RequestLog"/>.
    /// </returns>
    RequestLog BuildRequestLog(HttpContext httpContext, DateTimeOffset created, TimeSpan duration);
}
