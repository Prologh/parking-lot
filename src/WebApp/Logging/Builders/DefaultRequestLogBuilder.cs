﻿using Microsoft.AspNetCore.Http;
using ParkingLot.Domain.Logging;
using ParkingLot.WebApp.Logging.Builders.Abstractions;
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace ParkingLot.WebApp.Logging.Builders;

/// <summary>
/// Provides means of formatting request logs.
/// </summary>
public class DefaultRequestLogBuilder : IRequestLogBuilder
{
    /// <summary>
    /// Initializes a new instance of the <see cref="DefaultRequestLogBuilder"/>
    /// class.
    /// </summary>
    public DefaultRequestLogBuilder()
    {

    }

    /// <summary>
    /// Creates a single request log from provided data.
    /// </summary>
    /// <param name="httpContext">
    /// The <see cref="HttpContext"/> providing information about HTTP request
    /// and response.
    /// </param>
    /// <param name="created">
    /// Date and time when the request started.
    /// </param>
    /// <param name="duration">
    /// The duration of request execution.
    /// </param>
    /// <returns>
    /// An instance of <see cref="RequestLog"/>.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="httpContext"/> is <see langword="null"/>.
    /// </exception>
    public RequestLog BuildRequestLog(HttpContext httpContext, DateTimeOffset created, TimeSpan duration)
    {
        if (httpContext == null)
        {
            throw new ArgumentNullException(nameof(httpContext));
        }

        var ipAddress = ConvertIPAddress(httpContext.Connection.RemoteIpAddress);
        var requestHeadersString = ConvertHeaderDictionaryToSingleString(httpContext.Request.Headers);
        var responseHeadersString = ConvertHeaderDictionaryToSingleString(httpContext.Response.Headers);

        var requestLog = new RequestLog
        {
            Duration = duration.TotalMilliseconds,
            HttpMethod = httpContext.Request.Method,
            HttpStatusCode = httpContext.Response.StatusCode,
            IPAddress = ipAddress,
            IsHttps = httpContext.Request.IsHttps,
            Query = httpContext.Request.QueryString.Value,
            RequestHeaders = requestHeadersString,
            ResponseHeaders = responseHeadersString,
            Route = httpContext.Request.Path,
        };

        requestLog.SetCreationDate(DateTimeOffset.Now);

        return requestLog;
    }

    private string ConvertIPAddress(IPAddress ipAdress)
    {
        if (ipAdress == null)
        {
            return null;
        }
        else
        {
            // Map only if not IPv4.
            return ipAdress.AddressFamily == AddressFamily.InterNetwork
                ? ipAdress.ToString()
                : ipAdress.MapToIPv4().ToString();
        }
    }

    private string ConvertHeaderDictionaryToSingleString(IHeaderDictionary stringValues)
    {
        return string.Join(
            Environment.NewLine,
            stringValues.Select(h => $"{h.Key}={string.Join(",", h.Value)}"));
    }
}
