﻿using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ParkingLot.Application;
using ParkingLot.Application.Exceptions.Providers;
using ParkingLot.Application.Expressions;
using ParkingLot.Application.Http.Humanizers;
using ParkingLot.Application.Http.Providers;
using ParkingLot.Application.Http.Validators;
using ParkingLot.Application.Levels.Modules;
using ParkingLot.Application.Levels.Queries;
using ParkingLot.Application.Levels.Repositories;
using ParkingLot.Application.Logging.Modules;
using ParkingLot.Application.Logging.Queries;
using ParkingLot.Application.Logging.Repositories;
using ParkingLot.Application.Logging.Stores;
using ParkingLot.Application.Notifications.Storages;
using ParkingLot.Application.Notifications.Storages.Abstractions;
using ParkingLot.Application.Pagination.Managers;
using ParkingLot.Application.Queries;
using ParkingLot.Application.Repositories;
using ParkingLot.Application.SelectLists.Models;
using ParkingLot.Application.SelectLists.Queries;
using ParkingLot.Application.SpaceHistory.Modules;
using ParkingLot.Application.SpaceHistory.Queries;
using ParkingLot.Application.SpaceHistory.Repositories;
using ParkingLot.Application.Spaces.Modules;
using ParkingLot.Application.Spaces.Queries;
using ParkingLot.Application.Spaces.Repositories;
using ParkingLot.Application.Strainer.Filtering;
using ParkingLot.Application.Strainer.Mapping;
using ParkingLot.Application.Strainer.Sorting.Services;
using ParkingLot.Application.VehicleHistory.Modules;
using ParkingLot.Application.VehicleHistory.Repositories;
using ParkingLot.Application.Vehicles.Modules;
using ParkingLot.Application.Vehicles.Queries;
using ParkingLot.Application.Vehicles.Repositories;
using ParkingLot.Domain.Levels;
using ParkingLot.Extensions.Builder;
using ParkingLot.Extensions.DependencyInjection;
using ParkingLot.Infrastructure.Http.Humanizers;
using ParkingLot.Infrastructure.Http.Validators;
using ParkingLot.Infrastructure.Strainer.Filtering;
using ParkingLot.Infrastructure.Strainer.Mapping;
using ParkingLot.Persistance;
using ParkingLot.Persistance.Levels.Queries;
using ParkingLot.Persistance.Levels.Repositories;
using ParkingLot.Persistance.Logging.Queries;
using ParkingLot.Persistance.Logging.Repositories;
using ParkingLot.Persistance.Logging.Stores;
using ParkingLot.Persistance.Pagination.Managers;
using ParkingLot.Persistance.Queries;
using ParkingLot.Persistance.Repositories;
using ParkingLot.Persistance.Seeding;
using ParkingLot.Persistance.SpaceHistory.Queries;
using ParkingLot.Persistance.SpaceHistory.Repositories;
using ParkingLot.Persistance.Spaces.Queries;
using ParkingLot.Persistance.Spaces.Repositories;
using ParkingLot.Persistance.VehicleHistory.Repositories;
using ParkingLot.Persistance.Vehicles.Queries;
using ParkingLot.Persistance.Vehicles.Repositories;
using ParkingLot.WebApp.Http.Providers;
using ParkingLot.WebApp.ListView;
using ParkingLot.WebApp.ListView.Interfaces;
using ParkingLot.WebApp.Localization.Resources;
using ParkingLot.WebApp.Logging.Builders;
using ParkingLot.WebApp.Logging.Builders.Abstractions;
using ParkingLot.WebApp.Middleware.Logging;
using ParkingLot.WebApp.Middleware.Robots;
using ParkingLot.WebApp.Middleware.SiteMap;
using ParkingLot.WebApp.Mvc.Filters;
using ParkingLot.WebApp.Mvc.Razor;
using ParkingLot.WebApp.Services;
using ParkingLot.WebApp.Services.Abstractions;
using Serilog;
using System;

namespace ParkingLot.WebApp;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddMvc(options => options.EnableEndpointRouting = false)
            .AddFilter<InvalidModelStateActionFilter>()
            .AddViewLocalization()
            .AddDataAnnotationsLocalization<DataAnnotations>()
            .AddFluentValidationWithValidators();

        // Add AutoMapper.
        services.AddAutoMapperWithProfiles();

        // Enable application localization.
        services.AddLocalization(options => options.ResourcesPath = "Resources");

        // Configure request localization options.
        services.ConfigureRequestLocalizationOptions();

        // Add additional view location paths.
        services.AddViewLocationExpander<ViewLocationExpander>();

        // Enable session.
        services.AddSession(options =>
        {
            options.IdleTimeout = TimeSpan.FromSeconds(10);
            options.Cookie.HttpOnly = true;
        });

        // Add action context accessor.
        services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

        // Add robots options.
        services.Configure<RobotsOptions>(Configuration.GetSection("Robots"));

        //Configure HTTP ==> HTTPS redirection service.
        services.AddHttpsRedirection(options =>
        {
            options.HttpsPort = 443;
        });

        // Add database context.
        services.AddDbContext<ApplicationDbContext>(options =>
        {
            options.UseInMemoryDatabase("InMemoryDatabase");
            //options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
        });

        // Add Database initializer service.
        services.AddScoped<DatabaseInitializer>();

        // Configure request logging.
        services.Configure<RequestLoggingOptions>(Configuration.GetSection("RequestsLogging"));

        // Add Strainer (filtering, sorting and pagination support).
        services.AddStrainerWithLocalModules(Configuration.GetSection("Strainer"));
        services.AddScoped<ISortExpressionProvider, ParkingLotSortExpressionProvider>();
        services.AddScoped<IStrainerTypeMapper, StrainerTypeMapper>();
        services.AddScoped<IStrainerFiltersProvider, StrainerFiltersProvider>();
        services.AddScoped<IStrainerFiltersDecoder, StrainerFiltersDecoder>();
        services.AddTransient<IStrainerFilterOperatorSubgroupBuilder, StrainerFilterOperatorSubgroupBuilder>();
        services.AddSingleton<IStrainerFilterOperatorSubgroupStore, InMemoryStrainerFilterOperatorSubgroupStore>();

        // Strainer in-memory stored type mapper configuration.
        services.AddSingleton<IStrainerTypeMapperConfiguration, InMemoryStrainerTypeMapperConfiguration>();

        // Strainer type mapper modules.
        services.AddTransient<IStrainerTypeMapperModule, LevelsStrainerTypeMapperModule>();
        services.AddTransient<IStrainerTypeMapperModule, RequestLogsStrainerTypeMapperModule>();
        services.AddTransient<IStrainerTypeMapperModule, SpaceHistoryStrainerTypeMapperModule>();
        services.AddTransient<IStrainerTypeMapperModule, SpacesStrainerTypeMapperModule>();
        services.AddTransient<IStrainerTypeMapperModule, VehicleHistoryStrainerTypeMapperModule>();
        services.AddTransient<IStrainerTypeMapperModule, VehiclesStrainerTypeMapperModule>();

        // Add ListView
        services.AddListView();

        // Add ListView modules
        services.AddTransient<IListViewModule, LevelsListViewModule>();
        services.AddTransient<IListViewModule, SpacesListViewModule>();
        services.AddTransient<IListViewModule, LoggingListViewModule>();
        services.AddTransient<IListViewModule, SpaceHistoryListViewModule>();
        services.AddTransient<IListViewModule, VehicleHistoryListViewModule>();
        services.AddTransient<IListViewModule, VehiclesListViewModule>();

        // Add helpers.
        services.AddScoped<IExceptionMessageProvider, ExceptionMessageProvider>();
        services.AddScoped<IHttpStatusCodeHumanizer, HttpStatusCodeHumanizer>();
        services.AddScoped<IHttpStatusCodeMessageProvider, HttpStatusCodeMessageProvider>();
        services.AddScoped<IHttpStatusCodeValidator, HttpStatusCodeValidator>();
        services.AddScoped<INotificationStorage, NotificationStorage>();
        services.AddScoped<IRequestLogBuilder, DefaultRequestLogBuilder>();
        services.AddScoped<ISelectListProvider<Level>, SelectListProvider<Level, SelectListItem>>();

        // Add reflection helpers.
        services.AddTransient<IPropertyNameExtractor, PropertyNameExtractor>();
        services.AddTransient<IPropertyInfoExtractor, PropertyInfoExtractor>();

        // Add generic data layer services.
        services.AddScoped<IUnitOfWork, UnitOfWork>();
        services.AddScoped(typeof(IPaginationManager<,>), typeof(PaginationManager<,>));

        // Add repositories.
        services.AddScoped(typeof(IReadOnlyRepository<>), typeof(GenericReadOnlyRepository<>));
        services.AddScoped(typeof(IRepository<>), typeof(GenericRepository<>));
        services.AddScoped<ILevelsRepository, LevelsRepository>();
        services.AddScoped<IRequestLogsRepository, RequestLogsRepository>();
        services.AddScoped<ISpacesRepository, SpacesRepository>();
        services.AddScoped<ISpaceHistoryEntriesRepository, SpaceHistoryEntriesRepository>();
        services.AddScoped<IVehiclesRepository, VehiclesRepository>();
        services.AddScoped<IVehicleHistoryEntriesRepository, VehicleHistoryEntriesRepository>();

        // Add queries.
        services.AddScoped<IGetRequestLogDetailsQuery, GetRequestLogDetailsQuery>();
        services.AddScoped<IGetUpdateLevelInputModelQuery, GetUpdateLevelInputModelQuery>();
        services.AddScoped<ICheckAllLevelSpacesAreUnoccupiedQuery, CheckAllLevelSpacesAreUnoccupiedQuery>();
        services.AddScoped<IGetLevelMetadatasQuery, GetLevelMetadatasQuery>();
        services.AddScoped<IGetLevelDetailsQuery, GetLevelDetailsQuery>();
        services.AddScoped<ISelectListQuery<Level, SelectListItem>, LevelsSelectListQuery>();
        services.AddScoped<ICountRequestLogsQuery, GetRequestLogCountMetadataQuery>();
        services.AddScoped<IGetRequestLogDetailsQuery, GetRequestLogDetailsQuery>();
        services.AddScoped<IGetRequestLogsLineChartQuery, GetRequestLogsLineChartQuery>();
        services.AddScoped<IGetSpaceDetailsQuery, GetSpaceDetailsQuery>();
        services.AddScoped<IGetHighestSpaceNumberQuery, GetHighestSpaceNumberQuery>();
        services.AddScoped<IGetUpdateSpaceInputModelQuery, GetUpdateSpaceInputModelQuery>();
        services.AddScoped<ICheckSpaceWithNumberExistsQuery, CheckSpaceWithNumberExistsQuery>();
        services.AddScoped<ICheckSpaceIsCurrentlyOccupiedQuery, CheckSpaceIsCurrentlyOccupiedQuery>();
        services.AddScoped<IRemoveOldRequestLogsQuery, RemoveOldRequestLogsQuery>();
        services.AddScoped<IGetVehicleDetailsQuery, GetVehicleDetailsQuery>();
        services.AddScoped<IGetVehicleHistoryChartQuery, GetVehicleHistoryChartQuery>();
        services.AddScoped<IGetMostPopularSpacesQuery, GetMostPopularSpacesQuery>();

        // Add read query sources.
        services.AddScoped(typeof(IReadQuerySource<>), typeof(GenericReadQuerySource<>));

        // Add in memory stores.
        services.AddSingleton<IRequestLogStore, InMemoryRequestLogStore>();

        // Add MediatR.
        services.AddMediatR(x => x.RegisterServicesFromAssemblyContaining<IUnitOfWork>());

        // Add CronScheduler with jobs.
        services.AddCronSchedulerWithJobs();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseExceptionHandler("/Error");
            app.UseHsts();
        }

        app.UseMiddleware<RobotsMiddleware>();
        app.UseMiddleware<RequestLoggingMiddleware>();
        app.UseMiddleware<SiteMapMiddleware>();
        app.UseHttpsRedirection();
        app.UseStatusCodePagesWithReExecute("/Error/{0}");
        app.UseStaticFiles(new StaticFileOptions
        {
            OnPrepareResponse = context =>
            {
                context.Context.Items.Add(RequestLoggingMiddleware.StaticFilesResponseKey, bool.TrueString);
            },
        });
        app.UseSerilogRequestLogging();
        app.UseSession();
        app.UseDefaultRequestLocalization();
        app.UseMvcWithDefaultRoute();
    }
}
