﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ParkingLot.Application.SelectLists.Queries;
using ParkingLot.Domain.Abstractions;
using ParkingLot.WebApp.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ParkingLot.WebApp.Services;

public class SelectListProvider<TEntity, TSelectListItem> : ISelectListProvider<TEntity>
    where TEntity : BaseEntity
    where TSelectListItem : Application.SelectLists.Models.SelectListItem
{
    private readonly ISelectListQuery<TEntity, TSelectListItem> _selectListQuery;

    public SelectListProvider(
        ISelectListQuery<TEntity, TSelectListItem> autoCompleteQuery)
    {
        _selectListQuery = autoCompleteQuery ?? throw new ArgumentNullException(nameof(autoCompleteQuery));
    }

    public async Task<IEnumerable<SelectListItem>> GetSelectListAsync()
    {
        var items = await _selectListQuery.GetSelectListItemsAsync();

        return new SelectList(
            items,
            nameof(Application.SelectLists.Models.SelectListItem.Value),
            nameof(Application.SelectLists.Models.SelectListItem.Text));
    }

    public async Task<IEnumerable<SelectListItem>> GetSelectListAsync(object selectedValue)
    {
        var items = await _selectListQuery.GetSelectListItemsAsync();

        return new SelectList(
            items,
            nameof(Application.SelectLists.Models.SelectListItem.Value),
            nameof(Application.SelectLists.Models.SelectListItem.Text),
            selectedValue);
    }

    public async Task<IEnumerable<SelectListItem>> GetSelectListAsync(
        string dataValueField,
        string dataTextField)
    {
        if (dataValueField == null)
        {
            throw new ArgumentNullException(nameof(dataValueField));
        }

        if (dataTextField == null)
        {
            throw new ArgumentNullException(nameof(dataTextField));
        }

        var items = await _selectListQuery.GetSelectListItemsAsync();

        return new SelectList(items, dataValueField, dataTextField);
    }

    public async Task<IEnumerable<SelectListItem>> GetSelectListAsync(
        string dataValueField,
        string dataTextField,
        object selectedValue)
    {
        if (dataValueField == null)
        {
            throw new ArgumentNullException(nameof(dataValueField));
        }

        if (dataTextField == null)
        {
            throw new ArgumentNullException(nameof(dataTextField));
        }

        var items = await _selectListQuery.GetSelectListItemsAsync();

        return new SelectList(items, dataValueField, dataTextField, selectedValue);
    }

    public async Task<IEnumerable<SelectListItem>> GetSelectListAsync(
        string dataValueField,
        string dataTextField,
        object selectedValue,
        string dataGroupField)
    {
        if (dataValueField == null)
        {
            throw new ArgumentNullException(nameof(dataValueField));
        }

        if (dataTextField == null)
        {
            throw new ArgumentNullException(nameof(dataTextField));
        }

        if (dataGroupField == null)
        {
            throw new ArgumentNullException(nameof(dataGroupField));
        }

        var items = await _selectListQuery.GetSelectListItemsAsync();

        return new SelectList(items, dataValueField, dataTextField, selectedValue, dataGroupField);
    }
}
