﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ParkingLot.WebApp.Services.Abstractions;

public interface ISelectListProvider<TEntity>
    where TEntity : class
{
    Task<IEnumerable<SelectListItem>> GetSelectListAsync();

    Task<IEnumerable<SelectListItem>> GetSelectListAsync(object selectedValue);

    Task<IEnumerable<SelectListItem>> GetSelectListAsync(string dataValueField, string dataTextField);

    Task<IEnumerable<SelectListItem>> GetSelectListAsync(string dataValueField, string dataTextField, object selectedValue);

    Task<IEnumerable<SelectListItem>> GetSelectListAsync(string dataValueField, string dataTextField, object selectedValue, string dataGroupField);
}
