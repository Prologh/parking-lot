﻿using Microsoft.Extensions.Localization;
using ParkingLot.Application.Exceptions.Providers;
using ParkingLot.Persistance.Exceptions;
using System;
using System.Collections.Generic;

namespace ParkingLot.WebApp.Services;

/// <summary>
/// Provides methods for getting error messages from exceptions.
/// </summary>
public class ExceptionMessageProvider : IExceptionMessageProvider
{
    private readonly IStringLocalizer _localizer;
    private readonly Dictionary<Type, string> _errorMessageKeys;

    public ExceptionMessageProvider(IStringLocalizer<ExceptionMessageProvider> localizer)
    {
        _localizer = localizer;

        _errorMessageKeys = new Dictionary<Type, string>
        {
            [typeof(DbException)] = "DbOperationFailedErrorMessage",
        };
    }

    /// <summary>
    /// Gets the exception message based on its type.
    /// </summary>
    /// <param name="exception">
    /// <see cref="Exception"/> source instance.
    /// </param>
    /// <returns>
    /// Proper <see cref="string"/> message without fragile information.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="exception"/> is <see langword="null"/>.
    /// </exception>
    public string GetExceptionMessage(Exception exception)
    {
        if (exception == null)
        {
            throw new ArgumentNullException(nameof(exception));
        }

        if (_errorMessageKeys.TryGetValue(exception.GetType(), out var key))
        {
            return _localizer[key];
        }

        // General error message.
        return _localizer["GeneralErrorMessage"];
    }
}
