﻿/// <binding Clean='clean' ProjectOpened='watch' />
'use strict';

// Load plugins
var gulp = require('gulp');
var gulpAutoprefixer = require('gulp-autoprefixer');
var gulpConcat = require('gulp-concat');
var gulpCssMin = require('gulp-cssmin');
var gulpRename = require('gulp-rename');
var nodeSass = require('sass');
var gulpSass = require('gulp-sass');
var gulpTerser = require('gulp-terser');
var rimraf = require('rimraf');

gulpSass = gulpSass(nodeSass);

const mainCssFileName = 'site';
const mainJsFileName = 'site';
const mainSassFileName = 'site';
const mainBootstrapFileName = "bootstrap";

var paths = {
    nodeModules: './node_modules/',
    webroot: './wwwroot/'
};

paths.cssDir = paths.webroot + 'css/';
paths.jsDir = paths.webroot + 'js/';
paths.libDir = paths.webroot + 'lib/';
paths.scssDir = paths.webroot + 'scss/';
paths.scssMainSiteDir = paths.scssDir + 'site/';
paths.bootstrapDir = paths.scssDir + "bootstrap/";

paths.jsFiles = paths.jsDir + 'components/*.js';
paths.minJsFiles = paths.jsDir + '**/*.min.js';
paths.scssFiles = paths.scssDir + '**/*.scss';

paths.mainCss = paths.cssDir + mainCssFileName + '.css';
paths.mainCssBootstrap = paths.cssDir + mainBootstrapFileName + '.css';
paths.mainJs = paths.jsDir + mainJsFileName + '.js';
paths.mainMinCss = paths.cssDir + mainCssFileName + '.min.css';
paths.mainMinJs = paths.jsDir + mainJsFileName + '.min.js';
paths.mainSass = paths.scssMainSiteDir + mainSassFileName + '.scss';
paths.mainSassBootstrap = paths.bootstrapDir + mainBootstrapFileName + '.scss';



/* ### Clean ### */

// Clean output files
gulp.task('clean:cssDir', done => rimraf(paths.cssDir, done));
gulp.task('clean:libDir', done => rimraf(paths.libDir, done));
gulp.task('clean:mainJs', done => rimraf(paths.mainJs, done));
gulp.task('clean:mainMinJs', done => rimraf(paths.mainMinJs, done));
gulp.task('clean', gulp.series(['clean:cssDir', 'clean:libDir', 'clean:mainJs', 'clean:mainMinJs']));



/* ### Compile ### */

// Compile SCSS files to CSS
gulp.task('compile:scss:site', () => {
    return gulp.src(paths.mainSass)
        .pipe(gulpSass().on('error', gulpSass.logError))
        .pipe(gulpAutoprefixer())
        .pipe(gulp.dest(paths.cssDir));
});

gulp.task('compile:scss:bootstrap', () => {
    return gulp.src(paths.mainSassBootstrap)
        .pipe(gulpSass().on('error', gulpSass.logError))
        .pipe(gulpAutoprefixer())
        .pipe(gulpRename({ basename: 'bootstrap' }))
        .pipe(gulp.dest(paths.cssDir));
});

// SCSS compile task
gulp.task('compile:scss', gulp.series(['compile:scss:site', 'compile:scss:bootstrap']));

// Global compile task
gulp.task('compile', gulp.series(['compile:scss']));



/* ### Concat ### */

// Concat JavaScript files into one
gulp.task("concat:js", () => {
    return gulp.src(paths.jsFiles, { base: '.' })
        .pipe(gulpConcat(paths.mainJs))
        .pipe(gulp.dest('.'));
});

// Global Concat task
gulp.task('concat', gulp.series('concat:js'));



/* ### Copy ### */

// Copy Font Awesome CSS files
gulp.task('copy:css:font-awesome:css', () => {
    return gulp.src(paths.nodeModules + '@fortawesome/fontawesome-free/css/*.css')
        .pipe(gulpRename({ dirname: paths.libDir + '/font-awesome/css/' }))
        .pipe(gulp.dest('.'));
});

// Copy Font Awesome web fonts files
gulp.task('copy:css:font-awesome:webfonts', () => {
    return gulp.src(paths.nodeModules + '@fortawesome/fontawesome-free/webfonts/*')
        .pipe(gulpRename({ dirname: paths.libDir + '/font-awesome/webfonts/' }))
        .pipe(gulp.dest('.'));
});

// Copy Font Awesome icon pack
gulp.task('copy:css:font-awesome', gulp.series(['copy:css:font-awesome:css', 'copy:css:font-awesome:webfonts']));

// Copy Font Awesome CSS files
gulp.task('copy:css:animate.css', () => {
    return gulp.src(paths.nodeModules + 'animate.css/animate.css')
        .pipe(gulpRename({ dirname: paths.libDir + '/animate.css/' }))
        .pipe(gulp.dest('.'));
});

// Copy CSS libraries
gulp.task('copy:css', gulp.series(['copy:css:font-awesome', 'copy:css:animate.css']));

// Copy Bootstrap files
gulp.task('copy:js:bootstrap:js', () => {
    return gulp.src(paths.nodeModules + 'bootstrap/dist/js/bootstrap*.js')
        .pipe(gulpRename({ dirname: paths.libDir + '/bootstrap/' }))
        .pipe(gulp.dest('.'));
});

gulp.task('copy:js:bootstrap:map', () => {
    return gulp.src(paths.nodeModules + 'bootstrap/dist/js/bootstrap*.js.map')
        .pipe(gulpRename({ dirname: paths.libDir + '/bootstrap/' }))
        .pipe(gulp.dest('.'));
});

gulp.task('copy:js:bootstrap', gulp.series(['copy:js:bootstrap:map', 'copy:js:bootstrap:js']));

// Copy Bootstrap-Notify files
gulp.task('copy:js:bootstrap-notify', () => {
    return gulp.src(paths.nodeModules + 'bootstrap-notify/bootstrap-notify.js')
        .pipe(gulpRename({ dirname: paths.libDir + '/bootstrap-notify/' }))
        .pipe(gulp.dest('.'));
});

// Copy jQuery files
gulp.task('copy:js:jquery:js', () => {
    return gulp.src(paths.nodeModules + 'jquery/dist/jquery*.js')
        .pipe(gulpRename({ dirname: paths.libDir + '/jquery/' }))
        .pipe(gulp.dest('.'));
});

gulp.task('copy:js:jquery:map', () => {
    return gulp.src(paths.nodeModules + 'jquery/dist/jquery*.js.map')
        .pipe(gulpRename({ dirname: paths.libDir + '/jquery/' }))
        .pipe(gulp.dest('.'));
});

gulp.task('copy:js:jquery', gulp.series(['copy:js:jquery:map', 'copy:js:jquery:js']));

// Copy jQuery Validate files
gulp.task('copy:js:jquery-validate', () => {
    return gulp.src(paths.nodeModules + 'jquery-validation/dist/jquery.validate*.js')
        .pipe(gulpRename({ dirname: paths.libDir + '/jquery-validate/' }))
        .pipe(gulp.dest('.'));
});

// Copy jQuery Validate Unobtrusive files
gulp.task('copy:js:jquery-validate-unobtrusive', () => {
    return gulp.src(paths.nodeModules + 'jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive*.js')
        .pipe(gulpRename({ dirname: paths.libDir + '/jquery-validate-unobtrusive/' }))
        .pipe(gulp.dest('.'));
});

// Copy Chart JS files
gulp.task('copy:js:chart-js', () => {
    return gulp.src(paths.nodeModules + 'chart.js/dist/*.js')
        .pipe(gulpRename({ dirname: paths.libDir + '/chart-js/' }))
        .pipe(gulp.dest('.'));
});


// Copy JavaScript libraries
gulp.task('copy:js', gulp.series([
    'copy:js:bootstrap',
    'copy:js:bootstrap-notify',
    'copy:js:chart-js',
    'copy:js:jquery',
    'copy:js:jquery-validate',
    'copy:js:jquery-validate-unobtrusive'
]));

// Global Concat task
gulp.task('copy', gulp.series(['copy:css', 'copy:js']));



/* ### Minify ### */

// Minify CSS files
gulp.task("minify:css:site", () => {
    return gulp.src(paths.mainCss)
        .pipe(gulpCssMin())
        .pipe(gulpRename({ suffix: '.min' }))
        .pipe(gulp.dest(paths.cssDir));
});

gulp.task("minify:css:bootstrap", () => {
    return gulp.src(paths.mainCssBootstrap)
        .pipe(gulpCssMin())
        .pipe(gulpRename({ basename: 'bootstrap', suffix: '.min' }))
        .pipe(gulp.dest(paths.cssDir));
});

// CSS minify task
gulp.task('minify:css', gulp.series(['minify:css:site', 'minify:css:bootstrap']));

// Minify JavaScript files
gulp.task("minify:js", () => {
    return gulp.src(paths.mainJs)
        .pipe(gulpTerser())
        .pipe(gulpRename({ suffix: '.min' }))
        .pipe(gulp.dest(paths.jsDir));
});

// Global minify task
gulp.task('minify', gulp.series(['minify:js', 'minify:css']));



/* ### Watch ### */

// Watch scss files
gulp.task('watch:scss', () => {
    return gulp.watch(paths.scssFiles, gulp.series(['compile', 'minify:css']));
});

// Watch JavaScript files
gulp.task('watch:js', () => {
    gulp.watch(paths.jsFiles, gulp.series(['concat', 'minify:js']));
});

// Global watch task
gulp.task('watch', gulp.parallel(['watch:scss', 'watch:js']));



/* ### Default ### */

gulp.task('default', gulp.series(['compile', 'concat', 'copy', 'minify']));
