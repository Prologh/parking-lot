﻿using AutoMapper;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using ParkingLot.Application.Exceptions.Providers;
using ParkingLot.Application.Http.Providers;
using ParkingLot.Application.Levels.Queries;
using ParkingLot.WebApp.ViewModels;
using ParkingLot.WebApp.ViewModels.HomePage;
using System;
using System.Threading.Tasks;

namespace ParkingLot.WebApp.Controllers;

public class HomeController : Controller
{
    private readonly IExceptionMessageProvider _exceptionMessageProvider;
    private readonly IHttpStatusCodeMessageProvider _httpStatusCodeMessageProvider;
    private readonly IGetLevelMetadatasQuery _getLevelMetadatasQuery;
    private readonly IMapper _mapper;

    public HomeController(
        IExceptionMessageProvider exceptionMessageProvider,
        IHttpStatusCodeMessageProvider httpStatusCodeMessageProvider,
        IGetLevelMetadatasQuery getLevelMetadatasQuery,
        IMapper mapper)
    {
        _exceptionMessageProvider = exceptionMessageProvider;
        _httpStatusCodeMessageProvider = httpStatusCodeMessageProvider;
        _getLevelMetadatasQuery = getLevelMetadatasQuery;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var levelsMetadataList = await _getLevelMetadatasQuery.GetAsync();
        var viewModel = new HomePageVM
        {
            LevelMetadata = levelsMetadataList,
        };

        return View(viewModel);
    }

    [Route(nameof(Error))]
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        var exceptionHandler = HttpContext.Features.Get<IExceptionHandlerFeature>();
        var exception = exceptionHandler?.Error ?? new Exception();
        var message = _exceptionMessageProvider.GetExceptionMessage(exception);
        var viewModel = new ExceptionVM()
        {
            Exception = exception,
            Message = message,
        };

        return View("Exception", viewModel);
    }

    [Route(nameof(Error) + "/{httpStatusCode:int}")]
    [ResponseCache(Duration = int.MaxValue, Location = ResponseCacheLocation.Any, NoStore = false)]
    public IActionResult Error(int httpStatusCode)
    {
        var message = _httpStatusCodeMessageProvider.GetHttpStatusCodeMessage(httpStatusCode);
        var viewModel = new HttpStatusCodeVM()
        {
            HttpStatusCode = httpStatusCode,
            Message = message,
        };

        return View("HttpStatusCode", viewModel);
    }
}
