﻿using AutoMapper;
using CSharpFunctionalExtensions;
using Fluorite.Strainer.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using ParkingLot.Application.Logging.InputModels;
using ParkingLot.Application.Logging.Requests;
using ParkingLot.Application.Logging.ViewModels;
using ParkingLot.Application.Pagination.Managers;
using ParkingLot.Domain.Logging;
using ParkingLot.WebApp.Controllers.Abstract;
using System.Threading.Tasks;

namespace ParkingLot.WebApp.Controllers;

public class LoggingController : ResultsControllerBase
{
    private readonly IMapper _mapper;
    private readonly IMediator _mediator;
    private readonly IPaginationManager<RequestLog, RequestLogVM> _requestLogsPaginationManager;

    public LoggingController(
        IMapper mapper,
        IMediator mediator,
        IPaginationManager<RequestLog, RequestLogVM> requestLogsPaginationManager)
    {
        _mapper = mapper;
        _mediator = mediator;
        _requestLogsPaginationManager = requestLogsPaginationManager;
    }

    [HttpGet]
    public async Task<IActionResult> Index([FromQuery] StrainerModel strainerModel)
    {
        var pagedResult = await _requestLogsPaginationManager.GetPagedAsync(strainerModel);

        return View(pagedResult);
    }

    [Route("[controller]/Charts/Requests")]
    [HttpGet]
    public async Task<IActionResult> Chart()
    {
        return await Result.Success()
            .Map(() => new GetRequestLogsCountRequest())
            .Map(async request => await _mediator.Send(request))
            .Finally(result => BuildViewResult(result, "Charts/Requests"));
    }

    [HttpGet("[controller]/{id:int}")]
    public Task<IActionResult> Show([FromRoute] int id)
    {
        return Result.Success()
            .Map(() => new GetRequestLogDetailsRequest() { Id = id })
            .Bind(request => _mediator.Send(request))
            .Finally(BuildViewResult);
    }

    [HttpPost]
    public Task<IActionResult> Destroy([FromForm] DeleteRequestLogInputModel inputModel)
    {
        return Result.Success()
            .Map(() => _mapper.Map<DeleteRequestLogRequest>(inputModel))
            .Bind(request => _mediator.Send(request))
            .Finally(result => BuildActionResult(result, onSuccess: () => RedirectToAction(nameof(Index))));
    }
}
