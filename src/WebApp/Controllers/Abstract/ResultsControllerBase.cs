﻿using CSharpFunctionalExtensions;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ParkingLot.WebApp.Controllers.Abstract;

public abstract class ResultsControllerBase : Controller
{
    protected IActionResult BuildViewResult(Result result)
    {
        return result.IsSuccess
            ? View()
            : HandleFailedResult(result);
    }

    protected IActionResult BuildViewResult<T>(Result<T> result)
    {
        return result.IsSuccess
            ? View(result.Value)
            : HandleFailedResult(result);
    }

    protected IActionResult BuildViewResult(Result result, string viewName)
    {
        return result.IsSuccess
            ? View(viewName)
            : HandleFailedResult(result);
    }

    protected IActionResult BuildViewResult<T>(Result<T> result, string viewName)
    {
        return result.IsSuccess
            ? View(viewName, result.Value)
            : HandleFailedResult(result);
    }

    protected IActionResult BuildActionResult(Result result, Func<IActionResult> onSuccess)
    {
        return result.IsSuccess
            ? onSuccess()
            : HandleFailedResult(result);
    }

    private IActionResult HandleFailedResult(Result result)
    {
        if (result.IsSuccess)
        {
            throw new ArgumentException("Result must cannot be successful.", nameof(result));
        }

        if (result.Error.EndsWith("NotFound"))
        {
            return NotFound();
        }

        return BadRequest();
    }
}
