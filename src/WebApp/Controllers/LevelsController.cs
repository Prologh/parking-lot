﻿using Fluorite.Strainer.Models;
using Microsoft.AspNetCore.Mvc;
using ParkingLot.Application;
using ParkingLot.Application.Levels.InputModels;
using ParkingLot.Application.Levels.Queries;
using ParkingLot.Application.Levels.Repositories;
using ParkingLot.Application.Levels.ViewModels;
using ParkingLot.Application.Notifications.Models;
using ParkingLot.Application.Notifications.Storages.Abstractions;
using ParkingLot.Application.Pagination.Managers;
using ParkingLot.Application.Spaces.Queries;
using ParkingLot.Application.Spaces.Repositories;
using ParkingLot.Application.Spaces.ViewModels;
using ParkingLot.Domain.Levels;
using ParkingLot.Domain.Spaces;
using ParkingLot.Extensions.Notifications;
using System.Threading.Tasks;

namespace ParkingLot.WebApp.Controllers;

public class LevelsController : Controller
{
    private readonly INotificationStorage _notificationStorage;
    private readonly IPaginationManager<Level, LevelListItemVM> _levelsPaginationManager;
    private readonly IPaginationManager<Space, SpaceListItemVM> _spacesPaginationManager;
    private readonly ILevelsRepository _levelsRepository;
    private readonly ISpacesRepository _spacesRepository;
    private readonly IGetHighestSpaceNumberQuery _getHighestSpaceNumberQuery;
    private readonly IGetLevelDetailsQuery _getLevelDetailsQuery;
    private readonly IGetUpdateLevelInputModelQuery _getUpdateLevelViewModelQuery;
    private readonly ICheckAllLevelSpacesAreUnoccupiedQuery _checkAllLevelSpacesAreUnoccupiedQuery;
    private readonly IUnitOfWork _unitOfWork;

    public LevelsController(
        INotificationStorage notificationStorage,
        IPaginationManager<Level, LevelListItemVM> levelsPaginationManager,
        IPaginationManager<Space, SpaceListItemVM> spacesPaginationManager,
        ILevelsRepository levelsRepository,
        ISpacesRepository spacesRepository,
        IGetHighestSpaceNumberQuery getHighestSpaceNumberQuery,
        IGetLevelDetailsQuery getLevelDetailsQuery,
        IGetUpdateLevelInputModelQuery getUpdateLevelViewModelQuery,
        ICheckAllLevelSpacesAreUnoccupiedQuery checkAllLevelSpacesAreUnoccupiedQuery,
        IUnitOfWork unitOfWork)
    {
        _notificationStorage = notificationStorage;
        _levelsPaginationManager = levelsPaginationManager;
        _spacesPaginationManager = spacesPaginationManager;
        _levelsRepository = levelsRepository;
        _spacesRepository = spacesRepository;
        _getHighestSpaceNumberQuery = getHighestSpaceNumberQuery;
        _getLevelDetailsQuery = getLevelDetailsQuery;
        _getUpdateLevelViewModelQuery = getUpdateLevelViewModelQuery;
        _checkAllLevelSpacesAreUnoccupiedQuery = checkAllLevelSpacesAreUnoccupiedQuery;
        _unitOfWork = unitOfWork;
    }

    [HttpGet]
    public async Task<IActionResult> Index([FromQuery] StrainerModel strainerModel)
    {
        var result = await _levelsPaginationManager.GetPagedAsync(strainerModel);

        return View(result);
    }

    [HttpGet("[controller]/{id:int}")]
    public async Task<IActionResult> Show([FromRoute] int id)
    {
        var viewModel = await _getLevelDetailsQuery.GetAsync(id);
        if (viewModel == null)
        {
            return NotFound();
        }

        return View(viewModel);
    }

    [HttpGet]
    public IActionResult New()
    {
        return View(new CreateLevelInputModel());
    }

    [HttpPost]
    public async Task<IActionResult> Create([FromForm] CreateLevelInputModel inputModel)
    {
        var level = new Level(inputModel.Position.Value);

        // Get last space number.
        var lastSpaceNumber = await _getHighestSpaceNumberQuery.GetAsync();

        // Create related spaces.
        for (int i = 0; i < inputModel.SpacesAmount; i++)
        {
            var space = new Space
            {
                Level = level,
                Number = lastSpaceNumber + i + 1,
            };
            _spacesRepository.Add(space);
        }

        _levelsRepository.Add(level);

        await _unitOfWork.SaveChangesAsync();

        _notificationStorage.Builder()
            .Icon("fas fa-check")
            .Message($"New parking level created.")
            .Title("Success!")
            .Type(NotificationType.Success)
            .SetNotification();

        return RedirectToAction(nameof(Show), new { level.Id });
    }

    [HttpGet("[controller]/{id:int}/[action]")]
    public async Task<IActionResult> Edit([FromRoute] int id)
    {
        var viewModel = await _getUpdateLevelViewModelQuery.GetAsync(id);
        if (viewModel == null)
        {
            return NotFound();
        }

        return View(viewModel);
    }

    [HttpPost]
    public async Task<IActionResult> Update([FromForm] UpdateLevelInputModel inputModel)
    {
        var model = await _levelsRepository.FindAsync(inputModel.Id.Value);
        if (model == null)
        {
            return NotFound();
        }

        model.SetPosition(inputModel.Position.Value);

        await _unitOfWork.SaveChangesAsync();

        _notificationStorage.Builder()
            .Icon("fas fa-check")
            .Message("Parking level updated.")
            .Title("Success!")
            .Type(NotificationType.Success)
            .SetNotification();

        return RedirectToAction(nameof(Show), new { model.Id });
    }

    [HttpPost]
    public async Task<IActionResult> Destroy([FromForm] DeleteLevelInputModel viewModel)
    {
        var level = await _levelsRepository.FindAsync(viewModel.Id.Value);
        if (level is null)
        {
            return NotFound();
        }

        if (!await _checkAllLevelSpacesAreUnoccupiedQuery.CheckAsync(viewModel.Id.Value))
        {
            return BadRequest();
        }

        _levelsRepository.Remove(level);

        await _unitOfWork.SaveChangesAsync();

        _notificationStorage.Builder()
            .Icon("fas fa-check")
            .Message($"Parking level removed.")
            .Title("Success!")
            .Type(NotificationType.Success)
            .SetNotification();

        return RedirectToAction(nameof(Index));
    }
}
