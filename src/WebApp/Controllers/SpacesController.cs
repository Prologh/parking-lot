﻿using Fluorite.Strainer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using ParkingLot.Application;
using ParkingLot.Application.Notifications.Models;
using ParkingLot.Application.Notifications.Storages.Abstractions;
using ParkingLot.Application.Pagination.Managers;
using ParkingLot.Application.Spaces.InputModels;
using ParkingLot.Application.Spaces.Queries;
using ParkingLot.Application.Spaces.Repositories;
using ParkingLot.Application.Spaces.ViewModels;
using ParkingLot.Domain.Spaces;
using ParkingLot.Extensions.Notifications;
using System.Threading.Tasks;

namespace ParkingLot.WebApp.Controllers;

public class SpacesController : Controller
{
    private readonly ISpacesRepository _spacesRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IPaginationManager<Space, SpaceListItemVM> _spacesPaginationManager;
    private readonly IStringLocalizer<SpacesController> _localizer;
    private readonly INotificationStorage _notificationStorage;
    private readonly IGetSpaceDetailsQuery _getSpaceDetailsQuery;
    private readonly IGetUpdateSpaceInputModelQuery _getUpdateSpaceViewModelQuery;
    private readonly ICheckSpaceWithNumberExistsQuery _checkSpaceWithNumberExistsQuery;
    private readonly ICheckSpaceIsCurrentlyOccupiedQuery _checkSpaceIsCurrentlyOccupiedQuery;

    public SpacesController(
        INotificationStorage notificationStorage,
        IGetSpaceDetailsQuery getSpaceDetailsQuery,
        IGetUpdateSpaceInputModelQuery getUpdateSpaceViewModelQuery,
        ICheckSpaceWithNumberExistsQuery checkSpaceWithNumberExistsQuery,
        ICheckSpaceIsCurrentlyOccupiedQuery checkSpaceIsCurrentlyOccupiedQuery,
        IPaginationManager<Space, SpaceListItemVM> spacesPaginationManager,
        IStringLocalizer<SpacesController> stringLocalizer,
        ISpacesRepository spacesRepository,
        IUnitOfWork unitOfWork)
    {
        _notificationStorage = notificationStorage;
        _getSpaceDetailsQuery = getSpaceDetailsQuery;
        _getUpdateSpaceViewModelQuery = getUpdateSpaceViewModelQuery;
        _checkSpaceWithNumberExistsQuery = checkSpaceWithNumberExistsQuery;
        _checkSpaceIsCurrentlyOccupiedQuery = checkSpaceIsCurrentlyOccupiedQuery;
        _spacesPaginationManager = spacesPaginationManager;
        _localizer = stringLocalizer;
        _spacesRepository = spacesRepository;
        _unitOfWork = unitOfWork;
    }

    [HttpGet]
    public async Task<IActionResult> Index([FromQuery] StrainerModel strainerModel)
    {
        var pagedResult = await _spacesPaginationManager.GetPagedAsync(strainerModel);

        return View(pagedResult);
    }

    [HttpGet("[controller]/{id:int}")]
    public async Task<IActionResult> Show([FromRoute] int id)
    {
        var viewModel = await _getSpaceDetailsQuery.GetAsync(id);
        if (viewModel == null)
        {
            return NotFound();
        }

        return View(viewModel);
    }

    [HttpGet]
    public IActionResult New(int? id)
    {
        var viewModel = new CreateSpaceInputModel
        {
            LevelId = id ?? default,
        };

        return View(viewModel);
    }

    [HttpPost]
    public async Task<IActionResult> Create([FromForm] CreateSpaceInputModel inputModel)
    {
        if (!ModelState.IsValid)
        {
            Response.StatusCode = StatusCodes.Status400BadRequest;

            return View(nameof(New), inputModel);
        }

        if (await _checkSpaceWithNumberExistsQuery.CheckAsync(inputModel.Number.Value))
        {
            Response.StatusCode = StatusCodes.Status400BadRequest;
            ModelState.AddModelError(
                "SpaceNumberAlreadyTaken",
                _localizer.GetString("SpaceNumberAlreadyTaken", inputModel.Number));

            return View(nameof(New), inputModel);
        }

        var model = new Space
        {
            IsForHandicapped = inputModel.IsForHandicapped,
            LevelId = inputModel.LevelId.Value,
            Number = inputModel.Number.Value,
            Status = inputModel.Status.Value,
        };

        _spacesRepository.Add(model);

        await _unitOfWork.SaveChangesAsync();

        _notificationStorage.Builder()
            .Icon("fas fa-check")
            .Message("New parking space created.")
            .Title("Success!")
            .Type(NotificationType.Success)
            .SetNotification();

        return RedirectToAction(nameof(Show), new { model.Id });
    }

    [HttpGet("[controller]/{id:int}/[action]")]
    public async Task<IActionResult> Edit([FromRoute] int id)
    {
        var viewModel = await _getUpdateSpaceViewModelQuery.GetAsync(id);
        if (viewModel == null)
        {
            return NotFound();
        }

        return View(viewModel);
    }

    [HttpPost]
    public async Task<IActionResult> Update([FromForm] UpdateSpaceInputModel inputModel)
    {
        if (!ModelState.IsValid)
        {
            Response.StatusCode = StatusCodes.Status400BadRequest;

            return View(nameof(Edit), inputModel);
        }

        var model = await _spacesRepository.FindAsync(inputModel.Id.Value);
        if (model == null)
        {
            return NotFound();
        }

        if (await _checkSpaceWithNumberExistsQuery.CheckAsync(inputModel.Number.Value, inputModel.Id.Value))
        {
            Response.StatusCode = StatusCodes.Status400BadRequest;
            ModelState.AddModelError(
                "SpaceNumberAlreadyTaken",
                _localizer.GetString("SpaceNumberAlreadyTaken", inputModel.Number));

            return View(nameof(Edit), inputModel);
        }

        model.IsForHandicapped = inputModel.IsForHandicapped;
        model.LevelId = inputModel.LevelId.Value;
        model.Number = inputModel.Number.Value;
        model.Status = inputModel.Status.Value;

        _spacesRepository.Update(model);

        await _unitOfWork.SaveChangesAsync();

        _notificationStorage.Builder()
            .Icon("fas fa-check")
            .Message("Parking space updated.")
            .Title("Success!")
            .Type(NotificationType.Success)
            .SetNotification();

        return RedirectToAction(nameof(Show), new { model.Id });
    }

    [HttpPost]
    public async Task<IActionResult> Destroy([FromForm] DeleteSpaceInputModel viewModel)
    {
        var space = await _spacesRepository.FindAsync(viewModel.Id.Value);
        if (space is null)
        {
            return NotFound();
        }

        if (await _checkSpaceIsCurrentlyOccupiedQuery.CheckAsync(viewModel.Id.Value))
        {
            ModelState.AddModelError("SpaceRemoval", _localizer.GetString("CannotRemoveSpaceCurrentlyOccupied"));

            return BadRequest();
        }

        _spacesRepository.Remove(space);

        await _unitOfWork.SaveChangesAsync();

        _notificationStorage.Builder()
            .Icon("fas fa-check")
            .Message($"Parking space removed.")
            .Title("Success!")
            .Type(NotificationType.Success)
            .SetNotification();

        return RedirectToAction(nameof(Index));
    }
}
