﻿using Fluorite.Strainer.Models;
using Microsoft.AspNetCore.Mvc;
using ParkingLot.Application.Pagination.Managers;
using ParkingLot.Application.SpaceHistory.Queries;
using ParkingLot.Application.SpaceHistory.ViewModels;
using ParkingLot.Application.VehicleHistory.ViewModels;
using ParkingLot.Domain.SpaceHistory;
using ParkingLot.Domain.VehicleHistory;
using System;
using System.Threading.Tasks;

namespace ParkingLot.WebApp.Controllers;

public class HistoryController : Controller
{
    private readonly IPaginationManager<SpaceHistoryEntry, SpaceHistoryEntryVM> _spaceHistoryPaginationManager;
    private readonly IPaginationManager<VehicleHistoryEntry, VehicleHistoryEntryVM> _vehicleHistoryPaginationManager;
    private readonly IGetVehicleHistoryChartQuery _getSpaceHistoryChartQuery;
    private readonly IGetMostPopularSpacesQuery _getMostPopularSpacesQuery;

    public HistoryController(
        IPaginationManager<SpaceHistoryEntry, SpaceHistoryEntryVM> spaceHistoryPaginationManager,
        IPaginationManager<VehicleHistoryEntry, VehicleHistoryEntryVM> vehicleHistoryPaginationManager,
        IGetVehicleHistoryChartQuery getSpaceHistoryChartQuery,
        IGetMostPopularSpacesQuery getMostPopularSpacesQuery)
    {
        _spaceHistoryPaginationManager = spaceHistoryPaginationManager;
        _vehicleHistoryPaginationManager = vehicleHistoryPaginationManager;
        _getSpaceHistoryChartQuery = getSpaceHistoryChartQuery;
        _getMostPopularSpacesQuery = getMostPopularSpacesQuery;
    }

    public IActionResult Index() => RedirectToActionPermanent(nameof(Vehicles));

    [HttpGet]
    public async Task<IActionResult> Spaces([FromQuery] StrainerModel strainerModel)
    {
        var pagedResult = await _spaceHistoryPaginationManager.GetPagedAsync(strainerModel);

        return View(pagedResult);
    }

    [HttpGet]
    public async Task<IActionResult> Vehicles([FromQuery] StrainerModel strainerModel)
    {
        var pagedResult = await _vehicleHistoryPaginationManager.GetPagedAsync(strainerModel);

        return View(pagedResult);
    }

    [HttpGet]
    public async Task<IActionResult> Charts([FromQuery] int? days, [FromQuery] int? hourStep)
    {
        if (!days.HasValue || !hourStep.HasValue)
        {
            days = 30;
            hourStep = 24;
        }

        var viewModel = await _getSpaceHistoryChartQuery.GetAsync(
            from: DateTimeOffset.Now.Add(TimeSpan.FromDays(-days.Value)),
            to: DateTimeOffset.Now,
            step: TimeSpan.FromHours(hourStep.Value));

        return View(viewModel);
    }

    [HttpGet]
    public async Task<IActionResult> MostPopularSpacesChart()
    {
        var viewModel = await _getMostPopularSpacesQuery.GetAsync(limit: 10);

        return View(viewModel);
    }
}
