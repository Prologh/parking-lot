﻿using Fluorite.Strainer.Models;
using Microsoft.AspNetCore.Mvc;
using ParkingLot.Application.Pagination.Managers;
using ParkingLot.Application.Vehicles.Queries;
using ParkingLot.Application.Vehicles.ViewModels;
using ParkingLot.Domain.Vehicles;
using System.Threading.Tasks;

namespace ParkingLot.WebApp.Controllers;

public class VehiclesController : Controller
{
    private readonly IPaginationManager<Vehicle, VehicleVM> _vehiclesPaginationManager;
    private readonly IGetVehicleDetailsQuery _getVehicleDetailsQuery;

    public VehiclesController(
        IGetVehicleDetailsQuery getVehicleDetailsQuery,
        IPaginationManager<Vehicle, VehicleVM> vehiclesPaginationManager)
    {
        _getVehicleDetailsQuery = getVehicleDetailsQuery;
        _vehiclesPaginationManager = vehiclesPaginationManager;
    }

    [HttpGet]
    public async Task<IActionResult> Index([FromQuery] StrainerModel strainerModel)
    {
        var pagedResult = await _vehiclesPaginationManager.GetPagedAsync(strainerModel);

        return View(pagedResult);
    }

    [HttpGet("[controller]/{id:int}")]
    public async Task<IActionResult> Show([FromRoute] int id)
    {
        var viewModel = await _getVehicleDetailsQuery.GetAsync(id);
        if (viewModel == null)
        {
            return NotFound();
        }

        return View(viewModel);
    }
}
