﻿$(document).ready(() => {

    $('.filterConfigureModalButton').on("click", (e) => {
        $('.filterEditModal').modal('show');
    });

    $('.filterEditModalButton').on("click", (e) => {
        var filterFieldName = $(e.currentTarget).children('.filterFieldName').text();
        var filterFieldOperator = $(e.currentTarget).children('.filterFieldOperator').text();
        var filterFieldValue = $(e.currentTarget).children('.filterFieldValue').text();
        var index = $(e.currentTarget).data('index');
        var basisType = $(e.currentTarget).data('basis-type');

        $('#filterFieldLabel').text(filterFieldName);
        $('#filterFieldLabel').removeClass('d-none');
        $('#filterOperatorSelect').children("option").hide();
        $('#filterOperatorSelect').children('option[data-basis-type-subgroups*="' + basisType + '"]').show();
        $('#filterOperatorSelect').val(filterFieldOperator);
        $('#filterFieldInput').val(filterFieldValue);
        $('.filterEditModalApply').data('is-edit', true);
        $('.filterEditModalApply').data('index', index);
    });

    $('.filterAddModalButton').on("click", (e) => {
        $('#filterFieldSelect').removeClass('d-none');
        $('.filterEditModalApply').data('is-edit', false);
        $('#filterFieldSelect').trigger("change");
    });

    $('#filterFieldSelect').on("change", (e) => {
        var fieldSelect = $(e.currentTarget);
        var selectedOption = fieldSelect.find(":selected");
        var basisType = selectedOption.data('basis-type');
        var inputType = getInputType(basisType);

        $('#filterOperatorSelect').children("option").hide();
        $('#filterOperatorSelect').children('option[data-basis-type-subgroups*="' + basisType + '"]').show();
        $('#filterFieldInput').attr("type", inputType);
    });

    $('.removeFilterFieldButton').on("click", (e) => {
        var newUrl = new URL(document.location);
        var index = $(e.currentTarget).data('index');
        var filtersParameter = newUrl.searchParams.get('Filters');
        var existingFiltersSplitted = filtersParameter.split(',');
        existingFiltersSplitted[index] = null;

        var updatedFiltersParameter = existingFiltersSplitted.filter(v => v).join(',');

        if (updatedFiltersParameter) {
            newUrl.searchParams.set('Filters', updatedFiltersParameter);
        }
        else {
            newUrl.searchParams.delete('Filters');
        }

        newUrl.searchParams.delete('Page');
        newUrl.search = decodeURIComponent(newUrl.search);

        document.location = newUrl.toString();
    });

    $('.filterEditModal').on('hidden.bs.modal', () => {
        $('#filterFieldLabel').text('');
        $('#filterOperatorSelect').val($("#filterOperatorSelect option:first").val());
        $('#filterFieldInput').val('');
        $('#filterFieldSelect').addClass('d-none');
        $('#filterFieldLabel').addClass('d-none');
    })

    $('.filterEditModalApply').on('click', (e) => {
        var newUrl = new URL(document.location);
        var isEdit = $(e.currentTarget).data('is-edit');
        var index = $(e.currentTarget).data('index');
        var fieldName = getFilterFieldName(isEdit);
        var newFiltersValue = getFilterValue(fieldName);
        var filtersParameter = newUrl.searchParams.get('Filters');
        var updatedFiltersParameter = buildFiltersParameterValue(isEdit, index, filtersParameter, newFiltersValue);

        newUrl.searchParams.delete('Page');
        newUrl.searchParams.set('Filters', updatedFiltersParameter);
        newUrl.search = decodeURIComponent(newUrl.search);

        document.location = newUrl.toString();
    });

    function buildFiltersParameterValue(isEdit, index, filtersParameter, newFiltersValue) {
        if (isEdit) {
            var existingFiltersSplitted = filtersParameter.split(',')
            existingFiltersSplitted[index] = newFiltersValue;

            return existingFiltersSplitted.filter(v => v).join(',');
        }
        else {
            return [filtersParameter, newFiltersValue].filter(v => v).join(',');
        }
    }

    function getFilterValue(fieldName) {
        var filterOperator = $('#filterOperatorSelect').val();
        var filterValue = $('#filterFieldInput').val();

        return fieldName + filterOperator + filterValue;
    }

    function getFilterFieldName(isEdit) {
        if (isEdit) {
            return $('#filterFieldLabel').text();
        } else {
            return $('#filterFieldSelect').val();
        }
    }

    function getInputType(basisType) {
        switch (basisType) {
            case 'Boolean':
                return 'checkbox';
            case 'DateTime':
                return 'datetime-local';
            case 'Integer':
            case 'Number':
                return 'number';
            case 'String':
                return 'text';
            default:
                throw 'Unsupported basis type ' + basisType;
        }
    }

});