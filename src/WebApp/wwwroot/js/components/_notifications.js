﻿$.notifyDefaults({
    offset: 60,
    delay: 3000,
    placement: {
        from: "top",
        align: "center"
    },
    animate: {
        enter: 'animated fadeInDown',
        exit: 'animated fadeOutUp'
    },
    mouse_over: 'pause',
    newest_on_top: true
});
