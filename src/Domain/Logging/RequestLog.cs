﻿using ParkingLot.Domain.Abstractions;
using System;
using System.Diagnostics;

namespace ParkingLot.Domain.Logging;

/// <summary>
/// Represents single request log.
/// </summary>
[DebuggerDisplay("\\{{HttpMethod,nq}, {HttpStatusCode}\\}")]
public class RequestLog : BaseEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="RequestLog"/>
    /// class.
    /// </summary>
    public RequestLog()
    {

    }

    /// <summary>
    /// Gets or sets the duration of request execution on server-side
    /// expressed in miliseconds.
    /// </summary>
    public double Duration { get; set; }

    /// <summary>
    /// Gets or sets the HTTP method type.
    /// </summary>
    public string HttpMethod { get; set; }

    /// <summary>
    /// Gets or sets the HTTP status code type.
    /// </summary>
    public int HttpStatusCode { get; set; }

    /// <summary>
    /// Gets or sets the request caller IP address.
    /// </summary>
    public string IPAddress { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether requets
    /// was made using HTTPS protocol.
    /// </summary>
    public bool IsHttps { get; set; }

    /// <summary>
    /// Gets or sets request query.
    /// </summary>
    public string Query { get; set; }

    /// <summary>
    /// Gets or sets the request headers.
    /// </summary>
    public string RequestHeaders { get; set; }

    /// <summary>
    /// Gets or sets the response headers.
    /// </summary>
    public string ResponseHeaders { get; set; }

    /// <summary>
    /// Gets or sets the request route.
    /// </summary>
    public string Route { get; set; }
}
