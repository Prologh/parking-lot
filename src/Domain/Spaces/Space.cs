﻿using ParkingLot.Domain.Abstractions;
using ParkingLot.Domain.Levels;
using ParkingLot.Domain.SpaceHistory;
using System.Collections.Generic;

namespace ParkingLot.Domain.Spaces;

/// <summary>
/// Represents single parking space.
/// </summary>
public class Space : BaseEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Space"/> class.
    /// </summary>
    public Space()
    {
        History = new List<SpaceHistoryEntry>();
    }

    /// <summary>
    /// Gets or sets a value indicating whether current
    /// parking space is designed for handicapped drivers.
    /// </summary>
    public bool IsForHandicapped { get; set; }

    /// <summary>
    /// Gets or sets the identifier of a parking level to which current
    /// parking space belongs.
    /// </summary>
    public int LevelId { get; set; }

    /// <summary>
    /// Gets or sets the parking level on which current parking space is located.
    /// </summary>
    public Level Level { get; set; }

    /// <summary>
    /// Gets or sets space number.
    /// </summary>
    public int Number { get; set; }

    /// <summary>
    /// Gets or sets space history.
    /// </summary>
    public IList<SpaceHistoryEntry> History { get; set; }

    /// <summary>
    /// Gets or sets the space status.
    /// </summary>
    public SpaceStatus Status { get; set; }
}
