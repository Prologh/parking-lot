﻿namespace ParkingLot.Domain.Spaces;

/// <summary>
/// Representa status of a single parking space.
/// </summary>
public enum SpaceStatus
{
    /// <summary>
    /// Unoccupied - no vehicle occupies a parking space.
    /// </summary>
    Unoccupied = 0,

    /// <summary>
    /// Occupied - a vehicle has parked on a parking space.
    /// </summary>
    Occupied,

    /// <summary>
    /// Out of service - indicates maintenance work, parking space lock
    /// or other cause why the parking space cannot be used.
    /// </summary>
    OutOfService,
}