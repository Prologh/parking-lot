﻿using ParkingLot.Domain.Abstractions;
using ParkingLot.Domain.Vehicles;
using System;

namespace ParkingLot.Domain.VehicleHistory;

public class VehicleHistoryEntry : IBaseEntity
{
    public VehicleHistoryEntry(Vehicle vehicle, DateTimeOffset entryDate)
    {
        if (vehicle is null)
        {
            throw new ArgumentNullException(nameof(vehicle));
        }

        EnteredAt = entryDate;
        Vehicle = vehicle;
    }

    private VehicleHistoryEntry()
    {

    }

    /// <summary>
    /// Gets or sets the entity identifier.
    /// </summary>
    public int Id { get; private set; }

    public DateTimeOffset EnteredAt { get; private set; }

    public DateTimeOffset? ExitedAt { get; private set; }

    public bool HasExited { get; set; }

    public TimeSpan? StayTime { get; private set; }

    public Vehicle Vehicle { get; private set; }

    public int VehicleId { get; private set; }

    public void Exit(DateTimeOffset exitDate)
    {
        if (HasExited)
        {
            throw new InvalidOperationException(
                $"Vehicle history entry must have {nameof(HasExited)} flag " +
                $"set to {false} in order to be saved as exited.");
        }

        if (exitDate < EnteredAt)
        {
            throw new ArgumentException(
                $"{nameof(exitDate)} must be later date than date of entering with the releated vehicle.",
                nameof(exitDate));
        }

        ExitedAt = exitDate;
        HasExited = true;
        StayTime = exitDate.Subtract(EnteredAt);
    }
}
