﻿using ParkingLot.Domain.Abstractions;
using ParkingLot.Domain.Spaces;
using System;

namespace ParkingLot.Domain.SpaceHistory;

public class SpaceHistoryEntry : IBaseEntity
{
    public SpaceHistoryEntry(Space space, DateTimeOffset occupationTime)
    {
        if (space is null)
        {
            throw new ArgumentNullException(nameof(space));
        }

        Space = space;
        TakenAt = occupationTime;
    }

    private SpaceHistoryEntry()
    {

    }

    /// <summary>
    /// Gets or sets the entity identifier.
    /// </summary>
    public int Id { get; private set; }

    public bool IsReleased { get; private set; }

    public TimeSpan? OccupationTime { get; private set; }

    public DateTimeOffset? ReleasedAt { get; private set; }

    public Space Space { get; private set; }

    public int SpaceId { get; private set; }

    public DateTimeOffset TakenAt { get; private set; }

    public void Release(DateTimeOffset releaseDate)
    {
        if (IsReleased)
        {
            throw new InvalidOperationException(
                $"Space history entry must not be already released " +
                $"in order to be saved as released.");
        }

        if (releaseDate < TakenAt)
        {
            throw new ArgumentException(
                $"{nameof(releaseDate)} must be later date than date of taking the releated space.",
                nameof(releaseDate));
        }

        IsReleased = true;
        OccupationTime = releaseDate.Subtract(TakenAt);
        ReleasedAt = releaseDate;
    }
}
