﻿namespace ParkingLot.Domain.Vehicles;

public enum VehicleType
{
    Unknown = 0,
    Car,
    Motorcycle,
}
