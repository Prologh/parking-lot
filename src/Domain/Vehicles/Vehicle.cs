﻿using ParkingLot.Domain.Abstractions;
using ParkingLot.Domain.VehicleHistory;
using System;
using System.Collections.Generic;

namespace ParkingLot.Domain.Vehicles;

/// <summary>
/// Represent a vehicle.
/// </summary>
public class Vehicle : BaseEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Vehicle"/> class.
    /// </summary>
    public Vehicle()
    {
        History = new List<VehicleHistoryEntry>();
    }

    /// <summary>
    /// Gets or sets a list of vehicle history entries.
    /// </summary>
    public IList<VehicleHistoryEntry> History { get; set; }

    /// <summary>
    /// Gets or sets the license plate number.
    /// </summary>
    public string LicensePlate { get; set; }

    /// <summary>
    /// Gets or sets the vehicle type.
    /// </summary>
    public VehicleType Type { get; set; }
}
