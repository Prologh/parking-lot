﻿namespace ParkingLot;

/// <summary>
/// Contains all kind of constant values used accross the application.
/// </summary>
public static class Constants
{
    /// <summary>
    /// The very sophisticated name of this web application.
    /// </summary>
    public const string ApplicationName = "Parking Lot";
}
