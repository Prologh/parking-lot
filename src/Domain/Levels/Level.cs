﻿using ParkingLot.Domain.Abstractions;
using ParkingLot.Domain.Spaces;
using System.Collections.Generic;

namespace ParkingLot.Domain.Levels;

/// <summary>
/// Represents parking level.
/// </summary>
public class Level : BaseEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Level"/> class.
    /// </summary>
    public Level(int position)
    {
        Position = position;
        Spaces = new List<Space>();
    }

    /// <summary>
    /// Gets or sets the physical position (order in building).
    /// </summary>
    public int Position { get; private set; }

    /// <summary>
    /// Gets or sets the list of spaces which current parking level has.
    /// </summary>
    public IList<Space> Spaces { get; private set; }

    /// <summary>
    /// Sets level position.
    /// </summary>
    /// <param name="position">Level new position.</param>
    public void SetPosition(int position)
    {
        Position = position;
    }
}
