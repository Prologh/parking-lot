﻿using System;

namespace ParkingLot.Domain.Abstractions;

/// <summary>
/// Represents base entity.
/// </summary>
public abstract class BaseEntity : IBaseEntity, IAuditableEntity
{
    /// <summary>
    /// Gets or sets the entity identifier.
    /// </summary>
    public int Id { get; private set; }

    /// <summary>
    /// Gets or sets date and time of creation.
    /// </summary>
    public DateTimeOffset Created { get; private set; }

    /// <summary>
    /// Gets or sets date and time of last modification.
    /// </summary>
    public DateTimeOffset? Updated { get; private set; }

    /// <summary>
    /// Sets date and time of entity creation.
    /// </summary>
    /// <exception cref="ArgumentException">
    /// <paramref name="creationData"/> is <see cref="DateTimeOffset.MinValue"/>.
    /// </exception>
    /// <exception cref="InvalidOperationException">
    /// Entity already has creation date.
    /// </exception>
    public void SetCreationDate(DateTimeOffset creationData)
    {
        if (creationData == default)
        {
            throw new ArgumentException(
                "Creation date for entity cannot be default date.",
                nameof(creationData));
        }

        if (Created != default)
        {
            throw new InvalidOperationException(
                "Cannot set creation date for entity that already has creation date set.");
        }

        Created = creationData;
    }

    /// <summary>
    /// Sets date and time of last modification.
    /// </summary>
    /// <exception cref="ArgumentException">
    /// <paramref name="creationData"/> is <see cref="DateTimeOffset.MinValue"/>.
    /// </exception>
    /// <exception cref="ArgumentException">
    /// <paramref name="creationData"/> is earlier than existing last update date.
    /// </exception>
    public void SetLastUpdateDate(DateTimeOffset lastUpdateDate)
    {
        if (lastUpdateDate == default)
        {
            throw new ArgumentException(
                "Creation date for entity cannot be default date.",
                nameof(lastUpdateDate));
        }

        if (Updated != null && lastUpdateDate < Updated)
        {
            throw new ArgumentException(
                "Cannot set last update date that is earlier than existing last update date.",
                nameof(lastUpdateDate));
        }

        Updated = lastUpdateDate;
    }
}
