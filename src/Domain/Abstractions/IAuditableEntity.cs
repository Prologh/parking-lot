﻿using System;

namespace ParkingLot.Domain.Abstractions;

/// <summary>
/// Represents entity with time stampts for creation and last modification date.
/// </summary>
public interface IAuditableEntity
{
    /// <summary>
    /// Gets or sets date and time of creation.
    /// </summary>
    DateTimeOffset Created { get; }

    /// <summary>
    /// Gets or sets date and time of last modification.
    /// </summary>
    DateTimeOffset? Updated { get; }

    /// <summary>
    /// Sets date and time of entity creation.
    /// </summary>
    void SetCreationDate(DateTimeOffset creationDate);

    /// <summary>
    /// Sets date and time of last modification.
    /// </summary>
    void SetLastUpdateDate(DateTimeOffset lastUpdateDate);
}
