﻿namespace ParkingLot.Domain.Abstractions;

/// <summary>
/// Represents base identifiable entity.
/// </summary>
public interface IBaseEntity
{
    /// <summary>
    /// Gets or sets the entity identifier.
    /// </summary>
    int Id { get; }
}
