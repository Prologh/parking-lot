﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;

namespace ParkingLot.Infrastructure.Http.Content.Builders;

/// <summary>
/// Provides means of building <see cref="FormUrlEncodedContent"/> from
/// complex objects leveraging <see cref="JObject"/>.
/// </summary>
public static class FormUrlEncodedContentBuilder
{
    public static FormUrlEncodedContent BuildContent(object @object)
    {
        var dictionary = BuildDictionary(@object);

        return new FormUrlEncodedContent(dictionary);
    }

    public static IDictionary<string, string> BuildDictionary(object @object)
    {
        if (@object == null)
        {
            return new Dictionary<string, string>();
        }

        if (!(@object is JToken token))
        {
            return BuildDictionary(JObject.FromObject(@object));
        }

        if (token.HasValues)
        {
            var contentData = new Dictionary<string, string>();
            foreach (var child in token.Children().ToList())
            {
                var childContent = BuildDictionary(child);
                if (childContent.Any())
                {
                    contentData = contentData.Concat(childContent)
                        .ToDictionary(k => k.Key, v => v.Value);
                }
            }

            return contentData;
        }

        var jValue = token as JValue;
        if (jValue?.Value == null)
        {
            return new Dictionary<string, string>();
        }

        var value = jValue?.Type == JTokenType.Date ?
            jValue?.ToString("o", CultureInfo.InvariantCulture) :
            jValue?.ToString(CultureInfo.InvariantCulture);

        return new Dictionary<string, string> { { token.Path, value } };
    }
}
