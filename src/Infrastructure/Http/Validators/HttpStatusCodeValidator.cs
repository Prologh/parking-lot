﻿using ParkingLot.Application.Http.Validators;
using System;
using System.Net;

namespace ParkingLot.Infrastructure.Http.Validators;

/// <summary>
/// Provides methods for checking correctness of provided HTTP status codes.
/// </summary>
public class HttpStatusCodeValidator : IHttpStatusCodeValidator
{
    public HttpStatusCodeValidator()
    {

    }

    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether provided
    /// <see cref="int"/> is a valid HTTP status code.
    /// </summary>
    /// <param name="httpStatusCode">
    /// HTTP status code represented as <see cref="int"/>.
    /// </param>
    /// <returns>
    /// A <see cref="bool"/> value indicating whether provided
    /// <see cref="int"/> value is a valid HTTP status code.
    /// </returns>
    public bool CheckIfValid(int httpStatusCode)
    {
        return Enum.IsDefined(typeof(HttpStatusCode), httpStatusCode);
    }

    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether provided
    /// <see cref="string"/> is a valid HTTP status code.
    /// </summary>
    /// <param name="httpStatusCode">
    /// HTTP status code represented as <see cref="string"/>.
    /// </param>
    /// <returns>
    /// A <see cref="bool"/> value indicating whether provided
    /// <see cref="string"/> value is a valid HTTP status code.
    /// </returns>
    public bool CheckIfValid(string httpStatusCode)
    {
        return Enum.TryParse<HttpStatusCode>(httpStatusCode, out _);
    }

    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether provided
    /// <see cref="string"/> is a valid HTTP status code.
    /// </summary>
    /// <param name="httpStatusCode">
    /// HTTP status code represented as <see cref="string"/>.
    /// </param>
    /// <param name="ignoreCase">
    /// <see langword="true"/> to ignore case; <see langword="false"/>
    /// to consider case.
    /// </param>
    /// <returns>
    /// A <see cref="bool"/> value indicating whether provided
    /// <see cref="string"/> value is a valid HTTP status code.
    /// </returns>
    public bool CheckIfValid(string httpStatusCode, bool ignoreCase)
    {
        return Enum.TryParse<HttpStatusCode>(httpStatusCode, ignoreCase, out _);
    }
}
