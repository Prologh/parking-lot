﻿using ParkingLot.Application.Http.Humanizers;
using ParkingLot.Application.Http.Validators;
using System;
using System.Net;

namespace ParkingLot.Infrastructure.Http.Humanizers;

/// <summary>
/// Provides methods for getting human-readable (friendly) HTTP status codes.
/// </summary>
public class HttpStatusCodeHumanizer : IHttpStatusCodeHumanizer
{
    private readonly IHttpStatusCodeValidator _validator;

    public HttpStatusCodeHumanizer(IHttpStatusCodeValidator validator)
    {
        _validator = validator;
    }

    /// <summary>
    /// Gets humanized message based on provided HTTP status code.
    /// </summary>
    /// <param name="httpStatusCode">
    /// HTTP status code represented as <see cref="int"/>.
    /// </param>
    /// <returns>
    /// Human readable <see cref="string"/> message representing provided
    /// HTTP status code.
    /// </returns>
    /// <exception cref="ArgumentException">
    /// <paramref name="httpStatusCode"/> is not a valid HTTP status code.
    /// </exception>
    public string HumanizeHttpStatusCode(int httpStatusCode)
    {
        if (!_validator.CheckIfValid(httpStatusCode))
        {
            throw new ArgumentException(
                $"Provided {nameof(httpStatusCode)} {httpStatusCode} " +
                $"cannot be parsed to {typeof(HttpStatusCode).FullName}.\n" +
                $"Please provide a valid HTTP status code.");
        }

        return HumanizeHttpStatusCode((HttpStatusCode)Enum.Parse(typeof(HttpStatusCode), httpStatusCode.ToString()));
    }

    /// <summary>
    /// Gets humanized message based on provided HTTP status code.
    /// </summary>
    /// <param name="httpStatusCode">
    /// HTTP status code represented as <see cref="HttpStatusCode"/>.
    /// </param>
    /// <returns>
    /// Human readable <see cref="string"/> message representing provided
    /// HTTP status code.
    /// </returns>
    public string HumanizeHttpStatusCode(HttpStatusCode httpStatusCode)
    {
        var message = string.Empty;
        var httpErrorCodeString = httpStatusCode.ToString();

        if (httpStatusCode == HttpStatusCode.OK)
        {
            return HttpStatusCode.OK.ToString();
        }

        if (httpStatusCode == HttpStatusCode.OK)
        {
            return "OK.";
        }

        for (int i = 0; i < httpErrorCodeString.Length; i++)
        {
            var sign = httpErrorCodeString[i];
            if (i != 0 && char.IsUpper(sign))
            {
                message += " " + char.ToLower(sign);
            }
            else
            {
                message += sign.ToString();
            }
        }

        return $"{message}";
    }
}
