﻿using Fluorite.Strainer.Models.Configuration;
using Fluorite.Strainer.Models.Filtering;
using Fluorite.Strainer.Models.Metadata;
using Fluorite.Strainer.Services.Configuration;
using ParkingLot.Application.Strainer.Filtering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ParkingLot.Infrastructure.Strainer.Filtering;

public class StrainerFiltersProvider : IStrainerFiltersProvider
{
    private readonly IStrainerConfiguration _strainerConfiguration;

    public StrainerFiltersProvider(IStrainerConfigurationProvider strainerConfiguration)
    {
        _strainerConfiguration = strainerConfiguration.GetStrainerConfiguration();
    }

    public ModelFilters GetModelFilters(Type modelType)
    {
        if (modelType is null)
        {
            throw new ArgumentNullException(nameof(modelType));
        }

        var customFilterMethods = GetCustomFilterMethods(modelType);
        var filterableProperties = GetFilterableProperties(modelType);

        return new ModelFilters(
            customFilterMethods,
            filterableProperties,
            modelType);
    }

    private IReadOnlyList<IPropertyMetadata> GetFilterableProperties(Type modelType)
    {
        if (!_strainerConfiguration.PropertyMetadata.TryGetValue(modelType, out var properties))
        {
            return new List<IPropertyMetadata>().AsReadOnly();
        }

        return properties.Values.Where(p => p.IsFilterable).ToList().AsReadOnly();
    }

    private IReadOnlyList<ICustomFilterMethod> GetCustomFilterMethods(Type modelType)
    {
        if (!_strainerConfiguration.CustomFilterMethods.TryGetValue(modelType, out var methods))
        {
            return new List<ICustomFilterMethod>().AsReadOnly();
        }

        return methods.Values.ToList().AsReadOnly();
    }
}
