﻿using ParkingLot.Application.Strainer.Mapping;
using System;

namespace ParkingLot.Infrastructure.Strainer.Mapping;

public class StrainerTypeMapper : IStrainerTypeMapper
{
    private readonly IStrainerTypeMapperConfiguration _strainerTypeMapperConfiguration;

    public StrainerTypeMapper(IStrainerTypeMapperConfiguration strainerTypeMapperConfiguration)
    {
        _strainerTypeMapperConfiguration = strainerTypeMapperConfiguration
            ?? throw new ArgumentNullException(nameof(strainerTypeMapperConfiguration));
    }

    public Type Map(Type sourceType)
    {
        if (sourceType is null)
        {
            throw new ArgumentNullException(nameof(sourceType));
        }

        var typeMappings = _strainerTypeMapperConfiguration.GetMappings();

        if (typeMappings.TryGetValue(sourceType, out var destinationType))
        {
            return destinationType;
        }

        return null;
    }
}
