﻿using ParkingLot.Application.Strainer.Mapping;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ParkingLot.Infrastructure.Strainer.Mapping;

public class InMemoryStrainerTypeMapperConfiguration : IStrainerTypeMapperConfiguration
{
    private readonly ReadOnlyDictionary<Type, Type> _typeMappings;

    public InMemoryStrainerTypeMapperConfiguration(
        IEnumerable<IStrainerTypeMapperModule> strainerTypeMapperModules)
    {
        _typeMappings = new ReadOnlyDictionary<Type, Type>(
            strainerTypeMapperModules
                .SelectMany(module =>
                {
                    return module
                        .SourceTypes
                        .Select(type =>
                        {
                            return new KeyValuePair<Type, Type>(type, module.DestinationType);
                        });
                })
                .ToDictionary(pair => pair.Key, pair => pair.Value));
    }

    public IReadOnlyDictionary<Type, Type> GetMappings() => _typeMappings;
}
