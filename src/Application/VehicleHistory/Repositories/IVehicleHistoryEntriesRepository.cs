﻿using ParkingLot.Application.Repositories;
using ParkingLot.Domain.VehicleHistory;

namespace ParkingLot.Application.VehicleHistory.Repositories;

public interface IVehicleHistoryEntriesRepository : IRepository<VehicleHistoryEntry>
{

}
