﻿using ParkingLot.Application.Charts.ViewModels;
using System;

namespace ParkingLot.Application.VehicleHistory.ViewModels;

public class VehicleHistoryChartVM : ChartVM<int>
{
    public VehicleHistoryChartVM()
    {

    }

    public TimeSpan TimeSpan { get; set; }
}
