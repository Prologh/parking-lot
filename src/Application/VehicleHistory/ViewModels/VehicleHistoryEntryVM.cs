﻿using ParkingLot.Application.Abstractions;
using ParkingLot.Application.Vehicles.ViewModels;
using System;

namespace ParkingLot.Application.VehicleHistory.ViewModels;

public class VehicleHistoryEntryVM : IBaseViewModel
{
    public VehicleHistoryEntryVM()
    {

    }

    public DateTimeOffset EnteredAt { get; set; }

    public DateTimeOffset? ExitedAt { get; set; }

    public int Id { get; set; }

    public bool HasExited { get; set; }

    public TimeSpan? StayTime { get; set; }

    public VehicleVM Vehicle { get; set; }

    public int VehicleId { get; set; }

    public TimeSpan CalculateStayTime()
    {
        return HasExited
            ? StayTime.Value
            : DateTimeOffset.Now.Subtract(EnteredAt);
    }
}
