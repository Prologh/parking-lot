﻿using Fluorite.Strainer.Services.Modules;
using ParkingLot.Domain.VehicleHistory;

namespace ParkingLot.Application.VehicleHistory.Modules;

public class VehicleHistoryEntriesStrainerModule : StrainerModule
{
    public VehicleHistoryEntriesStrainerModule()
    {

    }

    public override void Load(IStrainerModuleBuilder builder)
    {
        builder.AddProperty<VehicleHistoryEntry>(e => e.EnteredAt)
            .IsFilterable()
            .IsSortable()
            .IsDefaultSort(isDescending: true);
        builder.AddProperty<VehicleHistoryEntry>(e => e.ExitedAt)
            .IsSortable()
            .IsFilterable();
        builder.AddProperty<VehicleHistoryEntry>(e => e.Vehicle.LicensePlate)
            .IsSortable()
            .IsFilterable();
        builder.AddProperty<VehicleHistoryEntry>(p => p.StayTime)
            .IsSortable()
            .IsFilterable();
    }
}
