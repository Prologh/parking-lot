﻿using ParkingLot.Application.Strainer.Mapping;
using ParkingLot.Application.VehicleHistory.ViewModels;
using ParkingLot.Domain.VehicleHistory;

namespace ParkingLot.Application.VehicleHistory.Modules;

public class VehicleHistoryStrainerTypeMapperModule : StrainerTypeMapperModule<VehicleHistoryEntry>
{
    public VehicleHistoryStrainerTypeMapperModule()
    {
        CreateMap<VehicleHistoryEntryVM>();
    }
}
