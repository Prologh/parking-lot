﻿using AutoMapper;
using ParkingLot.Application.VehicleHistory.ViewModels;
using ParkingLot.Domain.VehicleHistory;

namespace ParkingLot.Application.VehicleHistory.Mapping;

public class VehicleHistoryEntriesMappingProfile : Profile
{
    public VehicleHistoryEntriesMappingProfile()
    {
        CreateMap<VehicleHistoryEntry, VehicleHistoryEntryVM>();
    }

    public override string ProfileName => nameof(VehicleHistoryEntriesMappingProfile);
}
