﻿using ParkingLot.Application.VehicleHistory.ViewModels;
using System;
using System.Threading.Tasks;

namespace ParkingLot.Application.SpaceHistory.Queries;

public interface IGetVehicleHistoryChartQuery
{
    Task<VehicleHistoryChartVM> GetAsync(DateTimeOffset from, DateTimeOffset to, TimeSpan step);
}
