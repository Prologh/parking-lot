﻿using System;

namespace ParkingLot.Application.Exceptions.Providers;

/// <summary>
/// Defines minimum requirements for error message provider service.
/// </summary>
public interface IExceptionMessageProvider
{
    /// <summary>
    /// Gets the end exception message based on its type.
    /// </summary>
    /// <param name="exception">
    /// <see cref="Exception"/> source instance.
    /// </param>
    /// <returns>
    /// Proper <see cref="string"/> message without fragile information.
    /// </returns>
    string GetExceptionMessage(Exception exception);
}