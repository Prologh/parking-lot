﻿using System.Threading.Tasks;

namespace ParkingLot.Application;

public interface IUnitOfWork
{
    /// <summary>
    /// Asynchronously saves changes to the database.
    /// </summary>
    Task<int> SaveChangesAsync();
}
