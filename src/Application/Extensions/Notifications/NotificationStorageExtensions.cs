﻿using ParkingLot.Application.Notifications.Builders;
using ParkingLot.Application.Notifications.Storages.Abstractions;
using System;

namespace ParkingLot.Extensions.Notifications;

public static class NotificationStorageExtensions
{
    public static INotificationBuilder Builder(this INotificationStorage notificationStorage)
    {
        if (notificationStorage == null)
        {
            throw new ArgumentNullException(nameof(notificationStorage));
        }

        return new NotificationBuilder(notificationStorage);
    }
}
