﻿using System;
using System.Text;

namespace ParkingLot.Extensions;

public static class StringExtensions
{
    public static string ToPascalCase(this string value)
    {
        if (value is null)
        {
            throw new ArgumentNullException(nameof(value));
        }

        if (value.Length == 0)
        {
            return value;
        }

        var stringBuilder = new StringBuilder();
        stringBuilder.Append(char.ToUpper(value[0]));

        for (int i = 1; i < value.Length; i++)
        {
            var previousSign = value[i - 1];
            var currentSign = value[i];
            if (char.IsWhiteSpace(previousSign))
            {
                stringBuilder.Append(char.ToUpper(currentSign));
            }
            else
            {
                stringBuilder.Append(currentSign);
            }
        }

        return stringBuilder.ToString();
    }
}
