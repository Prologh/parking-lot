﻿using ParkingLot.Application.Pagination.Models;
using System;

namespace ParkingLot.Extensions.Pagination;

/// <summary>
/// Provides extension methods for <see cref="IPagedResult"/>.
/// </summary>
public static class IPagedResultExtensions
{
    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether current page
    /// is the first one.
    /// </summary>
    /// <param name="result">
    /// The current <see cref="IPagedResult"/> instance.
    /// </param>
    /// <returns>
    /// <see langword="true"/> if current page is the first one;
    /// otherwise <see langword="false"/>.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="result"/> is <see langword="null"/>
    /// </exception>
    public static bool IsFirstPage(this IPagedResult result)
    {
        if (result == null)
        {
            throw new ArgumentNullException(nameof(result));
        }

        return result.PageNumber == 1;
    }

    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether current page
    /// is the last one.
    /// </summary>
    /// <param name="result">
    /// The current <see cref="IPagedResult"/> instance.
    /// </param>
    /// <returns>
    /// <see langword="true"/> if current page is the last one;
    /// otherwise <see langword="false"/>.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="result"/> is <see langword="null"/>
    /// </exception>
    public static bool IsLastPage(this IPagedResult result)
    {
        if (result == null)
        {
            throw new ArgumentNullException(nameof(result));
        }

        return result.PageNumber == result.TotalPages;
    }

    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether current page
    /// is the second one.
    /// </summary>
    /// <param name="result">
    /// The current <see cref="IPagedResult"/> instance.
    /// </param>
    /// <returns>
    /// <see langword="true"/> if current page is the second one;
    /// otherwise <see langword="false"/>.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="result"/> is <see langword="null"/>
    /// </exception>
    public static bool IsSecondPage(this IPagedResult result)
    {
        if (result == null)
        {
            throw new ArgumentNullException(nameof(result));
        }

        return result.PageNumber == 2;
    }

    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether current page
    /// is the page before the last one.
    /// </summary>
    /// <param name="result">
    /// The current <see cref="IPagedResult"/> instance.
    /// </param>
    /// <returns>
    /// <see langword="true"/> if current page is the page before the last one;
    /// otherwise <see langword="false"/>.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="result"/> is <see langword="null"/>
    /// </exception>
    public static bool IsPenultimatePage(this IPagedResult result)
    {
        if (result == null)
        {
            throw new ArgumentNullException(nameof(result));
        }

        return result.PageNumber == result.TotalPages - 1;
    }
}
