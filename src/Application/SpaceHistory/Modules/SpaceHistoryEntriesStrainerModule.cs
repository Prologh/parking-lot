﻿using Fluorite.Strainer.Services.Modules;
using ParkingLot.Domain.SpaceHistory;

namespace ParkingLot.Application.SpaceHistory.Modules;

public class SpaceHistoryEntriesStrainerModule : StrainerModule
{
    public SpaceHistoryEntriesStrainerModule()
    {

    }

    public override void Load(IStrainerModuleBuilder builder)
    {
        builder.AddProperty<SpaceHistoryEntry>(e => e.TakenAt)
            .IsSortable()
            .IsDefaultSort(isDescending: true)
            .IsFilterable();
        builder.AddProperty<SpaceHistoryEntry>(e => e.ReleasedAt)
            .IsSortable()
            .IsFilterable();
        builder.AddProperty<SpaceHistoryEntry>(e => e.Space.Number)
            .IsSortable()
            .IsFilterable();
        builder.AddProperty<SpaceHistoryEntry>(e => e.OccupationTime)
            .IsSortable()
            .IsFilterable();
    }
}
