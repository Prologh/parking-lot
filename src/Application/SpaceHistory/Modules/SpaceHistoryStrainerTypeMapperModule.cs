﻿using ParkingLot.Application.SpaceHistory.ViewModels;
using ParkingLot.Application.Strainer.Mapping;
using ParkingLot.Domain.SpaceHistory;

namespace ParkingLot.Application.SpaceHistory.Modules;

public class SpaceHistoryStrainerTypeMapperModule : StrainerTypeMapperModule<SpaceHistoryEntry>
{
    public SpaceHistoryStrainerTypeMapperModule()
    {
        CreateMap<SpaceHistoryEntryVM>();
    }
}
