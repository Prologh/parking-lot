﻿using AutoMapper;
using ParkingLot.Application.SpaceHistory.ViewModels;
using ParkingLot.Domain.SpaceHistory;

namespace ParkingLot.Application.SpaceHistory.Mapping;

public class SpaceHistoryEntriesMappingProfile : Profile
{
    public SpaceHistoryEntriesMappingProfile()
    {
        CreateMap<SpaceHistoryEntry, SpaceHistoryEntryVM>()
            .ForMember(
                spaceHistoryEntryVM => spaceHistoryEntryVM.Space,
                options => options.AllowNull());
    }

    public override string ProfileName => nameof(SpaceHistoryEntriesMappingProfile);
}
