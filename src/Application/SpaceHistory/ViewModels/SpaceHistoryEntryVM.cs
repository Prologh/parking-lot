﻿using ParkingLot.Application.Abstractions;
using ParkingLot.Application.Spaces.ViewModels;
using System;

namespace ParkingLot.Application.SpaceHistory.ViewModels;

public class SpaceHistoryEntryVM : IBaseViewModel
{
    public SpaceHistoryEntryVM()
    {

    }

    public int Id { get; set; }

    public bool IsReleased { get; set; }

    public TimeSpan? OccupationTime { get; set; }

    public DateTimeOffset? ReleasedAt { get; set; }

    public SpaceListItemVM Space { get; set; }

    public int SpaceId { get; set; }

    public DateTimeOffset TakenAt { get; set; }

    public TimeSpan CalculateOccupationTime()
    {
        return IsReleased
            ? OccupationTime.Value
            : DateTimeOffset.Now.Subtract(TakenAt);
    }
}
