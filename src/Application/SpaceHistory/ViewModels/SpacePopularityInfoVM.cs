﻿namespace ParkingLot.Application.SpaceHistory.ViewModels;

public class SpacePopularityInfoVM
{
    public int Number { get; set; }

    public int TimesTaken { get; set; }
}
