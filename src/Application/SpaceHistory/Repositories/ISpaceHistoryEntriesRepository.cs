﻿using ParkingLot.Application.Repositories;
using ParkingLot.Domain.SpaceHistory;

namespace ParkingLot.Application.SpaceHistory.Repositories;

public interface ISpaceHistoryEntriesRepository : IRepository<SpaceHistoryEntry>
{

}
