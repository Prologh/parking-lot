﻿using ParkingLot.Application.SpaceHistory.ViewModels;
using System.Threading.Tasks;

namespace ParkingLot.Application.SpaceHistory.Queries;

public interface IGetMostPopularSpacesQuery
{
    Task<MostPopularSpacesChartVM> GetAsync(int limit);
}
