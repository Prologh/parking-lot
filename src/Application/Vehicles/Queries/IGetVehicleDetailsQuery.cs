﻿using ParkingLot.Application.Vehicles.ViewModels;
using System.Threading.Tasks;

namespace ParkingLot.Application.Vehicles.Queries;

public interface IGetVehicleDetailsQuery
{
    Task<VehicleVM> GetAsync(int id);
}
