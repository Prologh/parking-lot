﻿using ParkingLot.Application.Abstractions;
using ParkingLot.Application.VehicleHistory.ViewModels;
using ParkingLot.Domain.Vehicles;
using System.Collections.Generic;

namespace ParkingLot.Application.Vehicles.ViewModels;

/// <summary>
/// Represents vehicle view model.
/// </summary>
public class VehicleVM : BaseViewModel
{
    /// <summary>
    /// Initializes a new instance of the <see cref="VehicleVM"/> class.
    /// </summary>
    public VehicleVM()
    {
        History = new List<VehicleHistoryEntryVM>();
    }

    /// <summary>
    /// Gets or sets a list of vehicle history entries.
    /// </summary>
    public IList<VehicleHistoryEntryVM> History { get; set; }

    /// <summary>
    /// Gets or sets the license plate number.
    /// </summary>
    public string LicensePlate { get; set; }

    /// <summary>
    /// Gets or sets the vehicle type.
    /// </summary>
    public VehicleType Type { get; set; }
}
