﻿using FluentValidation;
using ParkingLot.Application.Vehicles.InputModels;

namespace ParkingLot.Application.Vehicles.Validation;

public class CreateVehicleInputModelValidator : AbstractValidator<CreateVehicleInputModel>
{
    public CreateVehicleInputModelValidator()
    {
        RuleFor(v => v.LicensePlate)
            .NotNull()
            .NotEmpty()
            .MaximumLength(20);
    }
}
