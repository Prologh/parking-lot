﻿using ParkingLot.Application.Repositories;
using ParkingLot.Domain.Vehicles;

namespace ParkingLot.Application.Vehicles.Repositories;

public interface IVehiclesRepository : IRepository<Vehicle>
{

}
