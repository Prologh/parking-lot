﻿using AutoMapper;
using ParkingLot.Application.Vehicles.ViewModels;
using ParkingLot.Domain.Vehicles;

namespace ParkingLot.Application.Vehicles.Mapping;

public class VehiclesMappingProfile : Profile
{
    public VehiclesMappingProfile()
    {
        CreateMap<Vehicle, VehicleVM>()
            .ForMember(
                vehicleVM => vehicleVM.History,
                options => options.Ignore());
    }

    public override string ProfileName => nameof(VehiclesMappingProfile);
}
