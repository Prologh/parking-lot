﻿using ParkingLot.Domain.Vehicles;

namespace ParkingLot.Application.Vehicles.InputModels;

public class CreateVehicleInputModel
{
    public CreateVehicleInputModel()
    {

    }

    public string LicensePlate { get; set; }

    public VehicleType Type { get; set; }
}
