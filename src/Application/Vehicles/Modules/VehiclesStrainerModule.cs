﻿using Fluorite.Strainer.Services.Modules;
using ParkingLot.Domain.Vehicles;

namespace ParkingLot.Application.Vehicles.Modules;

public class VehiclesStrainerModule : StrainerModule
{
    public VehiclesStrainerModule()
    {

    }

    public override void Load(IStrainerModuleBuilder builder)
    {
        builder.AddProperty<Vehicle>(v => v.Id)
            .IsFilterable()
            .IsSortable();
        builder.AddProperty<Vehicle>(v => v.Created)
            .IsSortable()
            .IsFilterable();
        builder.AddProperty<Vehicle>(v => v.LicensePlate)
            .IsFilterable()
            .IsSortable()
            .IsDefaultSort();
        builder.AddProperty<Vehicle>(v => v.Updated)
            .IsSortable()
            .IsFilterable();
        builder.AddProperty<Vehicle>(v => v.Type)
            .IsFilterable()
            .IsSortable();
    }
}
