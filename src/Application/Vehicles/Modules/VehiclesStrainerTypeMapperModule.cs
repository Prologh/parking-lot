﻿using ParkingLot.Application.Strainer.Mapping;
using ParkingLot.Application.Vehicles.ViewModels;
using ParkingLot.Domain.Vehicles;

namespace ParkingLot.Application.Vehicles.Modules;

public class VehiclesStrainerTypeMapperModule : StrainerTypeMapperModule<Vehicle>
{
    public VehiclesStrainerTypeMapperModule()
    {
        CreateMap<VehicleVM>();
    }
}
