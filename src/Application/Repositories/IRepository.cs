﻿using ParkingLot.Domain.Abstractions;
using System.Collections.Generic;

namespace ParkingLot.Application.Repositories;

/// <summary>
/// Provides CRUD operations for an entity.
/// </summary>
/// <typeparam name="TEntity">
/// The type of entity.
/// </typeparam>
public interface IRepository<TEntity> : IReadOnlyRepository<TEntity>
    where TEntity : class, IBaseEntity
{
    /// <summary>
    /// Synchronously marks an entity to be added when <see cref="IUnitOfWork.SaveChangesAsync"/>
    /// is called.
    /// </summary>
    /// <param name="entity">
    /// The entity to add.
    /// </param>
    void Add(TEntity entity);

    /// <summary>
    /// Synchronously marks a collection of entities to be added
    /// when <see cref="IUnitOfWork.SaveChangesAsync"/> is called.
    /// </summary>
    /// <param name="entity">
    /// The entities to add.
    /// </param>
    void AddRange(IEnumerable<TEntity> entities);

    /// <summary>
    /// Synchronously marks an entity to be removed when
    /// <see cref="IUnitOfWork.SaveChangesAsync"/> is called.
    /// </summary>
    /// <param name="entity">
    /// The entity to remove.
    /// </param>
    void Remove(TEntity entity);

    /// <summary>
    /// Synchronously marks a collection of entities to be removed
    /// when <see cref="IUnitOfWork.SaveChangesAsync"/> is called.
    /// </summary>
    /// <param name="entity">
    /// The entities to remove.
    /// </param>
    void RemoveRange(IEnumerable<TEntity> entities);

    /// <summary>
    /// Synchronously marks an entity to be updated when <see cref="IUnitOfWork.SaveChangesAsync"/>
    /// is called.
    /// </summary>
    /// <param name="entity">
    /// The entity to update.
    /// </param>
    void Update(TEntity entity);

    /// <summary>
    /// Synchronously marks a collection of entities to be updated
    /// when <see cref="IUnitOfWork.SaveChangesAsync"/> is called.
    /// </summary>
    /// <param name="entity">
    /// The entities to update.
    /// </param>
    void UpdateRange(IEnumerable<TEntity> entities);
}
