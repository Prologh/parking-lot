﻿using ParkingLot.Domain.Abstractions;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot.Application.Repositories;

public interface IReadOnlyRepository<TEntity>
    where TEntity : class, IBaseEntity
{
    /// <summary>
    /// Gets the <see cref="IQueryable{TEntity}"/> for executing queries upon it.
    /// </summary>
    IQueryable<TEntity> Queryable { get; }

    /// <summary>
    /// Asynchronously returns single entity matching provided identifiers.
    /// </summary>
    /// <param name="identifiers">
    /// The entity identifiers.
    /// </param>
    Task<TEntity> FindAsync(params object[] identifiers);
}
