﻿using ParkingLot.Application.Notifications.Models;
using System.Collections.Generic;

namespace ParkingLot.Application.Notifications.Storages.Abstractions;

public interface INotificationStorage
{
    INotification GetNotification();

    IList<INotification> GetNotifications();

    bool HasAnyNotifications();

    bool RemoveNotifications();

    void SetNotification(INotification notification);

    void SetNotifications(IEnumerable<INotification> notifications);
}
