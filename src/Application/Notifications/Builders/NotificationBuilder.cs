﻿using ParkingLot.Application.Notifications.Models;
using ParkingLot.Application.Notifications.Storages.Abstractions;
using System;

namespace ParkingLot.Application.Notifications.Builders;

public class NotificationBuilder : INotificationBuilder
{
    private readonly Notification _notification;

    public NotificationBuilder()
    {
        _notification = new Notification();
    }

    public NotificationBuilder(INotificationStorage notificationStorage)
    {
        _notification = new Notification();
        NotificationStorage = notificationStorage ?? throw new ArgumentNullException(nameof(notificationStorage));
    }

    public bool CanSetNotification => NotificationStorage != null;

    protected INotificationStorage NotificationStorage { get; }

    public INotificationAnimationBuilder Animate()
    {
        throw new NotImplementedException();
    }

    public INotification Build()
    {
        return _notification;
    }

    public INotificationBuilder Dismissable(bool isDismissable = true)
    {
        throw new NotImplementedException();
    }

    public INotificationBuilder Duration(TimeSpan duration)
    {
        throw new NotImplementedException();
    }

    public INotificationBuilder Duration(int miliseconds)
    {
        throw new NotImplementedException();
    }

    public INotificationBuilder Element(string parentElementSelector)
    {
        throw new NotImplementedException();
    }

    public INotificationBuilder Icon(string cssClassOrImgUrl)
    {
        _notification.Icon = cssClassOrImgUrl ?? throw new ArgumentNullException(nameof(cssClassOrImgUrl));

        return this;
    }

    public INotificationBuilder Message(string message)
    {
        _notification.Message = message ?? throw new ArgumentNullException(nameof(message));

        return this;
    }

    public INotificationBuilder MouseOverStopsDurationCounter(bool mouseOverStopsDurationCounter = true)
    {
        throw new NotImplementedException();
    }

    public INotificationBuilder Offset(int paddingPixels)
    {
        throw new NotImplementedException();
    }

    public INotificationBuilder Offset(int horizontalPaddingPixels, int verticalPaddingPixels)
    {
        throw new NotImplementedException();
    }

    public INotificationPlacementBuilder Placement()
    {
        throw new NotImplementedException();
    }

    public INotificationBuilder Position(NotificationPosition position)
    {
        throw new NotImplementedException();
    }

    public void SetNotification()
    {
        NotificationStorage.SetNotification(_notification);
    }

    public INotificationBuilder Spacing(int spacingPixels)
    {
        throw new NotImplementedException();
    }

    public INotificationBuilder Template(string htmlTemplate)
    {
        throw new NotImplementedException();
    }

    public INotificationBuilder Timer(TimeSpan timer)
    {
        throw new NotImplementedException();
    }

    public INotificationBuilder Timer(int miliseconds)
    {
        throw new NotImplementedException();
    }

    public INotificationBuilder Type(NotificationType type)
    {
        _notification.Type = type;

        return this;
    }

    public INotificationBuilder UrlTarget(NotificationUrlTarget urlTarget)
    {
        throw new NotImplementedException();
    }

    public INotificationBuilder VisibleProgressBar(bool isProgressBarvisible = false)
    {
        throw new NotImplementedException();
    }

    public INotificationBuilder ZIndex(int zIndex)
    {
        throw new NotImplementedException();
    }

    public INotificationBuilder Title(string title)
    {
        _notification.Title = title ?? throw new ArgumentNullException(nameof(title));

        return this;
    }
}
