﻿using ParkingLot.Application.Notifications.Models;

namespace ParkingLot.Application.Notifications.Builders;

public interface INotificationPlacementBuilder
{
    INotificationBuilder NotificationBuilder { get; }

    INotificationPlacementBuilder From(string from = "top");

    INotificationPlacementBuilder Align(AlignmentType alignmentType);
}