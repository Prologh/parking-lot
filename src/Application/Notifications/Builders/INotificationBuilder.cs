﻿using ParkingLot.Application.Notifications.Models;
using System;

namespace ParkingLot.Application.Notifications.Builders;

public interface INotificationBuilder
{
    bool CanSetNotification { get; }

    INotificationAnimationBuilder Animate();

    INotification Build();

    INotificationBuilder Dismissable(bool isDismissable = true);

    INotificationBuilder Duration(TimeSpan duration);

    INotificationBuilder Duration(int miliseconds);

    INotificationBuilder Element(string parentElementSelector);

    INotificationBuilder Icon(string cssClassOrImgUrl);

    INotificationBuilder Message(string message);

    INotificationBuilder MouseOverStopsDurationCounter(bool mouseOverStopsDurationCounter = true);

    INotificationBuilder Offset(int paddingPixels);

    INotificationBuilder Offset(int horizontalPaddingPixels, int verticalPaddingPixels);

    INotificationPlacementBuilder Placement();

    INotificationBuilder Position(NotificationPosition position);

    void SetNotification();

    INotificationBuilder Spacing(int spacingPixels);

    INotificationBuilder Template(string htmlTemplate);

    INotificationBuilder Timer(TimeSpan timer);

    INotificationBuilder Timer(int miliseconds);

    INotificationBuilder Title(string title);

    INotificationBuilder Type(NotificationType type);

    INotificationBuilder UrlTarget(NotificationUrlTarget urlTarget);

    INotificationBuilder VisibleProgressBar(bool isProgressBarvisible = false);

    INotificationBuilder ZIndex(int zIndex);
}