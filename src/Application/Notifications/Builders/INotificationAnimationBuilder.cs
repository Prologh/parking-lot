﻿namespace ParkingLot.Application.Notifications.Builders;

public interface INotificationAnimationBuilder
{
    INotificationBuilder NotificationBuilder { get; }
}