﻿namespace ParkingLot.Application.Notifications.Models;

public enum NotificationUrlTarget
{
    Blank = 0,
    Self,
    Parent,
    Top,
}
