﻿namespace ParkingLot.Application.Notifications.Models;

public enum NotificationType
{
    Primary = 0,
    Secondary,
    Info,
    Success,
    Warning,
    Danger,
    Light,
    Dark,
}
