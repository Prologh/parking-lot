﻿namespace ParkingLot.Application.Notifications.Models;

public enum AlignmentType
{
    Left = 0,
    Right,
    Center,
    Justify,
    Initial,
    Inherit,
}
