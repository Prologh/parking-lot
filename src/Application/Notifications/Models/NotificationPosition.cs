﻿namespace ParkingLot.Application.Notifications.Models;

public enum NotificationPosition
{
    Null = 0,
    Static,
    Fixed,
    Relative,
    Absolute,
}
