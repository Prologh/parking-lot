﻿namespace ParkingLot.Application.Notifications.Models;

public class Notification : INotification
{
    public Notification()
    {

    }

    public string Icon { get; set; }

    public string Message { get; set; }

    public string Title { get; set; }

    public NotificationType Type { get; set; }
}
