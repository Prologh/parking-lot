﻿namespace ParkingLot.Application.Notifications.Models;

public interface INotification
{
    string Icon { get; }

    string Message { get; }

    string Title { get; }

    NotificationType Type { get; }
}
