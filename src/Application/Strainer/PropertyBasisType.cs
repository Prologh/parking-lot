﻿namespace ParkingLot.Application.Strainer;

public enum PropertyBasisType
{
    Unknown = 0,
    String,
    Number,
    Integer,
    DateTime,
    Boolean,
}
