﻿using Fluorite.Strainer.Models.Sorting;

namespace ParkingLot.Application.Strainer.Sorting.Services;

public interface ISortExpressionProvider
{
    ISortExpression<T> GetDefaultSortExpression<T>();

    ISortExpression<T> GetSortExpression<T>(string name);

    ISortExpression<T> GetSortExpressionOrDefault<T>(string name);
}
