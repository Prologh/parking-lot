﻿using Fluorite.Strainer.Models.Sorting;
using Fluorite.Strainer.Services.Metadata;
using System.Linq;
using IStrainerSortExpressionProvider = Fluorite.Strainer.Services.Sorting.ISortExpressionProvider;
using IStrainerSortTermParser = Fluorite.Strainer.Services.Sorting.ISortTermParser;

namespace ParkingLot.Application.Strainer.Sorting.Services;

public class ParkingLotSortExpressionProvider : ISortExpressionProvider
{
    private readonly IMetadataFacade _metadataFacade;
    private readonly IStrainerSortExpressionProvider _sortExpressionProvider;
    private readonly IStrainerSortTermParser _sortTermParser;

    public ParkingLotSortExpressionProvider(
        IMetadataFacade metadataFacade,
        IStrainerSortExpressionProvider sortExpressionProvider,
        IStrainerSortTermParser sortTermParser)
    {
        _metadataFacade = metadataFacade;
        _sortExpressionProvider = sortExpressionProvider;
        _sortTermParser = sortTermParser;
    }

    public ISortExpression<T> GetDefaultSortExpression<T>()
    {
        return _sortExpressionProvider.GetDefaultExpression<T>();
    }

    public ISortExpression<T> GetSortExpression<T>(string input)
    {
        if (string.IsNullOrWhiteSpace(input))
        {
            return _sortExpressionProvider.GetDefaultExpression<T>();
        }

        var parsedTerm = _sortTermParser
            .GetParsedTerms(input)
            .FirstOrDefault();

        if (parsedTerm == null)
        {
            return _sortExpressionProvider.GetDefaultExpression<T>();
        }

        var metadata = _metadataFacade.GetMetadata<T>(
                isSortableRequired: true,
                isFilterableRequired: false,
                name: parsedTerm.Name);

        if (metadata != null)
        {
            return _sortExpressionProvider.GetExpression<T>(
                    metadata.PropertyInfo,
                    parsedTerm,
                    isSubsequent: false)
                ?? _sortExpressionProvider.GetDefaultExpression<T>();
        }

        return _sortExpressionProvider.GetDefaultExpression<T>();
    }

    public ISortExpression<T> GetSortExpressionOrDefault<T>(string input)
    {
        return GetSortExpression<T>(input)
            ?? GetDefaultSortExpression<T>();
    }
}
