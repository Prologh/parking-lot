﻿using ParkingLot.Domain.Abstractions;
using System;
using System.Collections.Generic;

namespace ParkingLot.Application.Strainer.Mapping;

public abstract class StrainerTypeMapperModule<TEntity> : IStrainerTypeMapperModule
    where TEntity : class, IBaseEntity
{
    private readonly List<Type> _sourceTypes;

    protected StrainerTypeMapperModule()
    {
        _sourceTypes = new List<Type>();
    }

    public Type DestinationType => typeof(TEntity);

    public IReadOnlyCollection<Type> SourceTypes => _sourceTypes.AsReadOnly();

    public void CreateMap<TViewModel>()
        where TViewModel : class
    {
        _sourceTypes.Add(typeof(TViewModel));
    }
}
