﻿using System;

namespace ParkingLot.Application.Strainer.Mapping;

public interface IStrainerTypeMapper
{
    Type Map(Type sourceType);
}
