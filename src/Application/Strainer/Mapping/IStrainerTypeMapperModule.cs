﻿using System;
using System.Collections.Generic;

namespace ParkingLot.Application.Strainer.Mapping;

public interface IStrainerTypeMapperModule
{
    Type DestinationType { get; }

    IReadOnlyCollection<Type> SourceTypes { get; }

    void CreateMap<TViewModel>()
        where TViewModel : class;
}
