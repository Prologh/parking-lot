﻿using System;
using System.Collections.Generic;

namespace ParkingLot.Application.Strainer.Mapping;

public interface IStrainerTypeMapperConfiguration
{
    IReadOnlyDictionary<Type, Type> GetMappings();
}
