﻿using FluentValidation;
using Fluorite.Strainer.Models;

namespace ParkingLot.WebApp.Validators.Pagination;

public class StrainerModelValidator : AbstractValidator<StrainerModel>
{
    public StrainerModelValidator()
    {
        RuleFor(p => p.Page)
            .GreaterThanOrEqualTo(1);
        RuleFor(p => p.PageSize)
            .InclusiveBetween(1, 50);
    }
}
