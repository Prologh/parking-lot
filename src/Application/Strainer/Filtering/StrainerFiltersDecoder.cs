﻿using Fluorite.Strainer.Models.Filtering.Terms;
using Fluorite.Strainer.Models.Metadata;
using Fluorite.Strainer.Services.Filtering;
using System.Collections.Generic;
using System.Linq;

namespace ParkingLot.Application.Strainer.Filtering;

public class StrainerFiltersDecoder : IStrainerFiltersDecoder
{
    private readonly IFilterTermParser _filterTermParser;

    public StrainerFiltersDecoder(IFilterTermParser filterTermParser)
    {
        _filterTermParser = filterTermParser;
    }

    public IReadOnlyList<(IPropertyMetadata, IFilterTerm)> Decode(
        string queryFilters,
        ModelFilters modelFilters)
    {
        var filters = new List<(IPropertyMetadata, IFilterTerm)>();
        var filterTerms = _filterTermParser.GetParsedTerms(queryFilters);

        foreach (var term in filterTerms)
        {
            if (term.Names.Count > 1 || term.Values.Count > 1)
            {
                continue;
            }

            var property = modelFilters.FilterableProperties.FirstOrDefault(x => (x.DisplayName ?? x.Name) == term.Names[0]);
            if (property is not null)
            {
                filters.Add((property, term));
            }
        }

        return filters;
    }
}
