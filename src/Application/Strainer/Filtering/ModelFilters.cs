﻿using Fluorite.Strainer.Models.Filtering;
using Fluorite.Strainer.Models.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ParkingLot.Application.Strainer.Filtering;

public class ModelFilters
{
    public ModelFilters(
        IReadOnlyList<ICustomFilterMethod> customFilterMethods,
        IReadOnlyList<IPropertyMetadata> filterableProperties,
        Type modelType)
    {
        CustomFilterMethods = customFilterMethods ?? throw new ArgumentNullException(nameof(customFilterMethods));
        FilterableProperties = filterableProperties ?? throw new ArgumentNullException(nameof(filterableProperties));
        ModelType = modelType ?? throw new ArgumentNullException(nameof(modelType));
    }

    public bool HasAnyFilters => CustomFilterMethods.Any() || FilterableProperties.Any();

    public IReadOnlyList<ICustomFilterMethod> CustomFilterMethods { get; }

    public IReadOnlyList<IPropertyMetadata> FilterableProperties { get; }

    public Type ModelType { get; }
}
