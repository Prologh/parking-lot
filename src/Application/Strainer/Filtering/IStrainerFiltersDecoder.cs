﻿using Fluorite.Strainer.Models.Filtering.Terms;
using Fluorite.Strainer.Models.Metadata;
using System.Collections.Generic;

namespace ParkingLot.Application.Strainer.Filtering;

public interface IStrainerFiltersDecoder
{
    IReadOnlyList<(IPropertyMetadata, IFilterTerm)> Decode(
        string queryFilters,
        ModelFilters modelFilters);
}
