﻿using System;

namespace ParkingLot.Application.Strainer.Filtering;

public interface IStrainerFiltersProvider
{
    ModelFilters GetModelFilters(Type modelType);
}
