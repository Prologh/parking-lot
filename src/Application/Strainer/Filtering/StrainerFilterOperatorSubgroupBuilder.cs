﻿using Fluorite.Strainer.Models.Filtering.Operators;
using Fluorite.Strainer.Services.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ParkingLot.Application.Strainer.Filtering;

public class StrainerFilterOperatorSubgroupBuilder : IStrainerFilterOperatorSubgroupBuilder
{
    private readonly IStrainerConfigurationProvider _strainerConfigurationProvider;

    public StrainerFilterOperatorSubgroupBuilder(IStrainerConfigurationProvider strainerConfigurationProvider)
    {
        _strainerConfigurationProvider = strainerConfigurationProvider;
    }

    public IReadOnlyDictionary<PropertyBasisType, IReadOnlyList<IFilterOperator>> BuildSubgroups()
    {
        var filterOperatorsSubgroups = new Dictionary<PropertyBasisType, IReadOnlyList<IFilterOperator>>();

        var configuration = _strainerConfigurationProvider.GetStrainerConfiguration();
        var allOperators = configuration.FilterOperators.Values;
        var allOperatorsWithoutCaseSensitive = allOperators
            .Where(f => !f.IsCaseInsensitive)
            .ToList()
            .AsReadOnly();
        var stringOperators = allOperatorsWithoutCaseSensitive
            .Where(f => !f.Symbol.Contains("<") && !f.Symbol.Contains(">"))
            .ToList()
            .AsReadOnly();
        var allOperatorsNonStringBased = allOperatorsWithoutCaseSensitive
            .Where(f => !f.IsStringBased)
            .ToList()
            .AsReadOnly();
        var equalityOperators = allOperatorsWithoutCaseSensitive
            .Where(f => f.Name.Equals("equal", StringComparison.InvariantCultureIgnoreCase)
                || f.Name.Equals("does not equal", StringComparison.InvariantCultureIgnoreCase))
            .ToList()
            .AsReadOnly();

        filterOperatorsSubgroups[PropertyBasisType.String] = stringOperators;
        filterOperatorsSubgroups[PropertyBasisType.Number] = allOperatorsNonStringBased;
        filterOperatorsSubgroups[PropertyBasisType.Integer] = allOperatorsNonStringBased;
        filterOperatorsSubgroups[PropertyBasisType.DateTime] = allOperatorsNonStringBased;
        filterOperatorsSubgroups[PropertyBasisType.Boolean] = equalityOperators;

        return filterOperatorsSubgroups;
    }
}
