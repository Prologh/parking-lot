﻿using Fluorite.Strainer.Models.Filtering.Operators;
using System.Collections.Generic;

namespace ParkingLot.Application.Strainer.Filtering;

public interface IStrainerFilterOperatorSubgroupBuilder
{
    public IReadOnlyDictionary<PropertyBasisType, IReadOnlyList<IFilterOperator>> BuildSubgroups();
}
