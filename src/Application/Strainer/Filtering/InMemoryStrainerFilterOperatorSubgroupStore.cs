﻿using Fluorite.Extensions;
using Fluorite.Strainer.Models.Filtering.Operators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ParkingLot.Application.Strainer.Filtering;

public class InMemoryStrainerFilterOperatorSubgroupStore : IStrainerFilterOperatorSubgroupStore
{
    private readonly IStrainerFilterOperatorSubgroupBuilder _strainerFilterOperatorSubgroupBuilder;

    private IReadOnlyDictionary<PropertyBasisType, IReadOnlyList<IFilterOperator>> _filterOperatorsSubgroups;
    private IReadOnlyDictionary<string, IReadOnlyList<PropertyBasisType>> _filterOperatorsBasisTypes;
    private bool _isInitialized;

    public InMemoryStrainerFilterOperatorSubgroupStore(IStrainerFilterOperatorSubgroupBuilder strainerFilterOperatorSubgroupBuilder)
    {
        _filterOperatorsSubgroups = null;
        _filterOperatorsBasisTypes = null;
        _strainerFilterOperatorSubgroupBuilder = strainerFilterOperatorSubgroupBuilder;
    }

    public IReadOnlyList<PropertyBasisType> GetBasisTypes(IFilterOperator filterOperator)
    {
        if (filterOperator is null)
        {
            throw new ArgumentNullException(nameof(filterOperator));
        }

        if (!_isInitialized)
        {
            Initialize();
        }

        return _filterOperatorsBasisTypes[filterOperator.Symbol];
    }

    public IReadOnlyList<IFilterOperator> GetSubgroup(PropertyBasisType propertyBasisType)
    {
        if (propertyBasisType == PropertyBasisType.Unknown)
        {
            throw new ArgumentException(
                "Cannot provider filter operators subgroup for an unkown property basis type.",
                nameof(propertyBasisType));
        }

        if (!_isInitialized)
        {
            Initialize();
        }

        return _filterOperatorsSubgroups[propertyBasisType];
    }

    public bool TryGetBasisTypes(IFilterOperator filterOperator, out IReadOnlyList<PropertyBasisType> propertyBasisTypes)
    {
        if (filterOperator is null)
        {
            throw new ArgumentNullException(nameof(filterOperator));
        }

        if (!_isInitialized)
        {
            Initialize();
        }

        return _filterOperatorsBasisTypes.TryGetValue(filterOperator.Symbol, out propertyBasisTypes);
    }

    private void Initialize()
    {
        _filterOperatorsSubgroups = _strainerFilterOperatorSubgroupBuilder.BuildSubgroups();
        _filterOperatorsBasisTypes = _filterOperatorsSubgroups
            .Keys
            .Select(basisType => (basisType, _filterOperatorsSubgroups[basisType]))
            .SelectMany(tuple => tuple.Item2.Select(filterOperator => (filterOperator, tuple.basisType)))
            .GroupBy(tuple => tuple.filterOperator.Symbol)
            .ToDictionary(g => g.Key, g => (IReadOnlyList<PropertyBasisType>)g.Select(tuple => tuple.basisType).ToList().AsReadOnly())
            .ToReadOnly();

        _isInitialized = true;
    }
}
