﻿using Fluorite.Strainer.Models.Filtering.Operators;
using System.Collections.Generic;

namespace ParkingLot.Application.Strainer.Filtering;

public interface IStrainerFilterOperatorSubgroupStore
{
    IReadOnlyList<IFilterOperator> GetSubgroup(PropertyBasisType propertyBasisType);

    IReadOnlyList<PropertyBasisType> GetBasisTypes(IFilterOperator filterOperator);

    bool TryGetBasisTypes(IFilterOperator filterOperator, out IReadOnlyList<PropertyBasisType> propertyBasisTypes);
}
