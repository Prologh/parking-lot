﻿using System.Net;

namespace ParkingLot.Application.Http.Humanizers;

/// <summary>
/// Defines minimum requirements for HTTP status code humanizer service.
/// </summary>
public interface IHttpStatusCodeHumanizer
{
    /// <summary>
    /// Gets humanized message based on provided HTTP status code.
    /// </summary>
    /// <param name="httpStatusCode">
    /// HTTP status code represented as <see cref="int"/>.
    /// </param>
    /// <returns>
    /// Human readable <see cref="string"/> message representing provided
    /// HTTP status code.
    /// </returns>
    string HumanizeHttpStatusCode(int httpStatusCode);

    /// <summary>
    /// Gets humanized message based on provided HTTP status code.
    /// </summary>
    /// <param name="httpStatusCode">
    /// HTTP status code represented as <see cref="HttpStatusCode"/>.
    /// </param>
    /// <returns>
    /// Human readable <see cref="string"/> message representing provided
    /// HTTP status code.
    /// </returns>
    string HumanizeHttpStatusCode(HttpStatusCode httpStatusCode);
}