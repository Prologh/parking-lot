﻿namespace ParkingLot.Application.Http.Validators;

/// <summary>
/// Defines minimum requirements for HTTP status code validator service.
/// </summary>
public interface IHttpStatusCodeValidator
{
    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether provided
    /// <see cref="int"/> is a valid HTTP status code.
    /// </summary>
    /// <param name="httpStatusCode">
    /// HTTP status code represented as <see cref="int"/>.
    /// </param>
    /// <returns>
    /// A <see cref="bool"/> value indicating whether provided
    /// <see cref="int"/> value is a valid HTTP status code.
    /// </returns>
    bool CheckIfValid(int httpStatusCode);

    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether provided
    /// <see cref="string"/> is a valid HTTP status code.
    /// </summary>
    /// <param name="httpStatusCode">
    /// HTTP status code represented as <see cref="string"/>.
    /// </param>
    /// <returns>
    /// A <see cref="bool"/> value indicating whether provided
    /// <see cref="string"/> value is a valid HTTP status code.
    /// </returns>
    bool CheckIfValid(string httpStatusCode);

    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether provided
    /// <see cref="string"/> is a valid HTTP status code.
    /// </summary>
    /// <param name="httpStatusCode">
    /// HTTP status code represented as <see cref="string"/>.
    /// </param>
    /// <param name="ignoreCase">
    /// <see langword="true"/> to ignore case; <see langword="false"/>
    /// to consider case.
    /// </param>
    /// <returns>
    /// A <see cref="bool"/> value indicating whether provided
    /// <see cref="string"/> value is a valid HTTP status code.
    /// </returns>
    bool CheckIfValid(string httpStatusCode, bool ignoreCase);
}
