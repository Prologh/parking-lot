﻿namespace ParkingLot.Application.Http.Providers;

/// <summary>
/// Defines minimum requirements for HTTP status code message provider service.
/// </summary>
public interface IHttpStatusCodeMessageProvider
{
    /// <summary>
    /// Gets the error message based on HTTP status code.
    /// </summary>
    /// <param name="httpStatusCode">
    /// HTTP status code represented as <see cref="int"/>.
    /// </param>
    /// <returns>
    /// Proper message indiciting HTTP status code.
    /// </returns>
    string GetHttpStatusCodeMessage(int httpStatusCode);
}
