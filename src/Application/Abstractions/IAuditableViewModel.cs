﻿using System;

namespace ParkingLot.Application.Abstractions;

/// <summary>
/// Represents view model with time stampts for creation and last modification date.
/// </summary>
public interface IAuditableViewModel
{
    /// <summary>
    /// Gets or sets date and time of creation.
    /// </summary>
    DateTimeOffset Created { get; set; }

    /// <summary>
    /// Gets or sets date and time of last modification.
    /// </summary>
    DateTimeOffset? Updated { get; set; }
}
