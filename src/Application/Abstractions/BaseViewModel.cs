﻿using System;

namespace ParkingLot.Application.Abstractions;

/// <summary>
/// Represents base view model.
/// </summary>
public abstract class BaseViewModel : IBaseViewModel, IAuditableViewModel
{
    /// <summary>
    /// Gets or sets the entity identifier.
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets date and time of creation.
    /// </summary>
    public DateTimeOffset Created { get; set; }

    /// <summary>
    /// Gets or sets date and time of last modification.
    /// </summary>
    public DateTimeOffset? Updated { get; set; }
}
