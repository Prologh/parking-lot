﻿namespace ParkingLot.Application.Abstractions;

/// <summary>
/// Represents base identifiable view model.
/// </summary>
public interface IBaseViewModel
{
    /// <summary>
    /// Gets or sets the view model identifier.
    /// </summary>
    int Id { get; set; }
}
