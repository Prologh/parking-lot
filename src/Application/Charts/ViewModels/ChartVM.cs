﻿using System.Collections.Generic;

namespace ParkingLot.Application.Charts.ViewModels;

public class ChartVM<T>
{
    public ChartVM()
    {
        DataSets = new List<DataSetVM<T>>();
        XAxisLabels = new List<string>();
    }

    public IList<DataSetVM<T>> DataSets { get; set; }

    public string Title { get; set; }

    public IList<string> XAxisLabels { get; set; }
}
