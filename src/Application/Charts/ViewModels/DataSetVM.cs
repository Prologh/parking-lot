﻿using System.Collections.Generic;

namespace ParkingLot.Application.Charts.ViewModels;

public class DataSetVM<T>
{
    public DataSetVM()
    {
        Data = new List<T>();
    }

    public string BackgroundColor { get; set; }

    public string BorderColor { get; set; }

    public IList<T> Data { get; set; }

    public string Label { get; set; }
}
