﻿using ParkingLot.Application.Abstractions;

namespace ParkingLot.Application.Levels.ViewModels;

public class LevelMetadataVM : BaseViewModel
{
    public LevelMetadataVM()
    {

    }

    public int Position { get; set; }

    public long OutOfServiceSpacesCount { get; set; }

    public long OccupiedSpacesCount { get; set; }

    public long UnoccupiedSpacesCount { get; set; }

    public long TotalSpacesCount { get; set; }
}
