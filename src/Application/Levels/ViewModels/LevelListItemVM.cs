﻿using ParkingLot.Application.Abstractions;

namespace ParkingLot.Application.Levels.ViewModels;

/// <summary>
/// Represents parking level view model.
/// </summary>
public class LevelListItemVM : BaseViewModel
{
    /// <summary>
    /// Initializes a new instance of the <see cref="LevelListItemVM"/> class.
    /// </summary>
    public LevelListItemVM()
    {

    }

    /// <summary>
    /// Gets or sets the physical position (order in building).
    /// </summary>
    public int Position { get; set; }

    public long SpacesCount { get; set; }
}
