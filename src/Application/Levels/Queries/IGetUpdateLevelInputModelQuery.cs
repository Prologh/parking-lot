﻿using ParkingLot.Application.Levels.InputModels;
using System.Threading.Tasks;

namespace ParkingLot.Application.Levels.Queries;

public interface IGetUpdateLevelInputModelQuery
{
    Task<UpdateLevelInputModel> GetAsync(int id);
}
