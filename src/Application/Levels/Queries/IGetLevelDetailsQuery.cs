﻿using ParkingLot.Application.Levels.ViewModels;
using System.Threading.Tasks;

namespace ParkingLot.Application.Levels.Queries;

public interface IGetLevelDetailsQuery
{
    Task<LevelVM> GetAsync(int id);
}
