﻿using ParkingLot.Application.Levels.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ParkingLot.Application.Levels.Queries;

public interface IGetLevelMetadatasQuery
{
    Task<IList<LevelMetadataVM>> GetAsync();
}
