﻿using System.Threading.Tasks;

namespace ParkingLot.Application.Levels.Queries;

public interface ICheckAllLevelSpacesAreUnoccupiedQuery
{
    Task<bool> CheckAsync(int id);
}
