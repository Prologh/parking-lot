﻿using AutoMapper;
using ParkingLot.Application.Levels.InputModels;
using ParkingLot.Application.Levels.ViewModels;
using ParkingLot.Application.SelectLists.Models;
using ParkingLot.Domain.Levels;
using ParkingLot.Domain.Spaces;
using System.Linq;

namespace ParkingLot.Application.Levels.Mapping;

public class LevelsMappingProfile : Profile
{
    public LevelsMappingProfile()
    {
        CreateMap<Level, LevelVM>()
            .ForMember(
                dest => dest.SpacesCount,
                opts => opts.MapFrom(src => src.Spaces.LongCount()));
        CreateMap<Level, LevelListItemVM>()
            .ForMember(
                dest => dest.SpacesCount,
                opts => opts.MapFrom(src => src.Spaces.LongCount()));
        CreateMap<Level, UpdateLevelInputModel>();
        CreateMap<Level, SelectListItem>()
            .ForMember(
                selectListItem => selectListItem.Value,
                options => options.MapFrom(level => level.Id))
            .ForMember(
                selectListItem => selectListItem.Text,
                options => options.MapFrom(level => level.Position));
        CreateMap<Level, LevelMetadataVM>()
            .ForMember(
                metadata => metadata.OccupiedSpacesCount,
                options => options.MapFrom(level => level.Spaces.LongCount(space =>
                    space.Status == SpaceStatus.Occupied)))
            .ForMember(
                metadata => metadata.UnoccupiedSpacesCount,
                options => options.MapFrom(level => level.Spaces.LongCount(space =>
                space.Status == SpaceStatus.Unoccupied)))
            .ForMember(
                metadata => metadata.OutOfServiceSpacesCount,
                options => options.MapFrom(level => level.Spaces.LongCount(space =>
                space.Status == SpaceStatus.OutOfService)))
            .ForMember(
                metadata => metadata.TotalSpacesCount,
                options => options.MapFrom(level => level.Spaces.LongCount()));
    }

    public override string ProfileName => nameof(LevelsMappingProfile);
}
