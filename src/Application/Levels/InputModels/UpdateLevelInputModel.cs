﻿namespace ParkingLot.Application.Levels.InputModels;

/// <summary>
/// Represents view model for updating parking level.
/// </summary>
public class UpdateLevelInputModel
{
    /// <summary>
    /// Initializes a new instance of the <see cref="UpdateLevelInputModel"/> class.
    /// </summary>
    public UpdateLevelInputModel()
    {

    }

    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    public int? Id { get; set; }

    /// <summary>
    /// Gets or sets the physical position (order in building).
    /// </summary>
    public int? Position { get; set; }
}
