﻿namespace ParkingLot.Application.Levels.InputModels;

/// <summary>
/// Represents view model for deleting parking level.
/// </summary>
public class DeleteLevelInputModel
{
    /// <summary>
    /// Initializes a new instance of the <see cref="DeleteLevelInputModel"/> class.
    /// </summary>
    public DeleteLevelInputModel()
    {

    }

    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    public int? Id { get; set; }
}
