﻿namespace ParkingLot.Application.Levels.InputModels;

/// <summary>
/// Represents view model for creating parking level.
/// </summary>
public class CreateLevelInputModel
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CreateLevelInputModel"/> class.
    /// </summary>
    public CreateLevelInputModel()
    {

    }

    /// <summary>
    /// Gets or sets the physical position (order in building).
    /// </summary>
    public int? Position { get; set; }

    /// <summary>
    /// Gets or sets the amount of spaces current parking level should hold.
    /// </summary>
    public int SpacesAmount { get; set; }
}
