﻿using Fluorite.Strainer.Services.Modules;
using ParkingLot.Domain.Levels;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace ParkingLot.Application.Levels.Modules;

public class LevelsStrainerModule : StrainerModule
{
    public override void Load(IStrainerModuleBuilder builder)
    {
        builder.AddProperty<Level>(l => l.Position)
            .IsFilterable()
            .IsSortable()
            .IsDefaultSort(isDescending: true);
        builder.AddProperty<Level>(l => l.Id)
            .IsFilterable()
            .IsSortable();
        builder.AddProperty<Level>(l => l.Created)
            .IsFilterable()
            .IsSortable();
        builder.AddProperty<Level>(l => l.Updated)
            .IsFilterable()
            .IsSortable();

        builder.AddCustomSortMethod<Level>(b => b
            .HasName(nameof(Level.Spaces))
            .HasFunction(SpacesCount())
            .Build());
    }

    private Expression<Func<Level, object>> SpacesCount()
    {
        return level => level.Spaces.Count();
    }
}
