﻿using ParkingLot.Application.Levels.ViewModels;
using ParkingLot.Application.Strainer.Mapping;
using ParkingLot.Domain.Levels;

namespace ParkingLot.Application.Levels.Modules;

public class LevelsStrainerTypeMapperModule : StrainerTypeMapperModule<Level>
{
    public LevelsStrainerTypeMapperModule()
    {
        CreateMap<LevelVM>();
        CreateMap<LevelListItemVM>();
    }
}
