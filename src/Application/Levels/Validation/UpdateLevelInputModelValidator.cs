﻿using FluentValidation;
using ParkingLot.Application.Levels.InputModels;

namespace ParkingLot.Application.Levels.Validation;

/// <summary>
/// Provides validation rules for <see cref="UpdateLevelInputModel"/> class.
/// </summary>
public class UpdateLevelInputModelValidator : AbstractValidator<UpdateLevelInputModel>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="UpdateLevelInputModelValidator"/>
    /// class.
    /// </summary>
    public UpdateLevelInputModelValidator()
    {
        RuleFor(l => l.Id)
            .NotNull();
        RuleFor(l => l.Position)
            .NotNull();
    }
}
