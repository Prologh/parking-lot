﻿using FluentValidation;
using ParkingLot.Application.Levels.InputModels;

namespace ParkingLot.Application.Levels.Validation;

/// <summary>
/// Provides validation rules for <see cref="CreateLevelInputModel"/> class.
/// </summary>
public class CreateLevelInputModelValidator : AbstractValidator<CreateLevelInputModel>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CreateLevelInputModelValidator"/>
    /// class.
    /// </summary>
    public CreateLevelInputModelValidator()
    {
        RuleFor(m => m.Position)
            .NotNull();
        RuleFor(m => m.SpacesAmount)
            .InclusiveBetween(0, 1000);
    }
}
