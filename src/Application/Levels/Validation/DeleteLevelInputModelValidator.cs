﻿using FluentValidation;
using ParkingLot.Application.Levels.InputModels;

namespace ParkingLot.Application.Levels.Validation;

/// <summary>
/// Provides validation rules for <see cref="DeleteLevelInputModel"/> class.
/// </summary>
public class DeleteLevelInputModelValidator : AbstractValidator<DeleteLevelInputModel>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="DeleteVMValidator"/>
    /// class.
    /// </summary>
    public DeleteLevelInputModelValidator()
    {
        RuleFor(p => p.Id)
            .NotNull();
    }
}
