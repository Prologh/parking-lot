﻿using ParkingLot.Application.Repositories;
using ParkingLot.Domain.Levels;

namespace ParkingLot.Application.Levels.Repositories;

public interface ILevelsRepository : IRepository<Level>
{

}
