﻿using ParkingLot.Application.Spaces.ViewModels;
using ParkingLot.Domain.Spaces;
using System;

namespace ParkingLot.Extensions.Spaces;

/// <summary>
/// Provides extension methods for <see cref="Space"/> entity.
/// </summary>
public static class SpaceVMExtensions
{
    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether current
    /// parking space is occupied.
    /// </summary>
    public static bool IsOccupied(this SpaceListItemVM space)
    {
        if (space is null)
        {
            throw new ArgumentNullException(nameof(space));
        }

        return space.Status.HasFlag(SpaceStatus.Occupied);
    }

    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether current
    /// parking space is out of service.
    /// </summary>
    public static bool IsOutOfService(this SpaceListItemVM space)
    {
        if (space is null)
        {
            throw new ArgumentNullException(nameof(space));
        }

        return space.Status.HasFlag(SpaceStatus.OutOfService);
    }

    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether current
    /// parking space is unoccupied.
    /// </summary>
    public static bool IsUnoccupied(this SpaceListItemVM space)
    {
        if (space is null)
        {
            throw new ArgumentNullException(nameof(space));
        }

        return space.Status.HasFlag(SpaceStatus.Unoccupied);
    }
}
