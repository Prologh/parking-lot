﻿using FluentValidation;
using ParkingLot.Application.Spaces.InputModels;

namespace ParkingLot.Application.Spaces.Validation;

/// <summary>
/// Provides validation rules for <see cref="UpdateSpaceInputModel"/> class.
/// </summary>
public class UpdateSpaceInputModelValidator : AbstractValidator<UpdateSpaceInputModel>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="UpdateSpaceInputModelValidator"/>
    /// class.
    /// </summary>
    public UpdateSpaceInputModelValidator()
    {
        RuleFor(m => m.Id)
            .NotNull();
        RuleFor(m => m.LevelId)
            .NotNull();
        RuleFor(m => m.Number)
            .NotNull()
            .GreaterThan(0);
        RuleFor(m => m.Status)
            .NotNull();
    }
}
