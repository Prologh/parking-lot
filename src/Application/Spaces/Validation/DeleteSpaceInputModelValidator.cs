﻿using FluentValidation;
using ParkingLot.Application.Spaces.InputModels;

namespace ParkingLot.Application.Spaces.Validation;

/// <summary>
/// Provides validation rules for <see cref="DeleteSpaceInputModel"/> class.
/// </summary>
public class DeleteSpaceInputModelValidator : AbstractValidator<DeleteSpaceInputModel>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="DeleteVMValidator"/>
    /// class.
    /// </summary>
    public DeleteSpaceInputModelValidator()
    {
        RuleFor(p => p.Id)
            .NotNull();
    }
}
