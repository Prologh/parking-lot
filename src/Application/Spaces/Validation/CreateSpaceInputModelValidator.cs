﻿using FluentValidation;
using ParkingLot.Application.Spaces.InputModels;

namespace ParkingLot.Application.Spaces.Validation;

/// <summary>
/// Provides validation rules for <see cref="CreateSpaceInputModel"/> class.
/// </summary>
public class CreateSpaceInputModelValidator : AbstractValidator<CreateSpaceInputModel>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CreateSpaceInputModelValidator"/>
    /// class.
    /// </summary>
    public CreateSpaceInputModelValidator()
    {
        RuleFor(m => m.LevelId)
            .NotNull();
        RuleFor(m => m.Number)
            .NotNull()
            .GreaterThan(0);
        RuleFor(m => m.Status)
            .NotNull();
    }
}
