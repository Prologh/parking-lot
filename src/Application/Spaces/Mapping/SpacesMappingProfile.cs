﻿using AutoMapper;
using ParkingLot.Application.Spaces.InputModels;
using ParkingLot.Application.Spaces.ViewModels;
using ParkingLot.Domain.Spaces;

namespace ParkingLot.Application.Spaces.Mapping;

public class SpacesMappingProfile : Profile
{
    public SpacesMappingProfile()
    {
        CreateMap<Space, SpaceVM>()
            .ForMember(
                dest => dest.LevelPosition,
                opts => opts.MapFrom(src => src.Level.Position));
        CreateMap<Space, SpaceListItemVM>()
            .ForMember(
                dest => dest.LevelPosition,
                opts => opts.MapFrom(src => src.Level.Position));
        CreateMap<Space, UpdateSpaceInputModel>();
    }

    public override string ProfileName => nameof(SpacesMappingProfile);
}
