﻿namespace ParkingLot.Application.Spaces.InputModels;

/// <summary>
/// Represents input model for deleting parking space.
/// </summary>
public class DeleteSpaceInputModel
{
    /// <summary>
    /// Initializes a new instance of the <see cref="DeleteSpaceInputModel"/> class.
    /// </summary>
    public DeleteSpaceInputModel()
    {

    }

    /// <summary>
    /// Gets or sets identifier.
    /// </summary>
    public int? Id { get; set; }
}
