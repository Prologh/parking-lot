﻿using ParkingLot.Domain.Spaces;

namespace ParkingLot.Application.Spaces.InputModels;

/// <summary>
/// Represents input model for creating single parking space.
/// </summary>
public class CreateSpaceInputModel
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CreateSpaceInputModel"/> class.
    /// </summary>
    public CreateSpaceInputModel()
    {

    }

    /// <summary>
    /// Gets or sets a value indicating whether current
    /// parking space is designed for handicapped drivers.
    /// </summary>
    public bool IsForHandicapped { get; set; }

    /// <summary>
    /// Gets or sets the identifier of a parking level to which current
    /// parking space belongs.
    /// </summary>
    public int? LevelId { get; set; }

    /// <summary>
    /// Gets or sets space number.
    /// </summary>
    public int? Number { get; set; }

    /// <summary>
    /// Gets or sets the space status.
    /// </summary>
    public SpaceStatus? Status { get; set; }
}
