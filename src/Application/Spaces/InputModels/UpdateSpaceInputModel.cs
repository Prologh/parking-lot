﻿using ParkingLot.Domain.Spaces;

namespace ParkingLot.Application.Spaces.InputModels;

/// <summary>
/// Represents input model for updating parking space.
/// </summary>
public class UpdateSpaceInputModel
{
    /// <summary>
    /// Initializes a new instance of the <see cref="UpdateSpaceInputModel"/> class.
    /// </summary>
    public UpdateSpaceInputModel()
    {

    }

    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    public int? Id { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether current
    /// parking space is designed for handicapped drivers.
    /// </summary>
    public bool IsForHandicapped { get; set; }

    /// <summary>
    /// Gets or sets the identifier of a parking level to which current
    /// parking space belongs.
    /// </summary>
    public int? LevelId { get; set; }

    /// <summary>
    /// Gets or sets space number.
    /// </summary>
    public int? Number { get; set; }

    /// <summary>
    /// Gets or sets the space status.
    /// </summary>
    public SpaceStatus? Status { get; set; }
}
