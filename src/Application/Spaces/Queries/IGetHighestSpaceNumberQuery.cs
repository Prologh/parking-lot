﻿using System.Threading.Tasks;

namespace ParkingLot.Application.Spaces.Queries;

public interface IGetHighestSpaceNumberQuery
{
    Task<int> GetAsync();

    Task<int> GetAsync(int levelId);
}
