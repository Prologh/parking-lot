﻿using System.Threading.Tasks;

namespace ParkingLot.Application.Spaces.Queries;

public interface ICheckSpaceWithNumberExistsQuery
{
    Task<bool> CheckAsync(int spaceNumber);

    Task<bool> CheckAsync(int spaceNumber, int spaceIdToIgnore);
}
