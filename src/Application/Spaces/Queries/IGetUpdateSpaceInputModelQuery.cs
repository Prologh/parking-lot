﻿using ParkingLot.Application.Spaces.InputModels;
using System.Threading.Tasks;

namespace ParkingLot.Application.Spaces.Queries;

public interface IGetUpdateSpaceInputModelQuery
{
    Task<UpdateSpaceInputModel> GetAsync(int id);
}
