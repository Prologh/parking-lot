﻿using ParkingLot.Application.Spaces.ViewModels;
using System.Threading.Tasks;

namespace ParkingLot.Application.Spaces.Queries;

public interface IGetSpaceDetailsQuery
{
    Task<SpaceVM> GetAsync(int id);
}
