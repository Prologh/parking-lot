﻿using System.Threading.Tasks;

namespace ParkingLot.Application.Spaces.Queries;

public interface ICheckSpaceIsCurrentlyOccupiedQuery
{
    Task<bool> CheckAsync(int id);
}
