﻿using ParkingLot.Application.Repositories;
using ParkingLot.Domain.Spaces;

namespace ParkingLot.Application.Spaces.Repositories;

public interface ISpacesRepository : IRepository<Space>
{

}
