﻿using ParkingLot.Application.Spaces.ViewModels;
using ParkingLot.Application.Strainer.Mapping;
using ParkingLot.Domain.Spaces;

namespace ParkingLot.Application.Spaces.Modules;

public class SpacesStrainerTypeMapperModule : StrainerTypeMapperModule<Space>
{
    public SpacesStrainerTypeMapperModule()
    {
        CreateMap<SpaceVM>();
        CreateMap<SpaceListItemVM>();
    }
}
