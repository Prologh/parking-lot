﻿using Fluorite.Strainer.Services.Modules;
using ParkingLot.Domain.Spaces;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace ParkingLot.Application.Spaces.Modules;

public class SpacesStrainerModule : StrainerModule
{
    public override void Load(IStrainerModuleBuilder builder)
    {
        builder.AddProperty<Space>(s => s.Number)
            .IsFilterable()
            .IsSortable()
            .IsDefaultSort();
        builder.AddProperty<Space>(s => s.Id)
            .IsFilterable()
            .IsSortable();
        builder.AddProperty<Space>(s => s.Created)
            .IsFilterable()
            .IsSortable();
        builder.AddProperty<Space>(s => s.Updated)
            .IsFilterable()
            .IsSortable();
        builder.AddProperty<Space>(s => s.IsForHandicapped)
            .IsFilterable()
            .IsSortable();
        builder.AddProperty<Space>(s => s.Level.Position)
            .HasDisplayName("LevelPosition")
            .IsSortable()
            .IsFilterable();
        builder.AddProperty<Space>(s => s.Status)
            .IsFilterable()
            .IsSortable();

        builder.AddCustomSortMethod<Space>(b => b
            .HasName(nameof(IsOccupied))
            .HasFunction(IsOccupied())
            .Build());
        builder.AddCustomSortMethod<Space>(b => b
            .HasName(nameof(IsUnoccupied))
            .HasFunction(IsUnoccupied())
            .Build());
        builder.AddCustomSortMethod<Space>(b => b
            .HasName(nameof(IsOutOfService))
            .HasFunction(IsOutOfService())
            .Build());
    }

    private Expression<Func<Space, object>> IsOccupied()
    {
        return space => space.Status == SpaceStatus.Occupied;
    }

    private Expression<Func<Space, object>> IsUnoccupied()
    {
        return space => space.Status == SpaceStatus.Unoccupied;
    }

    private Expression<Func<Space, object>> IsOutOfService()
    {
        return space => space.Status == SpaceStatus.OutOfService;
    }
}
