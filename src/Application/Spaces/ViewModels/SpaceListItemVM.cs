﻿using ParkingLot.Application.Abstractions;
using ParkingLot.Domain.Spaces;

namespace ParkingLot.Application.Spaces.ViewModels;

/// <summary>
/// Represents single parking space view model.
/// </summary>
public class SpaceListItemVM : BaseViewModel
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SpaceListItemVM"/> class.
    /// </summary>
    public SpaceListItemVM()
    {

    }

    /// <summary>
    /// Gets or sets a value indicating whether current
    /// parking space is designed for handicapped drivers.
    /// </summary>
    public bool IsForHandicapped { get; set; }

    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether current
    /// parking space is occupied.
    /// </summary>
    public bool IsOccupied => Status.HasFlag(SpaceStatus.Occupied);

    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether current
    /// parking space is unoccupied.
    /// </summary>
    public bool IsUnoccupied => Status.HasFlag(SpaceStatus.Unoccupied);

    /// <summary>
    /// Gets a <see cref="bool"/> value indicating whether current
    /// parking space is out of service.
    /// </summary>
    public bool IsOutOfService => Status.HasFlag(SpaceStatus.OutOfService);

    /// <summary>
    /// Gets or sets the identifier of a parking level to which current
    /// parking space belongs.
    /// </summary>
    public int LevelId { get; set; }

    public int LevelPosition { get; set; }

    /// <summary>
    /// Gets or sets space number.
    /// </summary>
    public int Number { get; set; }

    /// <summary>
    /// Gets or sets the space status.
    /// </summary>
    public SpaceStatus Status { get; set; }
}
