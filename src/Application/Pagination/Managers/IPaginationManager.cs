﻿using Fluorite.Strainer.Models;
using ParkingLot.Application.Pagination.Models;
using ParkingLot.Domain.Abstractions;
using System.Threading.Tasks;

namespace ParkingLot.Application.Pagination.Managers;

public interface IPaginationManager<TEntity, TViewModel>
    where TEntity : class, IBaseEntity
    where TViewModel : class
{
    Task<IPagedResult<TViewModel>> GetPagedAsync(IStrainerModel strainerModel);
}