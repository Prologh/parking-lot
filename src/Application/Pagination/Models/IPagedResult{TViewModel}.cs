﻿using System.Collections.Generic;

namespace ParkingLot.Application.Pagination.Models;

/// <summary>
/// Defines a strongly typed paginaton query result.
/// </summary>
/// <typeparam name="TViewModel">
/// The type of view model held within the result.
/// </typeparam>
public interface IPagedResult<TViewModel> : IPagedResult
{
    /// <summary>
    /// Gets the list of result items.
    /// </summary>
    IList<TViewModel> Items { get; }
}
