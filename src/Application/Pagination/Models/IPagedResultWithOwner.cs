﻿namespace ParkingLot.Application.Pagination.Models;

/// <summary>
/// Represents wrapper object over model and associated paged result.
/// </summary>
/// <typeparam name="TOwner">
/// The type of model.
/// </typeparam>
/// <typeparam name="TEntity">
/// The type of paged entity inside the underlying <see cref="IPagedResult{TEntity}"/>.
/// </typeparam>
public interface IPagedResultWithOwner<TOwner, TEntity> : IPagedResult<TEntity>
{
    /// <summary>
    /// Gets the owner model.
    /// </summary>
    TOwner Owner { get; }
}
