﻿namespace ParkingLot.Application.Pagination.Models;

/// <summary>
/// Defines a paged query result.
/// </summary>
public interface IPagedResult
{
    int ItemsCount { get; }

    /// <summary>
    /// Gets the page number.
    /// </summary>
    int PageNumber { get; }

    /// <summary>
    /// Gets the page size.
    /// </summary>
    int PageSize { get; }

    /// <summary>
    /// Gets total amount of items.
    /// </summary>
    long TotalItems { get; }

    /// <summary>
    /// Gets total amount of pages.
    /// </summary>
    long TotalPages { get; }
}
