﻿namespace ParkingLot.Application.Pagination.Models;

public interface IPagedQuery
{
    /// <summary>
    /// Gets or sets the page number.
    /// </summary>
    int? Page { get; set; }

    /// <summary>
    /// Gets or sets the size of page.
    /// </summary>
    int? Size { get; set; }
}
