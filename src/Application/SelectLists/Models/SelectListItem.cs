﻿namespace ParkingLot.Application.SelectLists.Models;

public class SelectListItem
{
    public string Text { get; set; }

    public string Value { get; set; }
}
