﻿using ParkingLot.Application.SelectLists.Models;
using ParkingLot.Domain.Abstractions;

namespace ParkingLot.Application.SelectLists.Queries;

public interface ISelectListQuery<TEntity>
    : ISelectListQuery<TEntity, SelectListItem>
    where TEntity : BaseEntity
{

}
