﻿using ParkingLot.Application.SelectLists.Models;
using ParkingLot.Domain.Abstractions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ParkingLot.Application.SelectLists.Queries;

public interface ISelectListQuery<TEntity, TSelectListItem>
    where TEntity : BaseEntity
    where TSelectListItem : SelectListItem
{
    Task<IList<TSelectListItem>> GetSelectListItemsAsync();
}
