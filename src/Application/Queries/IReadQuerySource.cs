﻿using ParkingLot.Domain.Abstractions;
using System.Linq;

namespace ParkingLot.Application.Queries;

public interface IReadQuerySource<TEntity>
    where TEntity : class, IBaseEntity
{
    IQueryable<TEntity> GetQuery();
}
