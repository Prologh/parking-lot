﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ParkingLot.Application.Expressions;

public class PropertyInfoExtractor : IPropertyInfoExtractor
{
    public IList<PropertyInfo> GetAllPublicSettableInstanceProperties<T>()
    {
        return typeof(T)
            .GetProperties(BindingFlags.Instance | BindingFlags.Public)
            .Where(x => x.CanWrite)
            .Where(x => x.PropertyType == typeof(string) || x.PropertyType.IsPrimitive || x.PropertyType.IsValueType)
            .ToList();
    }
}
