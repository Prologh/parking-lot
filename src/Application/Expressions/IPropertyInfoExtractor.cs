﻿using System.Collections.Generic;
using System.Reflection;

namespace ParkingLot.Application.Expressions;

public interface IPropertyInfoExtractor
{
    IList<PropertyInfo> GetAllPublicSettableInstanceProperties<T>();
}
