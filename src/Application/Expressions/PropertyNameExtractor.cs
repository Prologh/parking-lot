﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace ParkingLot.Application.Expressions;

public class PropertyNameExtractor : IPropertyNameExtractor
{
    public string GetPropertyName<T, TProperty>(Expression<Func<T, TProperty>> selector)
    {
        if (selector == null)
        {
            throw new ArgumentNullException(nameof(selector));
        }

        if (selector.Body is not MemberExpression body)
        {
            var ubody = selector.Body as UnaryExpression;
            body = ubody?.Operand as MemberExpression;
        }

        if (body?.Member is not PropertyInfo propertyInfo)
        {
            throw new ArgumentException(
                $"Expression for '{nameof(T)}' {selector} " +
                $"is not a valid expression leading to a readable property.");
        }

        var stack = new Stack<string>();

        while (body != null)
        {
            stack.Push(body.Member.Name);
            body = body.Expression as MemberExpression;
        }

        var fullName = string.Join(".", stack.ToArray());

        return fullName;
    }
}
