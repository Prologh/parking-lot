﻿using System;
using System.Linq.Expressions;

namespace ParkingLot.Application.Expressions;

public interface IPropertyNameExtractor
{
    string GetPropertyName<T, TProperty>(Expression<Func<T, TProperty>> selector);
}
