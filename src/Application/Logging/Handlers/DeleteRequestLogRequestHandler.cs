﻿using CSharpFunctionalExtensions;
using MediatR;
using ParkingLot.Application.Errors;
using ParkingLot.Application.Logging.Requests;
using ParkingLot.Application.Notifications.Models;
using ParkingLot.Application.Notifications.Storages.Abstractions;
using ParkingLot.Application.Repositories;
using ParkingLot.Domain.Logging;
using ParkingLot.Extensions.Notifications;
using System.Threading;
using System.Threading.Tasks;

namespace ParkingLot.Application.Logging.Handlers;

public class DeleteRequestLogRequestHandler : IRequestHandler<DeleteRequestLogRequest, Result>
{
    private readonly IRepository<RequestLog> _requestLogRepository;
    private readonly INotificationStorage _notificationStorage;
    private readonly IUnitOfWork _unitOfWork;

    public DeleteRequestLogRequestHandler(
        IUnitOfWork unitOfWork,
        IRepository<RequestLog> requestLogRepository,
        INotificationStorage notificationStorage)
    {
        _unitOfWork = unitOfWork;
        _requestLogRepository = requestLogRepository;
        _notificationStorage = notificationStorage;
    }

    public async Task<Result> Handle(DeleteRequestLogRequest request, CancellationToken cancellationToken)
    {
        var requestLog = await _requestLogRepository.FindAsync(request.Id);
        if (requestLog is null)
        {
            return Result.Failure(nameof(ErrorType.NotFound));
        }

        _requestLogRepository.Remove(requestLog);

        await _unitOfWork.SaveChangesAsync();

        _notificationStorage.Builder()
            .Icon("fas fa-check")
            .Message("Request log removed.")
            .Title("Success!")
            .Type(NotificationType.Success)
            .SetNotification();

        return Result.Success();
    }
}
