﻿using CSharpFunctionalExtensions;
using MediatR;
using ParkingLot.Application.Errors;
using ParkingLot.Application.Logging.Queries;
using ParkingLot.Application.Logging.Requests;
using ParkingLot.Application.Logging.ViewModels;
using System.Threading;
using System.Threading.Tasks;

namespace ParkingLot.Application.Logging.Handlers;

public class GetRequestLogDetailsRequestHandler : IRequestHandler<GetRequestLogDetailsRequest, Result<RequestLogVM>>
{
    private readonly IGetRequestLogDetailsQuery _getRequestLogDetailsQuery;

    public GetRequestLogDetailsRequestHandler(
        IGetRequestLogDetailsQuery getRequestLogDetailsQuery)
    {
        _getRequestLogDetailsQuery = getRequestLogDetailsQuery;
    }

    public async Task<Result<RequestLogVM>> Handle(GetRequestLogDetailsRequest request, CancellationToken cancellationToken)
    {
        var viewModel = await _getRequestLogDetailsQuery.GetAsync(request.Id);
        if (viewModel is null)
        {
            return Result.Failure<RequestLogVM>(nameof(ErrorType.NotFound));
        }

        return Result.Success(viewModel);
    }
}
