﻿using MediatR;
using ParkingLot.Application.Logging.Queries;
using ParkingLot.Application.Logging.Requests;
using System.Threading;
using System.Threading.Tasks;

namespace ParkingLot.Application.Logging.Handlers;

public class GetRequstLogsCountRequestHandler : IRequestHandler<GetRequestLogsCountRequest, long>
{
    private readonly ICountRequestLogsQuery _countRequestLogsQuery;

    public GetRequstLogsCountRequestHandler(
        ICountRequestLogsQuery countRequestLogsQuery)
    {
        _countRequestLogsQuery = countRequestLogsQuery;
    }

    public Task<long> Handle(GetRequestLogsCountRequest request, CancellationToken cancellationToken)
    {
        return _countRequestLogsQuery.CountAsync();
    }
}
