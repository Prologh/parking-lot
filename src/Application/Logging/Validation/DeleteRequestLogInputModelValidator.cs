﻿using FluentValidation;
using ParkingLot.Application.Logging.InputModels;

namespace ParkingLot.Application.Logging.Validation;

/// <summary>
/// Provides validation rules for <see cref="DeleteRequestLogInputModel"/> class.
/// </summary>
public class DeleteRequestLogInputModelValidator : AbstractValidator<DeleteRequestLogInputModel>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="DeleteVMValidator"/>
    /// class.
    /// </summary>
    public DeleteRequestLogInputModelValidator()
    {
        RuleFor(p => p.Id)
            .NotNull();
    }
}
