﻿namespace ParkingLot.Application.Logging.InputModels;

/// <summary>
/// Represents view model for deleting request log.
/// </summary>
public class DeleteRequestLogInputModel
{
    /// <summary>
    /// Initializes a new instance of the <see cref="DeleteRequestLogInputModel"/>
    /// class.
    /// </summary>
    public DeleteRequestLogInputModel()
    {

    }

    /// <summary>
    /// Gets or sets identifer.
    /// </summary>
    public int? Id { get; set; }
}
