﻿using System.Threading.Tasks;

namespace ParkingLot.Application.Logging.Queries;

public interface IRemoveOldRequestLogsQuery
{
    Task RemoveAsync(int days);
}
