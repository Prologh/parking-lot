﻿using ParkingLot.Application.Logging.ViewModels;
using System.Threading.Tasks;

namespace ParkingLot.Application.Logging.Queries;

public interface IGetRequestLogsLineChartQuery
{
    Task<LineChartVM> GetAsync();
}
