﻿using System.Threading.Tasks;

namespace ParkingLot.Application.Logging.Queries;

public interface ICountRequestLogsQuery
{
    Task<long> CountAsync();
}
