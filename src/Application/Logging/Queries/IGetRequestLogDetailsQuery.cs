﻿using ParkingLot.Application.Logging.ViewModels;
using System.Threading.Tasks;

namespace ParkingLot.Application.Logging.Queries;

public interface IGetRequestLogDetailsQuery
{
    Task<RequestLogVM> GetAsync(int id);
}
