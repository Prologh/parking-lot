﻿using System.Collections.Generic;

namespace ParkingLot.Application.Logging.ViewModels;

public class LineChartVM
{
    public LineChartVM()
    {
        DataSets = new List<IList<string>>();
        XAxisLabels = new List<string>();
    }

    public IList<IList<string>> DataSets { get; set; }

    public string Title { get; set; }

    public IList<string> XAxisLabels { get; set; }
}
