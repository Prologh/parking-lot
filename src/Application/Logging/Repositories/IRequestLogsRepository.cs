﻿using ParkingLot.Application.Repositories;
using ParkingLot.Domain.Logging;

namespace ParkingLot.Application.Logging.Repositories;

public interface IRequestLogsRepository : IRepository<RequestLog>
{

}
