﻿using AutoMapper;
using ParkingLot.Application.Logging.ViewModels;
using ParkingLot.Domain.Logging;

namespace ParkingLot.Application.Logging.Mapping;

public class LoggingMappingProfile : Profile
{
    public LoggingMappingProfile()
    {
        CreateMap<RequestLog, RequestLogVM>();
    }

    public override string ProfileName => nameof(LoggingMappingProfile);
}
