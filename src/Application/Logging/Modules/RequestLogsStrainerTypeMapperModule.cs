﻿using ParkingLot.Application.Logging.ViewModels;
using ParkingLot.Application.Strainer.Mapping;
using ParkingLot.Domain.Logging;

namespace ParkingLot.Application.Logging.Modules;

public class RequestLogsStrainerTypeMapperModule : StrainerTypeMapperModule<RequestLog>
{
    public RequestLogsStrainerTypeMapperModule()
    {
        CreateMap<RequestLogVM>();
    }
}
