﻿using Fluorite.Strainer.Services.Modules;
using ParkingLot.Domain.Logging;

namespace ParkingLot.Application.Logging.Modules;

public class RequestLogsStrainerModule : StrainerModule
{
    public override void Load(IStrainerModuleBuilder builder)
    {
        builder.AddProperty<RequestLog>(s => s.Created)
            .IsFilterable()
            .IsSortable()
            .IsDefaultSort(isDescending: true);
        builder.AddProperty<RequestLog>(s => s.Id)
            .IsFilterable()
            .IsSortable();
        builder.AddProperty<RequestLog>(s => s.Route)
            .IsFilterable()
            .IsSortable();
        builder.AddProperty<RequestLog>(s => s.HttpMethod)
            .IsFilterable()
            .IsSortable();
        builder.AddProperty<RequestLog>(s => s.HttpStatusCode)
            .IsFilterable()
            .IsSortable();
        builder.AddProperty<RequestLog>(s => s.Duration)
            .IsFilterable()
            .IsSortable();
        builder.AddProperty<RequestLog>(s => s.IPAddress)
            .IsSortable()
            .IsFilterable();
        builder.AddProperty<RequestLog>(s => s.IsHttps)
            .IsFilterable()
            .IsSortable();
        builder.AddProperty<RequestLog>(s => s.Query)
            .IsFilterable();
        builder.AddProperty<RequestLog>(s => s.RequestHeaders)
            .IsFilterable();
        builder.AddProperty<RequestLog>(s => s.ResponseHeaders)
            .IsFilterable();
    }
}
