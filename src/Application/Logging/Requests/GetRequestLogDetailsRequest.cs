﻿using CSharpFunctionalExtensions;
using MediatR;
using ParkingLot.Application.Logging.ViewModels;

namespace ParkingLot.Application.Logging.Requests;

public class GetRequestLogDetailsRequest : IRequest<Result<RequestLogVM>>
{
    public int Id { get; set; }
}
