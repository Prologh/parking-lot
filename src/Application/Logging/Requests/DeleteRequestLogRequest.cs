﻿using CSharpFunctionalExtensions;
using MediatR;

namespace ParkingLot.Application.Logging.Requests;

public class DeleteRequestLogRequest : IRequest<Result>
{
    public int Id { get; set; }
}
