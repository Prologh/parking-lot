﻿using ParkingLot.Domain.Logging;
using System.Collections.Generic;

namespace ParkingLot.Application.Logging.Stores;

public interface IRequestLogStore
{
    void Add(RequestLog requestLog);

    void Clear();

    IReadOnlyCollection<RequestLog> GetAll();
}
