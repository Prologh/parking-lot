﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ParkingLot.Persistance.Migrations
{
    public partial class AddRequestLogTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RequestLog",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTimeOffset>(nullable: false),
                    Duration = table.Column<double>(nullable: false),
                    HttpMethod = table.Column<string>(nullable: false),
                    HttpStatusCode = table.Column<int>(nullable: false),
                    IPAddress = table.Column<string>(nullable: true),
                    IsHttps = table.Column<bool>(nullable: false),
                    Query = table.Column<string>(nullable: true),
                    RequestHeaders = table.Column<string>(nullable: true),
                    ResponseHeaders = table.Column<string>(nullable: true),
                    Route = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequestLog", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RequestLog");
        }
    }
}
