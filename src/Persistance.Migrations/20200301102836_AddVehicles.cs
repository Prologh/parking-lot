﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ParkingLot.Persistance.Migrations
{
    public partial class AddVehicles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Space_Level_LevelId",
                table: "Space");

            migrationBuilder.AddColumn<int>(
                name: "VehicleId",
                table: "Space",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Vehicle",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Created = table.Column<DateTimeOffset>(nullable: false),
                    LicensePlate = table.Column<string>(maxLength: 20, nullable: false),
                    SpaceId = table.Column<int>(nullable: true),
                    Updated = table.Column<DateTimeOffset>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicle", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Space_VehicleId",
                table: "Space",
                column: "VehicleId",
                unique: true,
                filter: "[VehicleId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Space_Level_LevelId",
                table: "Space",
                column: "LevelId",
                principalTable: "Level",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Space_Vehicle_VehicleId",
                table: "Space",
                column: "VehicleId",
                principalTable: "Vehicle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Space_Level_LevelId",
                table: "Space");

            migrationBuilder.DropForeignKey(
                name: "FK_Space_Vehicle_VehicleId",
                table: "Space");

            migrationBuilder.DropTable(
                name: "Vehicle");

            migrationBuilder.DropIndex(
                name: "IX_Space_VehicleId",
                table: "Space");

            migrationBuilder.DropColumn(
                name: "VehicleId",
                table: "Space");

            migrationBuilder.AddForeignKey(
                name: "FK_Space_Level_LevelId",
                table: "Space",
                column: "LevelId",
                principalTable: "Level",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
