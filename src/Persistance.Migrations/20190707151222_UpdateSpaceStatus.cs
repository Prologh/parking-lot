﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ParkingLot.Persistance.Migrations
{
    public partial class UpdateSpaceStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsOccupied",
                table: "Space");

            migrationBuilder.DropColumn(
                name: "IsOutOfService",
                table: "Space");

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Space",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "Space");

            migrationBuilder.AddColumn<bool>(
                name: "IsOccupied",
                table: "Space",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsOutOfService",
                table: "Space",
                nullable: false,
                defaultValue: false);
        }
    }
}
