﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ParkingLot.Persistance.Migrations
{
    public partial class AddOccupationTimeAndStayTimeToHistoryEntries : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "VehicleHistoryEntry");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "SpaceHistoryEntry");

            migrationBuilder.AddColumn<bool>(
                name: "HasExited",
                table: "VehicleHistoryEntry",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "StayTime",
                table: "VehicleHistoryEntry",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsReleased",
                table: "SpaceHistoryEntry",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "OccupationTime",
                table: "SpaceHistoryEntry",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HasExited",
                table: "VehicleHistoryEntry");

            migrationBuilder.DropColumn(
                name: "StayTime",
                table: "VehicleHistoryEntry");

            migrationBuilder.DropColumn(
                name: "IsReleased",
                table: "SpaceHistoryEntry");

            migrationBuilder.DropColumn(
                name: "OccupationTime",
                table: "SpaceHistoryEntry");

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "VehicleHistoryEntry",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "SpaceHistoryEntry",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
