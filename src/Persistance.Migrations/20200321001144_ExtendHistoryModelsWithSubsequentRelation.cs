﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ParkingLot.Persistance.Migrations
{
    public partial class ExtendHistoryModelsWithSubsequentRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SubsequentEntryId",
                table: "VehicleHistoryEntry",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubsequentEntryId",
                table: "SpaceHistoryEntry",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHistoryEntry_SubsequentEntryId",
                table: "VehicleHistoryEntry",
                column: "SubsequentEntryId",
                unique: true,
                filter: "[SubsequentEntryId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_SpaceHistoryEntry_SubsequentEntryId",
                table: "SpaceHistoryEntry",
                column: "SubsequentEntryId",
                unique: true,
                filter: "[SubsequentEntryId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_SpaceHistoryEntry_SpaceHistoryEntry_SubsequentEntryId",
                table: "SpaceHistoryEntry",
                column: "SubsequentEntryId",
                principalTable: "SpaceHistoryEntry",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleHistoryEntry_VehicleHistoryEntry_SubsequentEntryId",
                table: "VehicleHistoryEntry",
                column: "SubsequentEntryId",
                principalTable: "VehicleHistoryEntry",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SpaceHistoryEntry_SpaceHistoryEntry_SubsequentEntryId",
                table: "SpaceHistoryEntry");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleHistoryEntry_VehicleHistoryEntry_SubsequentEntryId",
                table: "VehicleHistoryEntry");

            migrationBuilder.DropIndex(
                name: "IX_VehicleHistoryEntry_SubsequentEntryId",
                table: "VehicleHistoryEntry");

            migrationBuilder.DropIndex(
                name: "IX_SpaceHistoryEntry_SubsequentEntryId",
                table: "SpaceHistoryEntry");

            migrationBuilder.DropColumn(
                name: "SubsequentEntryId",
                table: "VehicleHistoryEntry");

            migrationBuilder.DropColumn(
                name: "SubsequentEntryId",
                table: "SpaceHistoryEntry");
        }
    }
}
