﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ParkingLot.Persistance.Migrations
{
    public partial class FixVehiclesForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Space_Vehicle_VehicleId",
                table: "Space");

            migrationBuilder.DropIndex(
                name: "IX_Space_VehicleId",
                table: "Space");

            migrationBuilder.DropColumn(
                name: "VehicleId",
                table: "Space");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_SpaceId",
                table: "Vehicle",
                column: "SpaceId",
                unique: true,
                filter: "[SpaceId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_Space_SpaceId",
                table: "Vehicle",
                column: "SpaceId",
                principalTable: "Space",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehicle_Space_SpaceId",
                table: "Vehicle");

            migrationBuilder.DropIndex(
                name: "IX_Vehicle_SpaceId",
                table: "Vehicle");

            migrationBuilder.AddColumn<int>(
                name: "VehicleId",
                table: "Space",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Space_VehicleId",
                table: "Space",
                column: "VehicleId",
                unique: true,
                filter: "[VehicleId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Space_Vehicle_VehicleId",
                table: "Space",
                column: "VehicleId",
                principalTable: "Vehicle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
