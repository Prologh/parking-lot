﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace ParkingLot.Persistance.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20190605171912_AddCreatedUpdatedColumns")]
    partial class AddCreatedUpdatedColumns
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ParkingLot.Models.Level", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("Created");

                    b.Property<int>("Position");

                    b.Property<DateTime>("Updated");

                    b.HasKey("Id");

                    b.ToTable("Level");
                });

            modelBuilder.Entity("ParkingLot.Models.Space", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("Created");

                    b.Property<bool>("IsOccupied");

                    b.Property<bool>("IsOutOfService");

                    b.Property<int>("LevelId");

                    b.Property<DateTime>("Updated");

                    b.HasKey("Id");

                    b.HasIndex("LevelId");

                    b.ToTable("Space");
                });

            modelBuilder.Entity("ParkingLot.Models.Space", b =>
                {
                    b.HasOne("ParkingLot.Models.Level", "Level")
                        .WithMany("Spaces")
                        .HasForeignKey("LevelId")
                        .OnDelete(DeleteBehavior.Restrict);
                });
#pragma warning restore 612, 618
        }
    }
}
