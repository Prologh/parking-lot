﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ParkingLot.Persistance.Migrations
{
    public partial class RemoveSubsequentHistoryEntries : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SpaceHistoryEntry_SpaceHistoryEntry_SubsequentEntryId",
                table: "SpaceHistoryEntry");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleHistoryEntry_VehicleHistoryEntry_SubsequentEntryId",
                table: "VehicleHistoryEntry");

            migrationBuilder.DropIndex(
                name: "IX_VehicleHistoryEntry_SubsequentEntryId",
                table: "VehicleHistoryEntry");

            migrationBuilder.DropIndex(
                name: "IX_SpaceHistoryEntry_SubsequentEntryId",
                table: "SpaceHistoryEntry");

            migrationBuilder.DropColumn(
                name: "Created",
                table: "VehicleHistoryEntry");

            migrationBuilder.DropColumn(
                name: "EntryType",
                table: "VehicleHistoryEntry");

            migrationBuilder.DropColumn(
                name: "SubsequentEntryId",
                table: "VehicleHistoryEntry");

            migrationBuilder.DropColumn(
                name: "Created",
                table: "SpaceHistoryEntry");

            migrationBuilder.DropColumn(
                name: "EntryType",
                table: "SpaceHistoryEntry");

            migrationBuilder.DropColumn(
                name: "SubsequentEntryId",
                table: "SpaceHistoryEntry");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "EnteredAt",
                table: "VehicleHistoryEntry",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "ExitedAt",
                table: "VehicleHistoryEntry",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "VehicleHistoryEntry",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "ReleasedAt",
                table: "SpaceHistoryEntry",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "SpaceHistoryEntry",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "TakenAt",
                table: "SpaceHistoryEntry",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EnteredAt",
                table: "VehicleHistoryEntry");

            migrationBuilder.DropColumn(
                name: "ExitedAt",
                table: "VehicleHistoryEntry");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "VehicleHistoryEntry");

            migrationBuilder.DropColumn(
                name: "ReleasedAt",
                table: "SpaceHistoryEntry");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "SpaceHistoryEntry");

            migrationBuilder.DropColumn(
                name: "TakenAt",
                table: "SpaceHistoryEntry");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Created",
                table: "VehicleHistoryEntry",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.AddColumn<int>(
                name: "EntryType",
                table: "VehicleHistoryEntry",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SubsequentEntryId",
                table: "VehicleHistoryEntry",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Created",
                table: "SpaceHistoryEntry",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.AddColumn<int>(
                name: "EntryType",
                table: "SpaceHistoryEntry",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SubsequentEntryId",
                table: "SpaceHistoryEntry",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHistoryEntry_SubsequentEntryId",
                table: "VehicleHistoryEntry",
                column: "SubsequentEntryId",
                unique: true,
                filter: "[SubsequentEntryId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_SpaceHistoryEntry_SubsequentEntryId",
                table: "SpaceHistoryEntry",
                column: "SubsequentEntryId",
                unique: true,
                filter: "[SubsequentEntryId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_SpaceHistoryEntry_SpaceHistoryEntry_SubsequentEntryId",
                table: "SpaceHistoryEntry",
                column: "SubsequentEntryId",
                principalTable: "SpaceHistoryEntry",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleHistoryEntry_VehicleHistoryEntry_SubsequentEntryId",
                table: "VehicleHistoryEntry",
                column: "SubsequentEntryId",
                principalTable: "VehicleHistoryEntry",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
