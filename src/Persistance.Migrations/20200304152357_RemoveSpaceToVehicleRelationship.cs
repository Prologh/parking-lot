﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ParkingLot.Persistance.Migrations
{
    public partial class RemoveSpaceToVehicleRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehicle_Space_SpaceId",
                table: "Vehicle");

            migrationBuilder.DropIndex(
                name: "IX_Vehicle_SpaceId",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "SpaceId",
                table: "Vehicle");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SpaceId",
                table: "Vehicle",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_SpaceId",
                table: "Vehicle",
                column: "SpaceId",
                unique: true,
                filter: "[SpaceId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_Space_SpaceId",
                table: "Vehicle",
                column: "SpaceId",
                principalTable: "Space",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
