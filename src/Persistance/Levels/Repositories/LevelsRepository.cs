﻿using ParkingLot.Application.Levels.Repositories;
using ParkingLot.Domain.Levels;
using ParkingLot.Persistance.Repositories;

namespace ParkingLot.Persistance.Levels.Repositories;

public class LevelsRepository : GenericRepository<Level>, ILevelsRepository
{
    public LevelsRepository(ApplicationDbContext context) : base(context)
    {

    }
}
