﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.SelectLists.Models;
using ParkingLot.Application.SelectLists.Queries;
using ParkingLot.Domain.Levels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Levels.Queries;

public class LevelsSelectListQuery : ISelectListQuery<Level, SelectListItem>
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public LevelsSelectListQuery(
        ApplicationDbContext dbContext,
        IMapper mapper)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    public async Task<IList<SelectListItem>> GetSelectListItemsAsync()
    {
        return await _dbContext
            .Levels
            .ProjectTo<SelectListItem>(_mapper.ConfigurationProvider)
            .ToListAsync();
    }
}
