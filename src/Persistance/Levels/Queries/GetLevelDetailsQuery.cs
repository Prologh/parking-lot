﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.Levels.Queries;
using ParkingLot.Application.Levels.ViewModels;
using System;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Levels.Queries;

public class GetLevelDetailsQuery : IGetLevelDetailsQuery
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public GetLevelDetailsQuery(
        ApplicationDbContext dbContext,
        IMapper mapper)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    public Task<LevelVM> GetAsync(int id)
    {
        return _dbContext
            .Levels
            .ProjectTo<LevelVM>(_mapper.ConfigurationProvider)
            .FirstOrDefaultAsync(l => l.Id == id);
    }
}
