﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.Levels.Queries;
using ParkingLot.Application.Levels.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Levels.Queries;

public class GetLevelMetadatasQuery : IGetLevelMetadatasQuery
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public GetLevelMetadatasQuery(
        ApplicationDbContext applicationDbContext,
        IMapper mapper)
    {
        _dbContext = applicationDbContext ?? throw new ArgumentNullException(nameof(applicationDbContext));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    public async Task<IList<LevelMetadataVM>> GetAsync()
    {
        return await _dbContext
            .Levels
            .Include(level => level.Spaces)
            .ProjectTo<LevelMetadataVM>(_mapper.ConfigurationProvider)
            .ToListAsync();
    }
}
