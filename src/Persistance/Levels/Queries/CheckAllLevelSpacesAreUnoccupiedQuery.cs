﻿using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.Levels.Queries;
using ParkingLot.Domain.Levels;
using ParkingLot.Domain.Spaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Levels.Queries;

public class CheckAllLevelSpacesAreUnoccupiedQuery : ICheckAllLevelSpacesAreUnoccupiedQuery
{
    private readonly ApplicationDbContext _dbContext;

    public CheckAllLevelSpacesAreUnoccupiedQuery(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
    }

    public Task<bool> CheckAsync(int id)
    {
        return _dbContext
            .Levels
            .Where(l => l.Id == id)
            .AllAsync(l => l.Spaces.All(s => s.Status != SpaceStatus.Occupied));
    }
}
