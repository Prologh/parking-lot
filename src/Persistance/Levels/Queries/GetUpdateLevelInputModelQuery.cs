﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.Levels.InputModels;
using ParkingLot.Application.Levels.Queries;
using System;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Levels.Queries;

public class GetUpdateLevelInputModelQuery : IGetUpdateLevelInputModelQuery
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public GetUpdateLevelInputModelQuery(
        ApplicationDbContext dbContext,
        IMapper mapper)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    public Task<UpdateLevelInputModel> GetAsync(int id)
    {
        return _dbContext
            .Levels
            .ProjectTo<UpdateLevelInputModel>(_mapper.ConfigurationProvider)
            .FirstOrDefaultAsync(l => l.Id == id);
    }
}
