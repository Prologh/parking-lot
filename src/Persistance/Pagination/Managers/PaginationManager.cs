﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Fluorite.Extensions;
using Fluorite.Strainer.Models;
using Fluorite.Strainer.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ParkingLot.Application.Pagination.Managers;
using ParkingLot.Application.Pagination.Models;
using ParkingLot.Application.Queries;
using ParkingLot.Domain.Abstractions;
using ParkingLot.Persistance.Pagination.Models;
using System;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Pagination.Managers;

public class PaginationManager<TEntity, TViewModel> : IPaginationManager<TEntity, TViewModel>
    where TEntity : class, IBaseEntity
    where TViewModel : class
{
    private readonly IMapper _mapper;
    private readonly IReadQuerySource<TEntity> _readQuerySource;
    private readonly IStrainerProcessor _strainerProcessor;
    private readonly StrainerOptions _strainerOptions;

    public PaginationManager(
        IOptions<StrainerOptions> strainerOptions,
        IMapper mapper,
        IReadQuerySource<TEntity> readQuerySource,
        IStrainerProcessor strainerProcessor)
    {
        _strainerOptions = strainerOptions.Value;
        _mapper = mapper;
        _readQuerySource = readQuerySource;
        _strainerProcessor = strainerProcessor;
    }

    public async Task<IPagedResult<TViewModel>> GetPagedAsync(IStrainerModel strainerModel)
    {
        if (strainerModel == null)
        {
            throw new ArgumentNullException(nameof(strainerModel));
        }

        var pageNumber = strainerModel.Page ?? _strainerOptions.DefaultPageNumber;
        var pageSize = strainerModel.PageSize ?? _strainerOptions.DefaultPageSize;

        var query = _readQuerySource
            .GetQuery()
            .ApplyFiltering(strainerModel, _strainerProcessor);

        var totalCount = await query.LongCountAsync();
        var items = await query
            .ApplySorting(strainerModel, _strainerProcessor)
            .ApplyPagination(strainerModel, _strainerProcessor)
            .ProjectTo<TViewModel>(_mapper.ConfigurationProvider)
            .ToListAsync();

        return new PagedResult<TViewModel>
        {
            Items = items,
            PageNumber = pageNumber,
            PageSize = pageSize,
            TotalItems = totalCount,
            TotalPages = (long)Math.Ceiling(totalCount * 1d / pageSize),
        };
    }
}
