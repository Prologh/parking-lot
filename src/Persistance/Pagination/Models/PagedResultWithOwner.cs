﻿using ParkingLot.Application.Pagination.Models;
using System;
using System.Collections.Generic;

namespace ParkingLot.Persistance.Pagination.Models;

/// <summary>
/// Represents wrapper object over owner and associated paged result.
/// </summary>
/// <typeparam name="TOwner">
/// The type of owner.
/// </typeparam>
/// <typeparam name="TEntity">
/// The type of paged entity inside the underlying <see cref="IPagedResult{TEntity}"/>.
/// </typeparam>
public class PagedResultWithOwner<TOwner, TEntity>
    : PagedResult<TEntity>,
    IPagedResultWithOwner<TOwner, TEntity>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PagedResultWithOwner{TModel, TPagedEntity}"/>
    /// class.
    /// </summary>
    public PagedResultWithOwner()
    {

    }

    /// <summary>
    /// Initializes a new instance of the <see cref="PagedResultWithOwner{TModel, TPagedEntity}"/>
    /// class based on existing collection.
    /// </summary>
    /// <param name="entities">
    /// The exsisting collection.
    /// </param>
    public PagedResultWithOwner(IEnumerable<TEntity> entities) : base(entities)
    {

    }

    /// <summary>
    /// Initializes a new instance of the <see cref="PagedResultWithOwner{TModel, TPagedEntity}"/>
    /// class based on existing list.
    /// </summary>
    /// <param name="list">
    /// The exsisting collection.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="list"/> is <see langword="null"/>.
    /// </exception>
    public PagedResultWithOwner(IList<TEntity> list) : base(list)
    {

    }

    /// <summary>
    /// Initializes a new instance of the <see cref="PagedResultWithOwner{TModel, TPagedEntity}"/>
    /// class based on existing paged result.
    /// </summary>
    /// <param name="result">
    /// The exsisting paged result.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="result"/> is <see langword="null"/>.
    /// </exception>
    public PagedResultWithOwner(IPagedResult<TEntity> result) : base(result.Items)
    {
        PageNumber = result.PageNumber;
        PageSize = result.PageSize;
        TotalItems = result.TotalItems;
        TotalPages = result.TotalPages;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="PagedResultWithOwner{TModel, TPagedEntity}"/>
    /// class with owner.
    /// </summary>
    /// <param name="owner">
    /// The owner.
    /// </param>
    public PagedResultWithOwner(TOwner owner)
    {
        Owner = owner;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="PagedResultWithOwner{TModel, TPagedEntity}"/>
    /// class based on existing collection with owner.
    /// </summary>
    /// <param name="entities">
    /// The exsisting collection.
    /// </param>
    /// <param name="owner">
    /// The owner.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="entities"/> is <see langword="null"/>.
    /// </exception>
    public PagedResultWithOwner(TOwner owner, IEnumerable<TEntity> entities) : base(entities)
    {
        if (entities == null)
        {
            throw new ArgumentNullException(nameof(entities));
        }

        Owner = owner;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="PagedResultWithOwner{TModel, TPagedEntity}"/>
    /// class based on existing list with owner.
    /// </summary>
    /// <param name="list">
    /// The exsisting collection.
    /// </param>
    /// <param name="owner">
    /// The owner.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="list"/> is <see langword="null"/>.
    /// </exception>
    public PagedResultWithOwner(TOwner owner, IList<TEntity> list) : base(list)
    {
        if (list == null)
        {
            throw new ArgumentNullException(nameof(list));
        }

        Owner = owner;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="PagedResultWithOwner{TModel, TPagedEntity}"/>
    /// class based on existing result with owner.
    /// </summary>
    /// <param name="owner">
    /// The owner.
    /// </param>
    /// <param name="result">
    /// The exsisting paged result.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="result"/> is <see langword="null"/>.
    /// </exception>
    public PagedResultWithOwner(TOwner owner, IPagedResult<TEntity> result) : this(result)
    {
        if (result == null)
        {
            throw new ArgumentNullException(nameof(result));
        }

        Owner = owner;
    }

    /// <summary>
    /// Gets or sets the owner.
    /// </summary>
    public TOwner Owner { get; set; }
}
