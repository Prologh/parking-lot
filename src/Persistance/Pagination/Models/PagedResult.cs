﻿using ParkingLot.Application.Pagination.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ParkingLot.Persistance.Pagination.Models;

/// <summary>
/// Represents result for a query to a paged resource, that is a single
/// fragment of the resource.
/// </summary>
/// <typeparam name="TEntity">
/// The type of entity held within the result.
/// </typeparam>
public class PagedResult<TEntity> : IPagedResult<TEntity>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PagedResult{TEntity}"/>
    /// class.
    /// </summary>
    public PagedResult()
    {
        Items = new List<TEntity>();
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="PagedResult{TEntity}"/>
    /// class based on existing collection.
    /// </summary>
    /// <param name="entities">
    /// The exsisting collection.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="entities"/> is <see langword="null"/>.
    /// </exception>
    public PagedResult(IEnumerable<TEntity> entities)
    {
        if (entities == null)
        {
            throw new ArgumentNullException(nameof(entities));
        }

        Items = entities.ToList();
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="PagedResult{TEntity}"/>
    /// class based on existing collection.
    /// </summary>
    /// <param name="list">
    /// The exsisting collection.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="list"/> is <see langword="null"/>.
    /// </exception>
    public PagedResult(IList<TEntity> list)
    {
        Items = list ?? throw new ArgumentNullException(nameof(list));
    }

    /// <summary>
    /// Gets or sets the list of result items.
    /// </summary>
    public IList<TEntity> Items { get; set; }

    public int ItemsCount => Items.Count;

    /// <summary>
    /// Gets or sets the page number.
    /// </summary>
    public int PageNumber { get; set; }

    /// <summary>
    /// Gets or sets the page size.
    /// </summary>
    public int PageSize { get; set; }

    /// <summary>
    /// Gets or sets total amount of items.
    /// </summary>
    public long TotalItems { get; set; }

    /// <summary>
    /// Gets or sets total amount of pages.
    /// </summary>
    public long TotalPages { get; set; }
}
