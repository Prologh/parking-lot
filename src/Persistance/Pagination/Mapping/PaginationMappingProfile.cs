﻿using AutoMapper;
using ParkingLot.Persistance.Pagination.Models;

namespace ParkingLot.Persistance.Pagination.Mapping;

public class PaginationMappingProfile : Profile
{
    public PaginationMappingProfile()
    {
        CreateMap(typeof(PagedResult<>), typeof(PagedResult<>));
    }

    public override string ProfileName => nameof(PaginationMappingProfile);
}
