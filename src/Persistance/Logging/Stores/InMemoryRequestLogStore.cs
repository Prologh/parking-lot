﻿using ParkingLot.Application.Collections.Concurrent;
using ParkingLot.Application.Logging.Stores;
using ParkingLot.Domain.Logging;
using System;
using System.Collections.Generic;

namespace ParkingLot.Persistance.Logging.Stores;

public class InMemoryRequestLogStore : IRequestLogStore
{
    private readonly ConcurrentHashSet<RequestLog> _requestLogs;

    public InMemoryRequestLogStore()
    {
        _requestLogs = new ConcurrentHashSet<RequestLog>();
    }

    public void Add(RequestLog requestLog)
    {
        if (requestLog is null)
        {
            throw new ArgumentNullException(nameof(requestLog));
        }

        _requestLogs.Add(requestLog);
    }

    public void Clear() => _requestLogs.Clear();

    public IReadOnlyCollection<RequestLog> GetAll() => _requestLogs;
}
