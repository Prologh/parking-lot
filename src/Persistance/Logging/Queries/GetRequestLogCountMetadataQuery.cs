﻿using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.Logging.Queries;
using System;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Logging.Queries;

public class GetRequestLogCountMetadataQuery : ICountRequestLogsQuery
{
    private readonly ApplicationDbContext _dbContext;

    public GetRequestLogCountMetadataQuery(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
    }

    public Task<long> CountAsync()
    {
        return _dbContext.RequestLogs.LongCountAsync();
    }
}
