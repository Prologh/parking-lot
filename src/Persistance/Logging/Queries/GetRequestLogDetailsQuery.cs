﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.Logging.Queries;
using ParkingLot.Application.Logging.ViewModels;
using System;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Logging.Queries;

public class GetRequestLogDetailsQuery : IGetRequestLogDetailsQuery
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public GetRequestLogDetailsQuery(
        ApplicationDbContext dbContext,
        IMapper mapper)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    public Task<RequestLogVM> GetAsync(int id)
    {
        return _dbContext
            .RequestLogs
            .ProjectTo<RequestLogVM>(_mapper.ConfigurationProvider)
            .FirstOrDefaultAsync(r => r.Id == id);
    }
}
