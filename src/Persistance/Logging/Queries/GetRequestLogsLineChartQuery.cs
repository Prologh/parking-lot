﻿using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.Logging.Queries;
using ParkingLot.Application.Logging.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Logging.Queries;

public class GetRequestLogsLineChartQuery : IGetRequestLogsLineChartQuery
{
    private readonly ApplicationDbContext _dbContext;

    public GetRequestLogsLineChartQuery(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
    }

    public async Task<LineChartVM> GetAsync()
    {
        var list = await _dbContext
            .RequestLogs
            .OrderBy(log => log.Created)
            .Select(log => log.Created)
            .ToListAsync();

        var dictionary = list
            .GroupBy(log => log.Date.DayOfYear)
            .ToDictionary(
                g => g.First().Date.ToString("yyyy-MM-dd"),
                g => g.Count().ToString());

        return new LineChartVM
        {
            DataSets = new List<IList<string>>
            {
                new List<string>(dictionary.Values),
            },
            Title = "Requests per day",
            XAxisLabels = new List<string>(dictionary.Keys),
        };
    }
}
