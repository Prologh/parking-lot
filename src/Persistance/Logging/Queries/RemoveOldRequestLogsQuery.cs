﻿using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.Logging.Queries;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Logging.Queries;

public class RemoveOldRequestLogsQuery : IRemoveOldRequestLogsQuery
{
    private readonly ApplicationDbContext _dbContext;

    public RemoveOldRequestLogsQuery(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
    }

    public async Task RemoveAsync(int days)
    {
        var dateLimit = new TimeSpan(days);
        var dateTreshold = DateTime.Now.Subtract(dateLimit);

        var requestLogsToRemove = await _dbContext
            .RequestLogs
            .Where(r => r.Created < dateTreshold)
            .ToListAsync();

        _dbContext.RemoveRange(requestLogsToRemove);
    }
}
