﻿using ParkingLot.Application.Logging.Repositories;
using ParkingLot.Domain.Logging;
using ParkingLot.Persistance.Repositories;

namespace ParkingLot.Persistance.Logging.Repositories;

public class RequestLogsRepository : GenericRepository<RequestLog>, IRequestLogsRepository
{
    public RequestLogsRepository(ApplicationDbContext context) : base(context)
    {

    }
}
