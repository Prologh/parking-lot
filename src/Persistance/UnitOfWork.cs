﻿using ParkingLot.Application;
using System;
using System.Threading.Tasks;

namespace ParkingLot.Persistance;

public class UnitOfWork : IUnitOfWork
{
    /// <summary>
    /// Initializes a new instance of the <see cref="UnitOfWork"/>
    /// class using database context.
    /// </summary>
    public UnitOfWork(ApplicationDbContext context)
    {
        DbContext = context ?? throw new ArgumentNullException(nameof(context));
    }

    /// <summary>
    /// Gets the database context.
    /// </summary>
    protected ApplicationDbContext DbContext { get; }

    /// <summary>
    /// Synchronously saves changes to the database.
    /// </summary>
    public int SaveChanges() => DbContext.SaveChanges();

    /// <summary>
    /// Asynchronously saves changes to the database.
    /// </summary>
    public Task<int> SaveChangesAsync() => DbContext.SaveChangesAsync();
}
