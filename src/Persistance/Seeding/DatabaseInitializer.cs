﻿using MathNet.Numerics.Distributions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ParkingLot.Domain.Levels;
using ParkingLot.Domain.Logging;
using ParkingLot.Domain.SpaceHistory;
using ParkingLot.Domain.Spaces;
using ParkingLot.Domain.VehicleHistory;
using ParkingLot.Domain.Vehicles;
using ParkingLot.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace ParkingLot.Persistance.Seeding;

/// <summary>
/// Provides means of database initialization.
/// </summary>
public class DatabaseInitializer
{
    private const int LevelsCount = 3;
    private const int SpacesCount = 60;
    private const int MaximumAverageVehicleEntriesPerYear = 12;
    private const int MinimumVehiclesPerYear = 2000;
    private const int MaximumVehiclesPerYear = 4000;
    private const int MinimumVehicleStayInMinutes = 2;
    private const int MaximumVehicleStayInMinutes = 60 * 24;

    private static readonly DateTimeOffset ParkingLotLaunchDate = DateTimeOffset.Now.AddMonths(-6);

    private readonly Random _random;
    private readonly ApplicationDbContext _dbContext;
    private readonly ILogger<DatabaseInitializer> _logger;

    public DatabaseInitializer(
        ApplicationDbContext dbContext,
        ILogger<DatabaseInitializer> logger)
    {
        _random = new Random();
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    }

    /// <summary>
    /// Creates the database if it does not exist and seeds it with
    /// initial data.
    /// </summary>
    public void Initialize(bool migrate = false, bool ensureCreated = false)
    {
        _logger.LogInformation("Database seeding started.");

        if (migrate)
        {
            _dbContext.Database.Migrate();
        }

        if (ensureCreated)
        {
            _dbContext.Database.EnsureCreated();
        }

        if (!_dbContext.Levels.AsNoTracking().Any())
        {
            DeleteAllEntities();

            var levels = GenerateLevels(LevelsCount);
            _dbContext.AddRange(levels);

            var spaces = GenerateSpaces(levels, SpacesCount);
            _dbContext.AddRange(spaces);

            var vehicles = GenerateVehicles();
            _dbContext.AddRange(vehicles);

            var vehicleHistoryEntries = GenerateVehicleHistoryEntires(vehicles);
            _dbContext.AddRange(vehicleHistoryEntries);

            var spaceHistoryEntries = GenerateSpaceHistoryEntires(spaces, vehicleHistoryEntries);
            _dbContext.AddRange(spaceHistoryEntries);

            var requestLogs = GenerateRequestLogs();
            _dbContext.AddRange(requestLogs);

            Validate();

            _logger.LogInformation("Saving changes to database...");
            _dbContext.SaveChanges();
            _logger.LogInformation("Changes saved.");
        }

        _logger.LogInformation("Database seeding finished.");
    }

    private void DeleteAllEntities()
    {
        _logger.LogInformation("Removing all existing entities...");

        var vehicles = _dbContext.Set<Vehicle>().ToList();
        var spaces = _dbContext.Set<Space>().ToList();
        var levels = _dbContext.Set<Level>().ToList();
        var spaceHistoryEntries = _dbContext.Set<SpaceHistoryEntry>().ToList();
        var vehicleHistoryEntries = _dbContext.Set<VehicleHistoryEntry>().ToList();
        var requestLogs = _dbContext.Set<RequestLog>().ToList();

        _dbContext.RemoveRange(vehicles);
        _dbContext.RemoveRange(spaces);
        _dbContext.RemoveRange(levels);
        _dbContext.RemoveRange(spaceHistoryEntries);
        _dbContext.RemoveRange(vehicleHistoryEntries);
        _dbContext.RemoveRange(requestLogs);

        _logger.LogInformation("All existing entities removed.");
    }

    private List<Level> GenerateLevels(int levelsAmount)
    {
        _logger.LogInformation("Generating {0} levels...", levelsAmount);

        var levels = new List<Level>(capacity: levelsAmount);

        for (int position = 0; Math.Abs(position) < levelsAmount; position--)
        {
            var level = new Level(position);
            level.SetCreationDate(ParkingLotLaunchDate);
            level.SetLastUpdateDate(_random.NextDateTimeOffset(level.Created));

            levels.Add(level);
        }

        _logger.LogInformation("Generated {0} levels.", levels.Count);

        return levels;
    }

    private List<Space> GenerateSpaces(IList<Level> levels, int spacesAmount)
    {
        _logger.LogInformation("Generating {0} spaces...", levels.Count * spacesAmount);

        var spaces = new List<Space>(capacity: levels.Count * spacesAmount);

        for (int levelNumber = 0; levelNumber < levels.Count; levelNumber++)
        {
            var level = levels[levelNumber];

            for (int spaceNumber = 0; spaceNumber < spacesAmount; spaceNumber++)
            {
                var space = new Space
                {
                    Level = level,
                    Number = spaceNumber + 1 + (levelNumber * SpacesCount),
                    Status = SpaceStatus.Unoccupied,
                };
                space.SetCreationDate(level.Created);
                space.SetLastUpdateDate(_random.NextDateTimeOffset(level.Created));

                spaces.Add(space);
            }
        }

        _logger.LogInformation("Generated {0} spaces.", spaces.Count);

        return spaces;
    }

    private List<Vehicle> GenerateVehicles()
    {
        var vehiclesPerYear = _random.Next(MinimumVehiclesPerYear, MaximumVehiclesPerYear + 1);
        var vehiclesCount = vehiclesPerYear * (int)(DateTimeOffset.Now - ParkingLotLaunchDate).TotalDays / 365;
        var vehicles = new List<Vehicle>(capacity: vehiclesCount);

        _logger.LogInformation("Generating {0} vehicles...", vehiclesCount);

        for (var i = 0; i < vehiclesCount; i++)
        {
            var vehicle = new Vehicle
            {
                LicensePlate = GenerateRandomLicensePlate(),
                Type = GenerateRandomVehicleType(),
            };
            vehicle.SetCreationDate(_random.NextDateTimeOffset(ParkingLotLaunchDate));
            vehicle.SetLastUpdateDate(_random.NextDateTimeOffset(vehicle.Created));

            vehicles.Add(vehicle);
        }

        _logger.LogInformation("Generated {0} vehicles.", vehicles.Count);

        return vehicles;
    }

    private List<VehicleHistoryEntry> GenerateVehicleHistoryEntires(IList<Vehicle> vehicles)
    {
        _logger.LogInformation("Generating vehicle history entries...");

        var vehicleHistoryEntries = new List<VehicleHistoryEntry>();

        foreach (var vehicle in vehicles)
        {
            var minDate = vehicle.Created;
            int maximumEntriesLimit =
                new int[]
                {
                    (int)((DateTimeOffset.Now - vehicle.Created).TotalDays / 365.0d * MaximumAverageVehicleEntriesPerYear),
                    1,
                }.Max();
            var entriesAmount = _random.Next(1, maximumEntriesLimit);
            var entriesPerVehicle = new List<VehicleHistoryEntry>();

            for (var i = 0; i < entriesAmount; i++)
            {
                // Entry date
                var entryDate = _random.NextDateTimeOffset(
                    minDate,
                    DateTimeOffset.Now);

                // Create
                var vehicleHistoryEntry = new VehicleHistoryEntry(vehicle, entryDate);

                // Exit
                var stayMinutes = new[]
                {
                    Normal.Sample(8.0, 2.0) * 60,
                    MinimumVehicleStayInMinutes,
                }.Max();
                var exitDate = entryDate.AddMinutes(stayMinutes);

                // Ignore the entry if colliding with existing history for the vehicle
                if (entriesPerVehicle.Any(entry =>
                    (entryDate >= entry.EnteredAt && entryDate <= entry.ExitedAt)
                    || (exitDate >= entry.EnteredAt && exitDate <= entry.ExitedAt)))
                {
                    _logger.LogInformation(
                        "{0} skipping vehicle history entry as " +
                        "it is colliding with other entries.",
                        vehicle.LicensePlate);

                    continue;
                }

                // Skip setting exit for the entry if exit date is set for near future
                if (exitDate <= DateTimeOffset.Now)
                {
                    vehicleHistoryEntry.Exit(exitDate);
                }
                else
                {
                    // Skip the whole entry if can't mark as exited
                    // and there is already existing entry marked as only entered
                    if (vehicleHistoryEntries.Any(e => !e.HasExited))
                    {
                        _logger.LogInformation(
                            "{0} skipping vehicle history entry as " +
                            "there is already one with entered status.",
                            vehicle.LicensePlate);

                        continue;
                    }
                }

                // Add new entry to list
                entriesPerVehicle.Add(vehicleHistoryEntry);
            }

            // If no history entires was generated for particular vehicle
            // then remove the vehicle from database, as it wouldn't be
            // created if no history entries with it are in database.
            if (!entriesPerVehicle.Any())
            {
                _dbContext.Vehicles.Remove(vehicle);
            }

            vehicleHistoryEntries.AddRange(entriesPerVehicle);
        }

        _logger.LogInformation("Generated {0} vehicle history entries.", vehicleHistoryEntries.Count);

        return vehicleHistoryEntries.OrderBy(e => e.EnteredAt).ToList();
    }

    private List<SpaceHistoryEntry> GenerateSpaceHistoryEntires(
        IList<Space> spaces,
        IList<VehicleHistoryEntry> vehicleHistoryEntries)
    {
        _logger.LogInformation("Generating space history entries...");

        var spaceHistoryEntries = new List<SpaceHistoryEntry>();
        var vehicleHistoryEntriesGroupedByVehicle = vehicleHistoryEntries
            .GroupBy(e => e.Vehicle.LicensePlate)
            .ToList();

        // Create space history entries for all vehicle history entries
        foreach (var vehicleHistoryGroup in vehicleHistoryEntriesGroupedByVehicle)
        {
            // Create space history entry for every vehicle history entry
            foreach (var vehicleHistoryEntry in vehicleHistoryGroup)
            {
                var randomUnoccupiedSpace = GetRandomUnoccupiedSpace(spaces);

                // Taking date
                var occupationDate = _random.NextDateTimeOffset(
                    vehicleHistoryEntry.EnteredAt,
                    vehicleHistoryEntry.EnteredAt.AddMinutes(15));

                // Skip the space occupation if vehicle just arrived
                if (occupationDate > DateTimeOffset.Now)
                {
                    continue;
                }

                // Create
                var spaceHistoryEntry = new SpaceHistoryEntry(randomUnoccupiedSpace, occupationDate);

                // Skip space release if the generated release date is in near future
                if (vehicleHistoryEntry.HasExited)
                {
                    // Release date
                    var releaseDate = _random.NextDateTimeOffset(
                        spaceHistoryEntry.TakenAt.AddMinutes(MinimumVehicleStayInMinutes),
                        spaceHistoryEntry.TakenAt.AddMinutes(MaximumVehicleStayInMinutes));

                    if (releaseDate <= DateTimeOffset.Now)
                    {
                        spaceHistoryEntry.Release(releaseDate);
                    }
                }

                // Add to list
                spaceHistoryEntries.Add(spaceHistoryEntry);
            }
        }

        _logger.LogInformation("Generated {0} space history entries.", spaceHistoryEntries.Count);

        return spaceHistoryEntries.OrderBy(entry => entry.TakenAt).ToList();
    }

    private List<RequestLog> GenerateRequestLogs()
    {
        return new List<RequestLog>
        {
            new RequestLog
            {
                Duration = 5,
                HttpMethod = HttpMethod.Get.Method,
                HttpStatusCode = (int)HttpStatusCode.OK,
                IPAddress = "127.0.0.1",
                IsHttps = true,
                Query = null,
                RequestHeaders = null,
                ResponseHeaders = null,
                Route = "/",
            },
        };
    }

    private string GenerateRandomLicensePlate()
    {
        var cityprefixes = new[]
        {
            "BI", "BS",
            "CB", "CT",
            "DJ", "DL", "DB", "DW", "DX",
            "EL",
            "FG", "FZ",
            "GD",
            "KK", "KR", "KT",
            "LU",
            "NO", "NE",
            "OP",
            "PO", "PL",
            "RP", "RK",
            "SK",
            "TK",
            "WE", "WX", "WB", "WP", "WR",
            "ZK",
        };

        return string.Concat(
            cityprefixes[_random.Next(cityprefixes.Length)],
            " ",
            _random.Next(9),
            _random.Next(9),
            _random.Next(9),
            _random.Next(9),
            _random.Next(9));
    }

    private Space GetRandomUnoccupiedSpace(IList<Space> spaces)
    {
        var unoccupiedSpaces = spaces
            .Where(s => s.Status == SpaceStatus.Unoccupied)
            .ToList();

        return unoccupiedSpaces[_random.Next(unoccupiedSpaces.Count)];
    }

    private VehicleType GenerateRandomVehicleType()
    {
        return _random.NextDouble() > 0.93
            ? _random.NextDouble() > 0.7 ? VehicleType.Unknown : VehicleType.Motorcycle
            : VehicleType.Car;
    }

    private void Validate()
    {
        var faultyVehicleHistoryEntries = _dbContext
            .VehicleHistory
            .Where(e => !e.HasExited)
            .GroupBy(e => e.Vehicle.LicensePlate)
            .Where(g => g.Count() > 1)
            .OrderByDescending(g => g.Count())
            .Select(g => $"Count = {g.Count()}, License plate = {g.Key}")
            .ToList();

        if (faultyVehicleHistoryEntries.Any())
        {
            throw new Exception(
                $"Vehicle history entires for {faultyVehicleHistoryEntries.Count} " +
                $"vehicle/s have more than 1 entries with {nameof(VehicleHistoryEntry.HasExited)} " +
                $"flag set to false. Vehicle can enter parking lot only once without leaving it. " +
                $"Vehicles: \n{string.Join("\n", faultyVehicleHistoryEntries)}");
        }
    }
}
