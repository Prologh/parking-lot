﻿using ParkingLot.Application.SpaceHistory.Repositories;
using ParkingLot.Domain.SpaceHistory;
using ParkingLot.Persistance.Repositories;

namespace ParkingLot.Persistance.SpaceHistory.Repositories;

public class SpaceHistoryEntriesRepository : GenericRepository<SpaceHistoryEntry>, ISpaceHistoryEntriesRepository
{
    public SpaceHistoryEntriesRepository(ApplicationDbContext context) : base(context)
    {

    }
}
