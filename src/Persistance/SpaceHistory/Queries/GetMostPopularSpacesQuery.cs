﻿using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.Charts.ViewModels;
using ParkingLot.Application.SpaceHistory.Queries;
using ParkingLot.Application.SpaceHistory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.SpaceHistory.Queries;

public class GetMostPopularSpacesQuery : IGetMostPopularSpacesQuery
{
    private const int MaxLimit = 10;

    private readonly ApplicationDbContext _dbContext;

    public GetMostPopularSpacesQuery(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<MostPopularSpacesChartVM> GetAsync(int limit)
    {
        if (limit < 1)
        {
            throw new ArgumentOutOfRangeException(
                nameof(limit),
                limit,
                $"{nameof(limit)} must be at least 1.");
        }

        if (limit > MaxLimit)
        {
            throw new ArgumentOutOfRangeException(
                nameof(limit),
                limit,
                $"{nameof(limit)} cannot have value greater than {MaxLimit}.");
        }

        var mostPopularSpaces = await _dbContext
            .SpaceHistory
            .Include(s => s.Space)
            .GroupBy(s => s.Space.Number)
            .OrderByDescending(g => g.Count())
            .ThenBy(g => g.Key)
            .Select(g => new SpacePopularityInfoVM
            {
                Number = g.Key,
                TimesTaken = g.Count(),
            })
            .Take(limit)
            .ToListAsync();

        return new MostPopularSpacesChartVM
        {
            DataSets = new List<DataSetVM<SpacePopularityInfoVM>>
            {
                new DataSetVM<SpacePopularityInfoVM>
                {
                    BackgroundColor = "rgba(0, 94, 70, 0.5)",
                    BorderColor = "rgba(0, 94, 70, 1)",
                    Data = mostPopularSpaces,
                    Label = "TimesTaken",
                },
            },
            Title = "Most popular spaces",
            XAxisLabels = mostPopularSpaces.Select(s => s.Number.ToString()).ToList(),
        };
    }
}
