﻿using ParkingLot.Application.Queries;
using ParkingLot.Domain.Abstractions;
using System;
using System.Linq;

namespace ParkingLot.Persistance.Queries;

public class GenericReadQuerySource<TEntity> : IReadQuerySource<TEntity>
    where TEntity : class, IBaseEntity
{
    private readonly ApplicationDbContext _dbContext;

    public GenericReadQuerySource(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
    }

    public virtual IQueryable<TEntity> GetQuery() => _dbContext.Set<TEntity>();
}
