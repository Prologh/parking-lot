﻿using System;

namespace ParkingLot.Extensions;

public static class TimeSpanExtensions
{
    public static bool IsNegative(this TimeSpan instance)
    {
        return !instance.Duration().Equals(instance);
    }
}
