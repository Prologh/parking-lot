﻿using System;

namespace ParkingLot.Extensions;

public static class RandomExtensions
{
    public static TimeSpan NextTimeSpan(this Random random)
    {
        if (random is null)
        {
            throw new ArgumentNullException(nameof(random));
        }

        return TimeSpan.FromMilliseconds(random.Next((int)TimeSpan.FromDays(1).TotalMilliseconds));
    }

    public static TimeSpan NextTimeSpan(this Random random, TimeSpan maxValue)
    {
        if (random is null)
        {
            throw new ArgumentNullException(nameof(random));
        }

        return TimeSpan.FromMilliseconds(random.Next((int)maxValue.TotalMilliseconds));
    }

    public static TimeSpan NextTimeSpan(this Random random, TimeSpan minValue, TimeSpan maxValue)
    {
        if (random is null)
        {
            throw new ArgumentNullException(nameof(random));
        }

        if (minValue > maxValue)
        {
            throw new ArgumentOutOfRangeException(
                nameof(maxValue),
                $"{nameof(maxValue)} cannot be lesser than {nameof(minValue)}.");
        }

        return TimeSpan.FromMilliseconds(random.Next(
            (int)minValue.TotalMilliseconds,
            (int)maxValue.TotalMilliseconds));
    }

    public static DateTimeOffset NextDateTimeOffset(this Random random)
    {
        return random.NextDateTimeOffset(DateTimeOffset.MinValue, DateTimeOffset.MaxValue);
    }

    public static DateTimeOffset NextDateTimeOffset(this Random random, DateTimeOffset minValue)
    {
        if (random == null)
        {
            throw new ArgumentNullException(nameof(random));
        }

        return random.NextDateTimeOffset(minValue, DateTimeOffset.Now);
    }

    public static DateTimeOffset NextDateTimeOffset(this Random random, DateTimeOffset minValue, DateTimeOffset maxValue)
    {
        if (random == null)
        {
            throw new ArgumentNullException(nameof(random));
        }

        if (minValue > maxValue)
        {
            throw new ArgumentOutOfRangeException(
                nameof(maxValue),
                $"{nameof(maxValue)} cannot be lesser than {nameof(minValue)}.");
        }

        if ((maxValue - minValue).TotalMilliseconds == 0)
        {
            return minValue;
        }

        var range = maxValue - minValue;

        var randTimeSpan = new TimeSpan((long)(random.NextDouble() * range.Ticks));

        return minValue + randTimeSpan;
    }
}
