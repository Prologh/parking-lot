﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using ParkingLot.Domain.Levels;
using ParkingLot.Domain.Logging;
using ParkingLot.Domain.SpaceHistory;
using ParkingLot.Domain.Spaces;
using ParkingLot.Domain.VehicleHistory;
using ParkingLot.Domain.Vehicles;
using System;

namespace ParkingLot.Extensions.EntityFrameworkCore.Metadata.Builders;

/// <summary>
/// Provides extension methods for <see cref="EntityTypeBuilder{TEntity}"/>.
/// </summary>
public static class EntityTypeBuilderExtensions
{
    /// <summary>
    /// Configures the SQL table for <see cref="Level"/> model using
    /// fluent mapping.
    /// </summary>
    /// <param name="builder">
    /// Current <see cref="EntityTypeBuilder{TEntity}"/> instance.
    /// </param>
    /// <returns>
    /// An instance of <see cref="EntityTypeBuilder{TEntity}"/> with
    /// configured SQL table for <see cref="Level"/> model.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="builder"/> is <see langword="null"/>.
    /// </exception>
    public static EntityTypeBuilder<Level> ConfigureLevelTable(this EntityTypeBuilder<Level> builder)
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }

        builder.ToTable(nameof(Level));

        builder.HasIndex(p => p.Position)
            .IsUnique();

        return builder;
    }

    /// <summary>
    /// Configures the SQL table for <see cref="RequestLog"/> model using
    /// fluent mapping.
    /// </summary>
    /// <param name="builder">
    /// Current <see cref="EntityTypeBuilder{TEntity}"/> instance.
    /// </param>
    /// <returns>
    /// An instance of <see cref="EntityTypeBuilder{TEntity}"/> with
    /// configured SQL table for <see cref="RequestLog"/> model.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="builder"/> is <see langword="null"/>.
    /// </exception>
    public static EntityTypeBuilder<RequestLog> ConfigureRequestLogTable(this EntityTypeBuilder<RequestLog> builder)
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }

        builder.ToTable(nameof(RequestLog));

        builder.Property(p => p.HttpMethod)
            .IsRequired();

        return builder;
    }

    /// <summary>
    /// Configures the SQL table for <see cref="Space"/> model using
    /// fluent mapping.
    /// </summary>
    /// <param name="builder">
    /// Current <see cref="EntityTypeBuilder{TEntity}"/> instance.
    /// </param>
    /// <returns>
    /// An instance of <see cref="EntityTypeBuilder{TEntity}"/> with
    /// configured SQL table for <see cref="Space"/> model.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="builder"/> is <see langword="null"/>.
    /// </exception>
    public static EntityTypeBuilder<Space> ConfigureSpaceTable(this EntityTypeBuilder<Space> builder)
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }

        builder.ToTable(nameof(Space));

        builder.HasIndex(p => p.Number)
            .IsUnique();

        builder.Property(p => p.Status)
            .HasConversion(new EnumToStringConverter<SpaceStatus>())
            .IsRequired();

        builder.HasOne(p => p.Level)
            .WithMany(p => p.Spaces)
            .OnDelete(DeleteBehavior.Cascade);

        return builder;
    }

    /// <summary>
    /// Configures the SQL table for <see cref="SpaceHistoryEntry"/> model using
    /// fluent mapping.
    /// </summary>
    /// <param name="builder">
    /// Current <see cref="EntityTypeBuilder{TEntity}"/> instance.
    /// </param>
    /// <returns>
    /// An instance of <see cref="EntityTypeBuilder{TEntity}"/> with
    /// configured SQL table for <see cref="SpaceHistoryEntry"/> model.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="builder"/> is <see langword="null"/>.
    /// </exception>
    public static EntityTypeBuilder<SpaceHistoryEntry> ConfigureSpaceHistoryEntryTable(this EntityTypeBuilder<SpaceHistoryEntry> builder)
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }

        builder.ToTable(nameof(SpaceHistoryEntry));

        builder.Property(p => p.OccupationTime)
            .HasConversion(new TimeSpanToTicksConverter());

        builder.HasOne(p => p.Space)
            .WithMany(p => p.History)
            .OnDelete(DeleteBehavior.Cascade);

        return builder;
    }

    /// <summary>
    /// Configures the SQL table for <see cref="Vehicle"/> model using
    /// fluent mapping.
    /// </summary>
    /// <param name="builder">
    /// Current <see cref="EntityTypeBuilder{TEntity}"/> instance.
    /// </param>
    /// <returns>
    /// An instance of <see cref="EntityTypeBuilder{TEntity}"/> with
    /// configured SQL table for <see cref="Vehicle"/> model.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="builder"/> is <see langword="null"/>.
    /// </exception>
    public static EntityTypeBuilder<Vehicle> ConfigureVehicleTable(this EntityTypeBuilder<Vehicle> builder)
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }

        builder.ToTable(nameof(Vehicle));

        builder.Property(p => p.LicensePlate)
            .HasMaxLength(20)
            .IsRequired();

        builder.Property(p => p.Type)
            .HasConversion(new EnumToStringConverter<VehicleType>())
            .IsRequired();

        return builder;
    }

    /// <summary>
    /// Configures the SQL table for <see cref="VehicleHistoryEntry"/> model using
    /// fluent mapping.
    /// </summary>
    /// <param name="builder">
    /// Current <see cref="EntityTypeBuilder{TEntity}"/> instance.
    /// </param>
    /// <returns>
    /// An instance of <see cref="EntityTypeBuilder{TEntity}"/> with
    /// configured SQL table for <see cref="VehicleHistoryEntry"/> model.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="builder"/> is <see langword="null"/>.
    /// </exception>
    public static EntityTypeBuilder<VehicleHistoryEntry> ConfigureVehicleHistoryEntryTable(this EntityTypeBuilder<VehicleHistoryEntry> builder)
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }

        builder.ToTable(nameof(VehicleHistoryEntry));

        builder.Property(p => p.StayTime)
            .HasConversion(new TimeSpanToTicksConverter());

        builder.HasOne(p => p.Vehicle)
            .WithMany(p => p.History)
            .OnDelete(DeleteBehavior.Cascade);

        return builder;
    }
}
