﻿using ParkingLot.Application.VehicleHistory.Repositories;
using ParkingLot.Domain.VehicleHistory;
using ParkingLot.Persistance.Repositories;

namespace ParkingLot.Persistance.VehicleHistory.Repositories;

public class VehicleHistoryEntriesRepository : GenericRepository<VehicleHistoryEntry>, IVehicleHistoryEntriesRepository
{
    public VehicleHistoryEntriesRepository(ApplicationDbContext context) : base(context)
    {

    }
}
