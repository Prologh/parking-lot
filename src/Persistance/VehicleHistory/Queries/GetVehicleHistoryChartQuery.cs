﻿using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.Charts.ViewModels;
using ParkingLot.Application.SpaceHistory.Queries;
using ParkingLot.Application.VehicleHistory.ViewModels;
using ParkingLot.Domain.VehicleHistory;
using ParkingLot.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.SpaceHistory.Queries;

public class GetVehicleHistoryChartQuery : IGetVehicleHistoryChartQuery
{
    private readonly ApplicationDbContext _dbContext;

    public GetVehicleHistoryChartQuery(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
    }

    public async Task<VehicleHistoryChartVM> GetAsync(DateTimeOffset from, DateTimeOffset to, TimeSpan step)
    {
        if (from > to)
        {
            throw new ArgumentOutOfRangeException(
                nameof(from),
                $"{nameof(from)} must be later date than {nameof(to)}.");
        }

        if (step == TimeSpan.Zero || step.IsNegative())
        {
            throw new ArgumentException(
                $"{nameof(step)} cannot be zero or negative value.",
                nameof(step));
        }

        var dateTimeFormat = GetDateTimeFormat(step);

        var xAxisLabels = new List<string>();
        var vehiclesEntered = new List<int>();
        var vehiclesExited = new List<int>();
        var vehiclesStaying = new List<int>();

        for (var rangeStart = from; rangeStart < to; rangeStart += step)
        {
            var rangeEnd = rangeStart.Add(step);

            xAxisLabels.Add(rangeStart.ToString(dateTimeFormat));

            var vehicleEnteredDaily = await _dbContext
                .VehicleHistory
                .Include(e => e.Vehicle)
                .Where(e => e.EnteredAt >= rangeStart && e.EnteredAt <= rangeEnd)
                .GroupBy(e => e.Vehicle.LicensePlate)
                .Select(g => g.Key)
                .CountAsync();
            vehiclesEntered.Add(vehicleEnteredDaily);

            var vehicleExitedDaily = await _dbContext
                .VehicleHistory
                .Include(e => e.Vehicle)
                .Where(e => e.ExitedAt.HasValue ? e.ExitedAt.Value >= rangeStart && e.ExitedAt.Value <= rangeEnd : false)
                .GroupBy(e => e.Vehicle.LicensePlate)
                .Select(g => g.Key)
                .CountAsync();
            vehiclesExited.Add(vehicleExitedDaily);

            var vehiclesStayingDaily = await _dbContext
                .VehicleHistory
                .Include(e => e.Vehicle)
                .Where(e => e.EnteredAt <= rangeEnd && !e.HasExited)
                .GroupBy(e => e.Vehicle.LicensePlate)
                .Select(g => g.Key)
                .CountAsync();
            vehiclesStaying.Add(vehiclesStayingDaily);
        }

        return new VehicleHistoryChartVM
        {
            DataSets = new List<DataSetVM<int>>
            {
                new DataSetVM<int>
                {
                    BackgroundColor = "rgba(127, 107, 0, 0.5)",
                    BorderColor = "rgba(127, 107, 0, 1)",
                    Data = vehiclesEntered,
                    Label = "Vehicles entered",
                },
                new DataSetVM<int>
                {
                    BackgroundColor = "rgba(0, 94, 70, 0.5)",
                    BorderColor = "rgba(0, 94, 70, 1)",
                    Data = vehiclesExited,
                    Label = "Vehicles exited",
                },
                new DataSetVM<int>
                {
                    BackgroundColor = "rgba(153, 51, 51, 0.5)",
                    BorderColor = "rgba(153, 51, 51, 1)",
                    Data = vehiclesStaying,
                    Label = "Stationary vehicles",
                },
            },
            TimeSpan = TimeSpan.FromDays((to - from).Days),
            Title = "Vehicles history",
            XAxisLabels = xAxisLabels,
        };
    }

    private string GetDateTimeFormat(TimeSpan step)
    {
        if (step.Duration().TotalHours <= 1)
        {
            return "HH:mm";
        }

        if (step.Duration().TotalHours <= 12)
        {
            return "HH:mm dd-MM";
        }

        if (step.Duration().TotalDays <= 7)
        {
            return "dd-MM-yyyy";
        }

        if (step.Duration().TotalDays <= 30)
        {
            return "MM-yyyy";
        }

        return "yyyy";
    }
}
