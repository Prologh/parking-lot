﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ParkingLot.Domain.Abstractions;
using ParkingLot.Domain.Levels;
using ParkingLot.Domain.Logging;
using ParkingLot.Domain.SpaceHistory;
using ParkingLot.Domain.Spaces;
using ParkingLot.Domain.VehicleHistory;
using ParkingLot.Domain.Vehicles;
using ParkingLot.Extensions.EntityFrameworkCore.Metadata.Builders;
using System;

namespace ParkingLot.Persistance;

/// <summary>
/// Represents a session with the database and can be used to query
/// and save instances of entities.
/// </summary>
public class ApplicationDbContext : DbContext
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ApplicationDbContext"/>
    /// class using the specified options.
    /// </summary>
    /// <param name="options">
    /// The options for this context.
    /// </param>
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
        ChangeTracker.Tracked += OnEntityTracked;
        ChangeTracker.StateChanged += OnEntityStateChanged;
    }

    /// <summary>
    /// Gets or sets <see cref="DbSet{TEntity}"/> of <see cref="Level"/>s.
    /// </summary>
    public DbSet<Level> Levels { get; set; }

    /// <summary>
    /// Gets or sets <see cref="DbSet{TEntity}"/> of <see cref="RequestLog"/>s.
    /// </summary>
    public DbSet<RequestLog> RequestLogs { get; set; }

    /// <summary>
    /// Gets or sets <see cref="DbSet{TEntity}"/> of <see cref="Space"/>s.
    /// </summary>
    public DbSet<Space> Spaces { get; set; }

    /// <summary>
    /// Gets or sets <see cref="DbSet{TEntity}"/> of <see cref="SpaceHistoryEntry"/>.
    /// </summary>
    public DbSet<SpaceHistoryEntry> SpaceHistory { get; set; }

    /// <summary>
    /// Gets or sets <see cref="DbSet{TEntity}"/> of <see cref="Vehicle"/>s.
    /// </summary>
    public DbSet<Vehicle> Vehicles { get; set; }

    /// <summary>
    /// Gets or sets <see cref="DbSet{TEntity}"/> of <see cref="VehicleHistoryEntry"/>.
    /// </summary>
    public DbSet<VehicleHistoryEntry> VehicleHistory { get; set; }

    /// <summary>
    /// Configures the model that was discovered by convention from
    /// the entity types exposed in <see cref="DbSet{TEntity}"/> properties
    /// on derived context.
    /// </summary>
    /// <param name="builder">
    /// The builder being used to construct the model for this context.
    /// </param>
    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.Entity<Level>().ConfigureLevelTable();
        builder.Entity<RequestLog>().ConfigureRequestLogTable();
        builder.Entity<Space>().ConfigureSpaceTable();
        builder.Entity<SpaceHistoryEntry>().ConfigureSpaceHistoryEntryTable();
        builder.Entity<Vehicle>().ConfigureVehicleTable();
        builder.Entity<VehicleHistoryEntry>().ConfigureVehicleHistoryEntryTable();
    }

    private void OnEntityTracked(object sender, EntityTrackedEventArgs e)
    {
        if (!e.FromQuery && e.Entry.State == EntityState.Added
            && e.Entry.Entity is IAuditableEntity entity
            && entity.Created == default)
        {
            entity.SetCreationDate(DateTimeOffset.Now);
        }
    }

    private void OnEntityStateChanged(object sender, EntityStateChangedEventArgs e)
    {
        if (e.NewState == EntityState.Modified
            && e.Entry.Entity is IAuditableEntity entity)
        {
            entity.SetLastUpdateDate(DateTimeOffset.Now);
        }
    }
}
