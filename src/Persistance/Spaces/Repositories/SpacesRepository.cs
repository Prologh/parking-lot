﻿using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.Spaces.Repositories;
using ParkingLot.Domain.Spaces;
using ParkingLot.Persistance.Repositories;

namespace ParkingLot.Persistance.Spaces.Repositories;

public class SpacesRepository : GenericRepository<Space>, ISpacesRepository
{
    public SpacesRepository(ApplicationDbContext context) : base(context)
    {

    }
}
