﻿using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.Spaces.Queries;
using ParkingLot.Domain.Spaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Spaces.Queries;

public class CheckSpaceIsCurrentlyOccupiedQuery : ICheckSpaceIsCurrentlyOccupiedQuery
{
    private readonly ApplicationDbContext _dbContext;

    public CheckSpaceIsCurrentlyOccupiedQuery(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
    }

    public Task<bool> CheckAsync(int id)
    {
        return _dbContext
            .Spaces
            .Where(s => s.Id == id)
            .AnyAsync(s => s.Status == SpaceStatus.Occupied);
    }
}
