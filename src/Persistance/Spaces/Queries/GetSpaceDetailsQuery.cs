﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.SpaceHistory.ViewModels;
using ParkingLot.Application.Spaces.Queries;
using ParkingLot.Application.Spaces.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Spaces.Queries;

public class GetSpaceDetailsQuery : IGetSpaceDetailsQuery
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public GetSpaceDetailsQuery(
        ApplicationDbContext dbContext,
        IMapper mapper)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    public async Task<SpaceVM> GetAsync(int id)
    {
        var spaceVM = await _dbContext
            .Spaces
            .ProjectTo<SpaceVM>(_mapper.ConfigurationProvider)
            .FirstOrDefaultAsync(s => s.Id == id);

        if (spaceVM != null)
        {
            spaceVM.RecentHistory = await _dbContext
                .SpaceHistory
                .Where(e => e.SpaceId == id)
                .OrderByDescending(e => e.TakenAt)
                .Take(10)
                .ProjectTo<SpaceHistoryEntryVM>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        return spaceVM;
    }
}
