﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.Spaces.InputModels;
using ParkingLot.Application.Spaces.Queries;
using System;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Spaces.Queries;

public class GetUpdateSpaceInputModelQuery : IGetUpdateSpaceInputModelQuery
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public GetUpdateSpaceInputModelQuery(
        ApplicationDbContext applicationDbContext,
        IMapper mapper)
    {
        _dbContext = applicationDbContext ?? throw new ArgumentNullException(nameof(applicationDbContext));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    public Task<UpdateSpaceInputModel> GetAsync(int id)
    {
        return _dbContext
            .Spaces
            .ProjectTo<UpdateSpaceInputModel>(_mapper.ConfigurationProvider)
            .FirstOrDefaultAsync(s => s.Id == id);
    }
}
