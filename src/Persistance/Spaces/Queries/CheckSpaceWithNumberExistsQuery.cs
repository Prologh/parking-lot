﻿using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.Spaces.Queries;
using System;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Spaces.Queries;

public class CheckSpaceWithNumberExistsQuery : ICheckSpaceWithNumberExistsQuery
{
    private readonly ApplicationDbContext _dbContext;

    public CheckSpaceWithNumberExistsQuery(ApplicationDbContext applicationDbContext)
    {
        _dbContext = applicationDbContext ?? throw new ArgumentNullException(nameof(applicationDbContext));
    }

    public Task<bool> CheckAsync(int number)
    {
        return _dbContext.Spaces.AnyAsync(s => s.Number == number);
    }

    public Task<bool> CheckAsync(int number, int spaceIdToIgnore)
    {
        return _dbContext
            .Spaces
            .AnyAsync(s => s.Number == number && s.Id != spaceIdToIgnore);
    }
}
