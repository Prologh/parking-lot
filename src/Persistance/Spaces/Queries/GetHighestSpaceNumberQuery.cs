﻿using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.Spaces.Queries;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Spaces.Queries;

public class GetHighestSpaceNumberQuery : IGetHighestSpaceNumberQuery
{
    private readonly ApplicationDbContext _dbContext;

    public GetHighestSpaceNumberQuery(ApplicationDbContext applicationDbContext)
    {
        _dbContext = applicationDbContext ?? throw new ArgumentNullException(nameof(applicationDbContext));
    }

    public Task<int> GetAsync()
    {
        return _dbContext.Spaces.MaxAsync(s => s.Number);
    }

    public Task<int> GetAsync(int levelId)
    {
        return _dbContext
            .Spaces
            .Where(s => s.LevelId == levelId)
            .MaxAsync(s => s.Number);
    }
}
