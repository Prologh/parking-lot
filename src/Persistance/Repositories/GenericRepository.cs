﻿using ParkingLot.Application;
using ParkingLot.Application.Repositories;
using ParkingLot.Domain.Abstractions;
using System;
using System.Collections.Generic;

namespace ParkingLot.Persistance.Repositories;

public class GenericRepository<TEntity> : GenericReadOnlyRepository<TEntity>, IRepository<TEntity>
    where TEntity : class, IBaseEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="GenericRepository{TEntity}"/>
    /// class using database context.
    /// </summary>
    public GenericRepository(ApplicationDbContext context) : base(context)
    {

    }

    /// <summary>
    /// Synchronously marks an entity to be added when
    /// <see cref="IUnitOfWork.SaveChangesAsync"/> is called.
    /// </summary>
    /// <param name="entity">
    /// The entity to add.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="entity"/> is <see langword="null"/>.
    /// </exception>
    public void Add(TEntity entity)
    {
        if (entity == null)
        {
            throw new ArgumentNullException(nameof(entity));
        }

        DbSet.Add(entity);
    }

    /// <summary>
    /// Synchronously marks a collection of entities to be added
    /// when <see cref="IUnitOfWork.SaveChangesAsync"/> is called.
    /// </summary>
    /// <param name="entities">
    /// The entities to add.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="entities"/> is <see langword="null"/>.
    /// </exception>
    public void AddRange(IEnumerable<TEntity> entities)
    {
        if (entities == null)
        {
            throw new ArgumentNullException(nameof(entities));
        }

        DbSet.AddRange(entities);
    }

    /// <summary>
    /// Synchronously marks an entity to be removed when
    /// <see cref="IUnitOfWork.SaveChangesAsync"/> is called.
    /// </summary>
    /// <param name="entity">
    /// The entity to remove.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="entity"/> is <see langword="null"/>.
    /// </exception>
    public void Remove(TEntity entity)
    {
        if (entity is null)
        {
            throw new ArgumentNullException(nameof(entity));
        }

        DbSet.Remove(entity);
    }

    /// <summary>
    /// Synchronously marks a collection of entities to be removed
    /// when <see cref="IUnitOfWork.SaveChangesAsync"/> is called.
    /// </summary>
    /// <param name="entities">
    /// The entities to remove.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="entities"/> is <see langword="null"/>.
    /// </exception>
    public void RemoveRange(IEnumerable<TEntity> entities)
    {
        if (entities == null)
        {
            throw new ArgumentNullException(nameof(entities));
        }

        DbSet.RemoveRange(entities);
    }

    /// <summary>
    /// Synchronously marks an entity to be update when
    /// <see cref="IUnitOfWork.SaveChangesAsync"/> is called.
    /// </summary>
    /// <param name="entity">
    /// The entity to update.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="entity"/> is <see langword="null"/>.
    /// </exception>
    public void Update(TEntity entity)
    {
        if (entity == null)
        {
            throw new ArgumentNullException(nameof(entity));
        }

        DbSet.Update(entity);
    }

    /// <summary>
    /// Synchronously marks a collection of entities to be updated
    /// when <see cref="IUnitOfWork.SaveChangesAsync"/> is called.
    /// </summary>
    /// <param name="entities">
    /// The entities to update.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="entities"/> is <see langword="null"/>.
    /// </exception>
    public void UpdateRange(IEnumerable<TEntity> entities)
    {
        if (entities == null)
        {
            throw new ArgumentNullException(nameof(entities));
        }

        DbSet.UpdateRange(entities);
    }
}
