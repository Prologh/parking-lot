﻿using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.Repositories;
using ParkingLot.Domain.Abstractions;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Repositories;

public class GenericReadOnlyRepository<TEntity> : IReadOnlyRepository<TEntity>
    where TEntity : class, IBaseEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="GenericEFCoreUnitOfWork"/>
    /// class using database context.
    /// </summary>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="context"/> is <see langword="null"/>.
    /// </exception>
    public GenericReadOnlyRepository(ApplicationDbContext context)
    {
        DbContext = context ?? throw new ArgumentNullException(nameof(context));
        DbSet = context.Set<TEntity>();
    }

    /// <summary>
    /// Gets the <see cref="IQueryable{TEntity}"/> for executing queries upon it.
    /// </summary>
    public IQueryable<TEntity> Queryable => DbSet.AsQueryable();

    /// <summary>
    /// Gets the database context.
    /// </summary>
    protected ApplicationDbContext DbContext { get; }

    /// <summary>
    /// Gets the <see cref="DbSet{TEntity}"/> of type of entity this
    /// repository manages.
    /// </summary>
    protected DbSet<TEntity> DbSet { get; }

    /// <summary>
    /// Asynchronously returns single entity matching provided identifiers.
    /// </summary>
    /// <param name="identifiers">
    /// The entity identifiers.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="identifiers"/> is <see langword="null"/>.
    /// </exception>
    public async Task<TEntity> FindAsync(params object[] identifiers)
    {
        if (identifiers is null)
        {
            throw new ArgumentNullException(nameof(identifiers));
        }

        return await DbSet.FindAsync(identifiers);
    }
}
