﻿using ParkingLot.Application.Vehicles.Repositories;
using ParkingLot.Domain.Vehicles;
using ParkingLot.Persistance.Repositories;

namespace ParkingLot.Persistance.Vehicles.Repositories;

public class VehiclesRepository : GenericRepository<Vehicle>, IVehiclesRepository
{
    public VehiclesRepository(ApplicationDbContext context) : base(context)
    {

    }
}
