﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using ParkingLot.Application.VehicleHistory.ViewModels;
using ParkingLot.Application.Vehicles.Queries;
using ParkingLot.Application.Vehicles.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot.Persistance.Vehicles.Queries;

public class GetVehicleDetailsQuery : IGetVehicleDetailsQuery
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public GetVehicleDetailsQuery(
        ApplicationDbContext dbContext,
        IMapper mapper)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    public async Task<VehicleVM> GetAsync(int id)
    {
        var vehicleVM = await _dbContext
            .Vehicles
            .ProjectTo<VehicleVM>(_mapper.ConfigurationProvider)
            .FirstOrDefaultAsync(v => v.Id == id);

        if (vehicleVM != null)
        {
            vehicleVM.History = await _dbContext
                .VehicleHistory
                .Where(e => e.VehicleId == id)
                .OrderByDescending(e => e.EnteredAt)
                .Take(10)
                .ProjectTo<VehicleHistoryEntryVM>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        return vehicleVM;
    }
}
