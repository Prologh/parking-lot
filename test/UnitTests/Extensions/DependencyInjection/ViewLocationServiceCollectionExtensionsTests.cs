﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ParkingLot.Extensions.DependencyInjection;
using ParkingLot.WebApp.Mvc.Razor;
using Xunit;

namespace ParkingLot.UnitTests.Extensions.DependencyInjection;

public class ViewLocationServiceCollectionExtensionsTests
{
    [Fact]
    public void ExtensionMethodAddsNewViewLocationExpander_InstanceParameter()
    {
        // Arrange
        var services = new ServiceCollection();
        services.AddMvc();
        services.AddLogging();

        // Act
        var serviceProvider = services.BuildServiceProvider();
        var preExtensionOptions = serviceProvider.GetRequiredService<IOptions<RazorViewEngineOptions>>().Value;
        services.AddViewLocationExpander(new ViewLocationExpander());
        serviceProvider = services.BuildServiceProvider();
        var postExtensionOptions = serviceProvider.GetRequiredService<IOptions<RazorViewEngineOptions>>().Value;
        serviceProvider.Dispose();

        // Assert
        postExtensionOptions
            .ViewLocationExpanders
            .Count
            .Should()
            .BeGreaterThan(preExtensionOptions.ViewLocationExpanders.Count);
    }

    [Fact]
    public void ExtensionMethodAddsNewViewLocationExpander_TypeParameter()
    {
        // Arrange
        var services = new ServiceCollection();
        services.AddMvc();

        // These two are required to activate the RazorViewEngineOptions.
        services.AddSingleton<IHostEnvironment, HostingEnvironment>();
        services.AddSingleton<ILoggerFactory, LoggerFactory>();

        // Act
        var serviceProvider = services.BuildServiceProvider();
        var preExtensionOptions = serviceProvider.GetRequiredService<IOptions<RazorViewEngineOptions>>().Value;
        services.AddViewLocationExpander<ViewLocationExpander>();
        serviceProvider = services.BuildServiceProvider();
        var postExtensionOptions = serviceProvider.GetRequiredService<IOptions<RazorViewEngineOptions>>().Value;
        serviceProvider.Dispose();

        // Assert
        postExtensionOptions
            .ViewLocationExpanders
            .Count
            .Should()
            .BeGreaterThan(preExtensionOptions.ViewLocationExpanders.Count);
    }
}
