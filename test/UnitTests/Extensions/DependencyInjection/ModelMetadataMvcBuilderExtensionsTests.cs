﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.ObjectPool;
using Microsoft.Extensions.Options;
using ParkingLot.Extensions.DependencyInjection;
using ParkingLot.WebApp.Mvc.ModelBinding.Metadata;
using Xunit;

namespace ParkingLot.UnitTests.Extensions.DependencyInjection;

public class ModelMetadataMvcBuilderExtensionsTests
{
    [Fact]
    public void ExtensionMethodAddsModelMetadataDetailsProvider()
    {
        // Arrange
        var services = new ServiceCollection();
        var mvcBuilder = services.AddMvc();

        // These two are required to activate MvcOptions.
        services.AddSingleton<ILoggerFactory, LoggerFactory>();
        services.AddSingleton<ObjectPoolProvider, DefaultObjectPoolProvider>();

        // Act
        var serviceProvider = services.BuildServiceProvider();
        var preExtensionOptions = serviceProvider.GetRequiredService<IOptions<MvcOptions>>().Value;
        mvcBuilder.AddModelMetadataDetailsProvider(new CustomDisplayMetadataProvider());
        serviceProvider = services.BuildServiceProvider();
        var postExtensionOptions = serviceProvider.GetRequiredService<IOptions<MvcOptions>>().Value;
        serviceProvider.Dispose();

        // Assert
        postExtensionOptions
            .ModelMetadataDetailsProviders
            .Count
            .Should()
            .BeGreaterThan(preExtensionOptions.ModelMetadataDetailsProviders.Count);
    }
}
