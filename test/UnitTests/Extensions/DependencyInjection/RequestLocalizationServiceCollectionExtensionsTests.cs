﻿using FluentAssertions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.Internal;
using Microsoft.Extensions.Options;
using ParkingLot.Extensions.DependencyInjection;
using Xunit;

namespace ParkingLot.UnitTests.Extensions.DependencyInjection;

public class RequestLocalizationServiceCollectionExtensionsTests
{
    [Fact]
    public void ExtensionMethodAddsMoreSupportedCultures()
    {
        // Arrange
        var services = new ServiceCollection();
        services.AddLogging();
        services.AddMvc();

        // These two are required to activate the RequestLocalizationOptions.
        services.AddSingleton<IHostEnvironment, HostingEnvironment>();

        // Act
        var serviceProvider = services.BuildServiceProvider();
        var preExtensionOptions = serviceProvider
            .GetRequiredService<IOptions<RequestLocalizationOptions>>()
            .Value;
        services.ConfigureRequestLocalizationOptions();
        serviceProvider = services.BuildServiceProvider();
        var postExtensionOptions = serviceProvider
            .GetRequiredService<IOptions<RequestLocalizationOptions>>()
            .Value;
        serviceProvider.Dispose();

        // Assert
        postExtensionOptions
            .SupportedCultures
            .Count
            .Should()
            .BeGreaterThan(preExtensionOptions.SupportedCultures.Count);
        postExtensionOptions
            .SupportedUICultures
            .Count
            .Should()
            .BeGreaterThan(preExtensionOptions.SupportedUICultures.Count);
    }
}
