﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc.DataAnnotations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using ParkingLot.Extensions.DependencyInjection;
using Xunit;

namespace ParkingLot.UnitTests.Extensions.DependencyInjection;

public class DataAnnotationsLocalizationServiceCollectionExtensionsTests
{
    [Fact]
    public void ExtensionMethodAddsCustomStringLocalizerFactory()
    {
        // Arrange
        var services = new ServiceCollection();
        services.AddLogging();
        var mvcBuilder = services.AddMvc();

        // Act
        var serviceProvider = services.BuildServiceProvider();
        var preExtensionOptions = serviceProvider.GetRequiredService<IOptions<MvcDataAnnotationsLocalizationOptions>>().Value;
        mvcBuilder.AddDataAnnotationsLocalization<string>();
        serviceProvider = services.BuildServiceProvider();
        var postExtensionOptions = serviceProvider.GetRequiredService<IOptions<MvcDataAnnotationsLocalizationOptions>>().Value;
        serviceProvider.Dispose();

        // Assert
        preExtensionOptions
            .Should()
            .NotBeSameAs(postExtensionOptions);
        preExtensionOptions
             .DataAnnotationLocalizerProvider
             .Should()
             .NotBeSameAs(postExtensionOptions.DataAnnotationLocalizerProvider);
    }
}
