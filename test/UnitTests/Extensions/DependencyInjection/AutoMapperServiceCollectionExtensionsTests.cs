﻿using AutoMapper;
using AutoMapper.Internal;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using ParkingLot.Extensions.DependencyInjection;
using ParkingLot.WebApp.Mapping.Logging;
using Xunit;

namespace ParkingLot.UnitTests.Extensions.DependencyInjection;

public class AutoMapperServiceCollectionExtensionsTests
{
    [Fact]
    public void ExtensionMethodAddsAutoMapperWithProfiles()
    {
        // Arrange
        var services = new ServiceCollection();

        // Act
        var serviceProvider = services.BuildServiceProvider();
        var preExtensionMapper = serviceProvider.GetService<IMapper>();
        services.AddAutoMapperWithProfiles();
        serviceProvider = services.BuildServiceProvider();
        var postExtensionMapper = serviceProvider.GetService<IMapper>();
        serviceProvider.Dispose();

        // Assert
        preExtensionMapper
            .Should()
            .BeNull("Because AutoMapper has not been registered yet.");
        postExtensionMapper
            .Should()
            .NotBeNull("Because extension method should already add " +
                    "AutoMapper to the service collection.");
        postExtensionMapper.ConfigurationProvider.Internal()
            .Profiles
            .Should()
            .NotBeEmpty("Because extension method should add mapping profiles.");
    }
}
