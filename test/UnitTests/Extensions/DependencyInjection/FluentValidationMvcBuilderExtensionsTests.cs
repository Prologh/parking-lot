﻿using FluentAssertions;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using ParkingLot.Application.Levels.InputModels;
using ParkingLot.Extensions.DependencyInjection;
using ParkingLot.UnitTests.Extensions.DependencyInjection.Stubs;
using Xunit;

namespace ParkingLot.UnitTests.Extensions.DependencyInjection;

public class FluentValidationMvcBuilderExtensionsTests
{
    [Fact]
    public void ExtensionMethod_AddsFluentValidationWithValidators()
    {
        // Arrange
        var services = new ServiceCollection();
        var mvcBuilder = services.AddMvc();

        // Act
        var serviceProvider = services.BuildServiceProvider();
        var preExtensionValidator = serviceProvider.GetService<IValidator<CreateLevelInputModel>>();
        mvcBuilder.AddFluentValidationWithValidators();
        serviceProvider = services.BuildServiceProvider();
        var postExtensionValidator = serviceProvider.GetService<IValidator<CreateLevelInputModel>>();
        var validatorFactory = serviceProvider.GetService<IValidatorFactory>();
        serviceProvider.Dispose();

        // Assert
        preExtensionValidator.Should().BeNull();
        postExtensionValidator.Should().NotBeNull();
        validatorFactory.Should().NotBeNull();
    }

    [Fact]
    public void ExtensionMethod_AddsFluentValidationWithValidators_TypeParameter()
    {
        // Arrange
        var services = new ServiceCollection();
        var mvcBuilder = services.AddMvc();

        // Act
        var serviceProvider = services.BuildServiceProvider();
        var preExtensionValidator = serviceProvider.GetService<IValidator<Stub>>();
        mvcBuilder.AddFluentValidationWithValidatorsFrom<StubValidator>();
        serviceProvider = services.BuildServiceProvider();
        var postExtensionValidator = serviceProvider.GetService<IValidator<Stub>>();
        var validatorFactory = serviceProvider.GetService<IValidatorFactory>();
        serviceProvider.Dispose();

        // Assert
        preExtensionValidator.Should().BeNull();
        postExtensionValidator.Should().NotBeNull();
        validatorFactory.Should().NotBeNull();
    }

    [Fact]
    public void ExtensionMethod_AddsFluentValidationWithValidators_TypeAsParameter()
    {
        // Arrange
        var services = new ServiceCollection();
        var mvcBuilder = services.AddMvc();
        var validatorType = typeof(StubValidator);

        // Act
        var serviceProvider = services.BuildServiceProvider();
        var preExtensionValidator = serviceProvider.GetService(typeof(IValidator<Stub>)) as IValidator<Stub>;
        mvcBuilder.AddFluentValidationWithValidatorsFrom(validatorType);
        serviceProvider = services.BuildServiceProvider();
        var postExtensionValidator = serviceProvider.GetService(typeof(IValidator<Stub>)) as IValidator<Stub>;
        var validatorFactory = serviceProvider.GetService<IValidatorFactory>();
        serviceProvider.Dispose();

        // Assert
        preExtensionValidator.Should().BeNull();
        postExtensionValidator.Should().NotBeNull();
        validatorFactory.Should().NotBeNull();
    }
}
