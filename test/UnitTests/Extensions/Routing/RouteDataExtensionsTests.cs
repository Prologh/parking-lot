﻿using FluentAssertions;
using Microsoft.AspNetCore.Routing;
using ParkingLot.Extensions.Routing;
using Xunit;

namespace ParkingLot.UnitTests.Extensions.Routing;

public class RouteDataExtensionsTests
{
    [Fact]
    public void ExtensionMethodGetsActionRouteValue()
    {
        // Arrange
        var routeValuesDictionary = new RouteValueDictionary
        {
            { "Action", "Index" },
        };
        var routeData = new RouteData(routeValuesDictionary);

        // Act
        var routeDataAction = routeData.GetAction();

        // Assert
        routeDataAction.Should().Be("Index");
    }

    [Fact]
    public void ExtensionMethodGetsAreaRouteValue()
    {
        // Arrange
        var routeValuesDictionary = new RouteValueDictionary
        {
            { "Area", "Admin" },
        };
        var routeData = new RouteData(routeValuesDictionary);

        // Act
        var routeDataArea = routeData.GetArea();

        // Assert
        routeDataArea.Should().Be("Admin");
    }

    [Fact]
    public void ExtensionMethodGetsControllerRouteValue()
    {
        // Arrange
        var routeValuesDictionary = new RouteValueDictionary
        {
            { "Controller", "Home" },
        };
        var routeData = new RouteData(routeValuesDictionary);

        // Act
        var routeDatatController = routeData.GetController();

        // Assert
        routeDatatController.Should().Be("Home");
    }
}
