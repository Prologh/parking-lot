﻿using FluentAssertions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.DependencyInjection;
using ParkingLot.Extensions.Builder;
using ParkingLot.Extensions.DependencyInjection;
using System;
using Xunit;

namespace ParkingLot.UnitTests.Extensions.Builder;

public class RequestLocalizationBuilderExtensionsTests
{
    [Fact]
    public void ExtensionMethodAddsLocalizationComponent()
    {
        // Arrange
        var services = new ServiceCollection();
        services.AddLogging();
        services.ConfigureRequestLocalizationOptions();
        using var serviceProvider = services.BuildServiceProvider();
        var builder = new ApplicationBuilder(serviceProvider);

        // Act
        var preExtensionsDelegate = builder.Build();
        builder.UseDefaultRequestLocalization();
        var postExtensionDelegate = builder.Build();

        // Assert
        postExtensionDelegate
            .Target
            .Should()
            .BeOfType<RequestLocalizationMiddleware>();
        postExtensionDelegate
            .Target
            .Should()
            .NotBe(preExtensionsDelegate.Target);
    }

    [Fact]
    public void UnregisteredRequestLocalizationOptionsThrowsAnException()
    {
        // Arrange
        var services = new ServiceCollection();
        using var serviceProvider = services.BuildServiceProvider();
        var builder = new ApplicationBuilder(serviceProvider);

        // Act & Arrange
        Assert.Throws<InvalidOperationException>(() => builder.UseDefaultRequestLocalization());
    }
}
