﻿using FluentAssertions;
using ParkingLot.Extensions;
using Xunit;

namespace ParkingLot.UnitTests.Extensions.String;

public class ToPascalCaseTests
{
    [Theory]
    [InlineData("", "")]
    [InlineData("1", "1")]
    [InlineData("a", "A")]
    [InlineData("A A", "A A")]
    [InlineData("a a", "A A")]
    public void ExtensionMethod_Returns_ExpectedResult(string input, string expectedResult)
    {
        // Act
        var result = input.ToPascalCase();

        // Arrange
        result.Should().Be(expectedResult);
    }
}
