﻿using FluentAssertions;
using ParkingLot.Extensions.Pagination;
using ParkingLot.Persistance.Pagination.Models;
using Xunit;

namespace ParkingLot.UnitTests.Extensions.ViewModels;

public class IPagedResultExtensionsTests
{
    [Fact]
    public void ExtensionMethodShouldCheckIfCurrentPageIsTheFirstOne()
    {
        // Arrange
        var pageNumber = 1;
        var totalPages = 10;
        var result = new PagedResult<int>
        {
            PageNumber = pageNumber,
            TotalPages = totalPages,
        };

        // Act
        var isFirstPage = result.IsFirstPage();

        // Assert
        isFirstPage.Should().BeTrue();
    }

    [Fact]
    public void ExtensionMethodShouldCheckIfCurrentPageIsTheLastOne()
    {
        // Arrange
        var pageNumber = 10;
        var totalPages = 10;
        var result = new PagedResult<int>
        {
            PageNumber = pageNumber,
            TotalPages = totalPages,
        };

        // Act
        var isLastPage = result.IsLastPage();

        // Assert
        isLastPage.Should().BeTrue();
    }

    [Fact]
    public void ExtensionMethodShouldCheckIfCurrentPageIsTheSecondOne()
    {
        // Arrange
        var pageNumber = 2;
        var totalPages = 10;
        var result = new PagedResult<int>
        {
            PageNumber = pageNumber,
            TotalPages = totalPages,
        };

        // Act
        var isSecondPage = result.IsSecondPage();

        // Assert
        isSecondPage.Should().BeTrue();
    }

    [Fact]
    public void ExtensionMethodShouldCheckIfCurrentPageIsThePenultimateOne()
    {
        // Arrange
        var pageNumber = 9;
        var totalPages = 10;
        var result = new PagedResult<int>
        {
            PageNumber = pageNumber,
            TotalPages = totalPages,
        };

        // Act
        var isPenultimatePage = result.IsPenultimatePage();

        // Assert
        isPenultimatePage.Should().BeTrue();
    }
}
