﻿using FluentAssertions;
using Microsoft.Extensions.Localization;
using Moq;
using ParkingLot.Persistance.Exceptions;
using ParkingLot.WebApp.Services;
using System;
using System.Collections.Generic;
using Xunit;

namespace ParkingLot.UnitTests.Services.Exceptions;

public class ExceptionMessageProviderTests
{
    public static IEnumerable<object[]> Data =>
        new List<object[]>
        {
                    new object[] { new DbException(), "DbOperationFailedErrorMessage" },
                    new object[] { new Exception(), "GeneralErrorMessage" },
        };

    [Theory]
    [MemberData(nameof(Data))]
    public void GetExceptionMessage_Returns_Proper_Localized_ErrorMessage(Exception exception, string key)
    {
        // Arrange
        var errorMessage = "LocalizedError";
        var localizerMock = new Mock<IStringLocalizer<ExceptionMessageProvider>>();
        localizerMock.SetupGet(localizer => localizer[key])
            .Returns(new LocalizedString(key, errorMessage));
        var provider = new ExceptionMessageProvider(localizerMock.Object);

        // Act
        var result = provider.GetExceptionMessage(exception);

        // Assert
        result.Should().Be(errorMessage);
    }
}
