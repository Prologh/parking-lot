﻿using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.AspNetCore.Routing;
using Moq;
using ParkingLot.WebApp.Mvc.TagHelpers;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace ParkingLot.UnitTests.Mvc.TagHelpers;

public class AnchorActiveClassTagHelperTests
{
    private readonly TagHelperContext _tagHelperContext;
    private readonly ActionContext _actionContext;
    private readonly ViewDataDictionary _viewData;
    private readonly ViewContext _viewContext;
    private readonly TagHelperOutput _tagHelperOutput;
    private readonly AnchorActiveClassTagHelper _anchorActiveClassTagHelper;

    public AnchorActiveClassTagHelperTests()
    {
        _tagHelperContext = new TagHelperContext(
            tagName: "a",
            allAttributes: new TagHelperAttributeList
            {
                            { "asp-action", "Index" },
                            { "asp-controller", "Home" },
                            { "asp-add-active-class", "true" },
            },
            items: new Dictionary<object, object>(),
            uniqueId: "test");
        _actionContext = new ActionContext(
            new DefaultHttpContext(),
            new RouteData(),
            new ActionDescriptor(),
            new ModelStateDictionary());
        _viewData = new ViewDataDictionary(
            new EmptyModelMetadataProvider(),
            new ModelStateDictionary());
        _viewContext = new ViewContext(
            _actionContext,
            Mock.Of<IView>(),
            _viewData,
            Mock.Of<ITempDataDictionary>(),
            TextWriter.Null,
            new HtmlHelperOptions());
        _tagHelperOutput = new TagHelperOutput(
            tagName: "a",
            attributes: new TagHelperAttributeList(),
            getChildContentAsync: (result, encoder) =>
            {
                var tagHelperContent = new DefaultTagHelperContent();
                tagHelperContent.SetHtmlContent(string.Empty);

                return Task.FromResult<TagHelperContent>(tagHelperContent);
            });
        _anchorActiveClassTagHelper = new AnchorActiveClassTagHelper
        {
            AddActiveClass = true,
            ViewContext = _viewContext,
        };
    }

    [Fact]
    public void TagHelper_Adds_ActiveCssClass_When_Route_Matches()
    {
        // Arrange
        _viewContext.RouteData.Values.Add("area", string.Empty);
        _viewContext.RouteData.Values.Add("controller", "Home");

        // Act
        _anchorActiveClassTagHelper.Process(_tagHelperContext, _tagHelperOutput);

        // Assert
        _tagHelperOutput
            .Attributes
            .Should()
            .Contain(a => a.Name == "class");
        _tagHelperOutput
            .Attributes["class"]
            .Value
            .Should()
            .Be("active");
    }

    [Fact]
    public void TagHelper_Appends_ActiveCssClass_At_The_End()
    {
        // Arrange
        var tagHelperOutput = new TagHelperOutput(
            tagName: "a",
            attributes: new TagHelperAttributeList
            {
                new TagHelperAttribute("class", "link"),
            },
            getChildContentAsync: (result, encoder) =>
            {
                var tagHelperContent = new DefaultTagHelperContent();
                tagHelperContent.SetHtmlContent(string.Empty);

                return Task.FromResult<TagHelperContent>(tagHelperContent);
            });
        _viewContext.RouteData.Values.Add("area", string.Empty);
        _viewContext.RouteData.Values.Add("controller", "Home");

        // Act
        _anchorActiveClassTagHelper.Process(_tagHelperContext, tagHelperOutput);

        // Assert
        tagHelperOutput
            .Attributes
            .Should()
            .Contain(a => a.Name == "class");
        tagHelperOutput
            .Attributes["class"]
            .Value
            .Should()
            .Be("link active");
    }

    [Fact]
    public void TagHelper_Do_Not_Add_ActiveCssClass_When_Route_Does_Not_Match()
    {
        // Arrange
        var tagHelperContext = new TagHelperContext(
           tagName: "a",
           allAttributes: new TagHelperAttributeList
           {
                { "asp-action", "Index" },
                { "asp-controller", "Levels" },
                { "asp-add-active-class", "true" },
           },
           items: new Dictionary<object, object>(),
           uniqueId: "test");
        _viewContext.RouteData.Values.Add("area", string.Empty);
        _viewContext.RouteData.Values.Add("controller", "Home");

        // Act
        _anchorActiveClassTagHelper.Process(tagHelperContext, _tagHelperOutput);

        // Assert
        _tagHelperOutput
            .Attributes
            .Should()
            .NotContain(a => a.Name == "class");
        _tagHelperOutput
            .Attributes
            .Should()
            .NotContain(a => a.Name == "active");
    }
}
