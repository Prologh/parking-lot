﻿using Xunit;

namespace ParkingLot.IntegrationTests;

/// <summary>
/// Provides base class fixture for integration testing.
/// </summary>
public abstract class ParkingLotWebAppFixtureBase : IClassFixture<ParkingLotWebAppFactory>
{
    protected ParkingLotWebAppFixtureBase(ParkingLotWebAppFactory factory)
    {
        Factory = factory;
    }

    /// <summary>
    /// Gets the web application factory.
    /// </summary>
    protected ParkingLotWebAppFactory Factory { get; }
}
