﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ParkingLot.Persistance;
using ParkingLot.Persistance.Seeding;
using ParkingLot.WebApp;
using System;
using System.Linq;
using System.Net.Http;

namespace ParkingLot.IntegrationTests;

/// <summary>
/// Factory for bootstrapping Parking Lot Web Applciation in memory
/// for functional end to end tests.
/// </summary>
public class ParkingLotWebAppFactory : WebApplicationFactory<Startup>
{
    private ServiceProvider _internalServiceProvider;

    /// <summary>
    /// Creates a new instance of an <see cref="HttpClient"/> that
    /// can be used to send <see cref="HttpRequestMessage"/> to the server.
    /// The base address of the <see cref="HttpClient"/> instance
    /// will be set to https://localhost:443.
    /// </summary>
    /// <returns>
    /// An instance of <see cref="HttpClient"/>.
    /// </returns>
    public virtual HttpClient CreateHttpsClient()
    {
        return CreateClient(new WebApplicationFactoryClientOptions
        {
            BaseAddress = new Uri("https://localhost:443"),
        });
    }

    /// <summary>
    /// Creates a new instance of an <see cref="HttpClient"/> that
    /// can be used to send <see cref="HttpRequestMessage"/> to the server.
    /// The base address of the <see cref="HttpClient"/> instance
    /// will be set to https://localhost:443.
    /// </summary>
    /// <param name="options">
    /// Options used for creation <see cref="HttpClient"/>.
    /// </param>
    /// <returns>
    /// An instance of <see cref="HttpClient"/>.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="options"/> is <see langword="null"/>.
    /// </exception>
    public virtual HttpClient CreateHttpsClient(WebApplicationFactoryClientOptions options)
    {
        if (options == null)
        {
            throw new ArgumentNullException(nameof(options));
        }

        options.BaseAddress = new Uri("https://localhost:443");

        return CreateClient(options);
    }

    /// <summary>
    /// Creates a new instance of an <see cref="HttpClient"/> that
    /// can be used to send <see cref="HttpRequestMessage"/> to the server.
    /// The base address of the <see cref="HttpClient"/> instance
    /// will be set to https://localhost:443.
    /// </summary>
    /// <param name="options">
    /// Action for configuring <see cref="WebApplicationFactoryClientOptions"/>
    /// used for creation <see cref="HttpClient"/>.
    /// </param>
    /// <returns>
    /// An instance of <see cref="HttpClient"/>.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="action"/> is <see langword="null"/>.
    /// </exception>
    public virtual HttpClient CreateHttpsClient(Action<WebApplicationFactoryClientOptions> action)
    {
        if (action == null)
        {
            throw new ArgumentNullException(nameof(action));
        }

        var defaultOptions = ClientOptions;
        defaultOptions.BaseAddress = new Uri("https://localhost:443");
        action(defaultOptions);

        return CreateClient(defaultOptions);
    }

    /// <summary>
    /// Gives a fixture an opportunity to configure the application
    /// before it gets built.
    /// </summary>
    /// <param name="builder">
    /// The <see cref="IWebHostBuilder"/> for the application.
    /// </param>
    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        builder.ConfigureTestServices(services =>
        {
            // Clean already registered DbContext options;
            RemoveDbContextOptions<ApplicationDbContext>(services);

            // Create seperate service provider.
            _internalServiceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Add a database context using an in-memory database for testing.
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseInMemoryDatabase("InMemoryDatabase");
                options.UseInternalServiceProvider(_internalServiceProvider);
            });

            using var serviceProvider = services.BuildServiceProvider();

            // Create a scope to obtain a reference to the database contexts.
            using var scope = serviceProvider.CreateScope();
            var scopedServiceProvider = scope.ServiceProvider;
            var dbInitializer = scopedServiceProvider.GetRequiredService<DatabaseInitializer>();

            try
            {
                dbInitializer.Initialize(migrate: false, ensureCreated: true);
            }
            catch (Exception ex)
            {
                var logger = scopedServiceProvider.GetRequiredService<ILogger<ParkingLotWebAppFactory>>();

                logger.LogError(ex, "An error occurred while seeding the database.");
            }
        });
    }

    protected override void Dispose(bool disposing)
    {
        if (!disposing)
        {
            if (_internalServiceProvider != null)
            {
                _internalServiceProvider.Dispose();
            }
        }
    }

    private void RemoveDbContextOptions<TDbContext>(IServiceCollection services)
        where TDbContext : DbContext
    {
        var descriptor = services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<TDbContext>));
        if (descriptor != null)
        {
            services.Remove(descriptor);
        }
    }
}
