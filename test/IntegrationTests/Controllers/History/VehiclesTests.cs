﻿using FluentAssertions;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace ParkingLot.IntegrationTests.Controllers.History;

public class VehiclesTests : ParkingLotWebAppFixtureBase
{
    public VehiclesTests(ParkingLotWebAppFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Action_ReturnsSuccessfulStatusCode()
    {
        // Arrange
        var uri = "History/Vehicles";
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.GetAsync(uri);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task Action_ReturnsBadRequest_WhenModelStatelIsInvalid()
    {
        // Arrange
        var uri = "History/Vehicles?PageSize=-1";
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.GetAsync(uri);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }
}
