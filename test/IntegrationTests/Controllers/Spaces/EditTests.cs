﻿using FluentAssertions;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace ParkingLot.IntegrationTests.Controllers.Spaces;

public class EditTests : ParkingLotWebAppFixtureBase
{
    public EditTests(ParkingLotWebAppFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Action_ReturnsSuccessfulStatusCode()
    {
        // Arrange
        var id = 1;
        var uri = $"Spaces/{id}/Edit";
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.GetAsync(uri);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task Action_ReturnsNotFound_WhenNoMatchingEntityIsFound()
    {
        // Arrange
        var id = 0;
        var uri = $"Spaces/{id}/Edit";
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.GetAsync(uri);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }
}
