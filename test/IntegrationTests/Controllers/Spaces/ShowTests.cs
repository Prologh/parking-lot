﻿using FluentAssertions;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace ParkingLot.IntegrationTests.Controllers.Spaces;

public class ShowTests : ParkingLotWebAppFixtureBase
{
    public ShowTests(ParkingLotWebAppFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Action_ReturnsSuccessfulStatusCode_WhenMatchingEntityIsFound()
    {
        // Arrange
        var spaceId = 1;
        var uri = $"Spaces/{spaceId}";
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.GetAsync(uri);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task Action_ReturnsNotFound_WhenNoMatchingEntityIsFound()
    {
        // Arrange
        var spaceId = 0;
        var uri = $"Spaces/{spaceId}";
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.GetAsync(uri);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }
}
