﻿using FluentAssertions;
using ParkingLot.Application.Spaces.InputModels;
using ParkingLot.Domain.Spaces;
using ParkingLot.Infrastructure.Http.Content.Builders;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace ParkingLot.IntegrationTests.Controllers.Spaces;

public class UpdateTests : ParkingLotWebAppFixtureBase
{
    public UpdateTests(ParkingLotWebAppFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Action_UpdatesEntity_WhenModelStateIsValid()
    {
        // Arrange
        var inputModel = new UpdateSpaceInputModel
        {
            Id = 1,
            LevelId = 1,
            Number = 1,
            Status = SpaceStatus.Unoccupied,
        };
        var uri = "Spaces/Update";
        using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
        using var request = new HttpRequestMessage(HttpMethod.Post, uri)
        {
            Content = content,
        };
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.SendAsync(request);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task Action_ReturnsBadRequest_WhenModelStateIsInvalid()
    {
        // Arrange
        var input = new UpdateSpaceInputModel();
        var uri = "Spaces/Update";
        using var content = FormUrlEncodedContentBuilder.BuildContent(input);
        using var request = new HttpRequestMessage(HttpMethod.Post, uri)
        {
            Content = content,
        };
        using var client = Factory.CreateHttpsClient(options => options.AllowAutoRedirect = false);

        // Act
        using var response = await client.SendAsync(request);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }

    [Fact]
    public async Task Action_ReturnsNotFound_WhenNoMatchingEntityIsFound()
    {
        // Arrange
        var inputModel = new UpdateSpaceInputModel
        {
            Id = int.MaxValue,
            LevelId = 1,
            Number = int.MaxValue,
            Status = SpaceStatus.Occupied,
        };
        var uri = "Spaces/Update";
        using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
        using var request = new HttpRequestMessage(HttpMethod.Post, uri)
        {
            Content = content,
        };
        using var client = Factory.CreateHttpsClient(options => options.AllowAutoRedirect = false);

        // Act
        using var response = await client.SendAsync(request);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }
}
