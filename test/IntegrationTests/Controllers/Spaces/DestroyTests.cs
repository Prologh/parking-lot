﻿using FluentAssertions;
using ParkingLot.Application.Spaces.InputModels;
using ParkingLot.Domain.Spaces;
using ParkingLot.Infrastructure.Http.Content.Builders;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace ParkingLot.IntegrationTests.Controllers.Spaces;

public class DestroyTests : ParkingLotWebAppFixtureBase
{
    public DestroyTests(ParkingLotWebAppFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Action_DeletesExistingEntity_WhenModelStateIsValid()
    {
        int spaceId;

        // Create unoccupied space
        {
            // Arrange
            var inputModel = new CreateSpaceInputModel
            {
                LevelId = 1,
                Number = new Random().Next(50000, 100000),
                Status = SpaceStatus.Unoccupied,
            };
            var uri = "Spaces/Create";
            using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
            using var request = new HttpRequestMessage(HttpMethod.Post, uri)
            {
                Content = content,
            };
            using var client = Factory.CreateHttpsClient();

            // Act
            using var response = await client.SendAsync(request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            spaceId = int.Parse(response.RequestMessage.RequestUri.AbsoluteUri.Split('/').Last());
        }

        // Destroy space
        {
            // Arrange
            var inputModel = new DeleteSpaceInputModel { Id = spaceId };
            var uri = "Spaces/Destroy";
            using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
            using var request = new HttpRequestMessage(HttpMethod.Post, uri)
            {
                Content = content,
            };
            using var client = Factory.CreateHttpsClient(options => options.AllowAutoRedirect = false);

            // Act
            using var response = await client.SendAsync(request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
        }
    }

    [Fact]
    public async Task Action_ReturnsNotFound_WhenModelIsNotFound()
    {
        // Arrange
        var inputModel = new DeleteSpaceInputModel { Id = 0 };
        var uri = "Spaces/Destroy";
        using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
        using var request = new HttpRequestMessage(HttpMethod.Post, uri)
        {
            Content = content,
        };
        using var client = Factory.CreateHttpsClient(options => options.AllowAutoRedirect = false);

        // Act
        using var response = await client.SendAsync(request);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }

    [Fact]
    public async Task Action_ReturnsBadRequest_WhenSpaceIsOccupied()
    {
        int spaceId;

        // Create occupied space
        {
            // Arrange
            var inputModel = new CreateSpaceInputModel
            {
                LevelId = 1,
                Number = new Random().Next(50000, 100000),
                Status = SpaceStatus.Occupied,
            };
            var uri = "Spaces/Create";
            using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
            using var request = new HttpRequestMessage(HttpMethod.Post, uri)
            {
                Content = content,
            };
            using var client = Factory.CreateHttpsClient();

            // Act
            using var response = await client.SendAsync(request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            spaceId = int.Parse(response.RequestMessage.RequestUri.AbsoluteUri.Split('/').Last());
        }

        // Destroy space
        {
            // Arrange
            var inputModel = new DeleteSpaceInputModel { Id = spaceId };
            var uri = "Spaces/Destroy";
            using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
            using var request = new HttpRequestMessage(HttpMethod.Post, uri)
            {
                Content = content,
            };
            using var client = Factory.CreateHttpsClient(options => options.AllowAutoRedirect = false);

            // Act
            using var response = await client.SendAsync(request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
    }

    [Fact]
    public async Task Action_ReturnsBadRequest_WhenModelStateIsInvalid()
    {
        // Arrange
        var inputModel = new DeleteSpaceInputModel();
        var uri = "Spaces/Destroy";
        using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
        using var request = new HttpRequestMessage(HttpMethod.Post, uri)
        {
            Content = content,
        };
        using var client = Factory.CreateHttpsClient(options => options.AllowAutoRedirect = false);

        // Act
        using var response = await client.SendAsync(request);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }
}
