﻿using FluentAssertions;
using ParkingLot.Application.Spaces.InputModels;
using ParkingLot.Domain.Spaces;
using ParkingLot.Infrastructure.Http.Content.Builders;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace ParkingLot.IntegrationTests.Controllers.Spaces;

public class CreateTests : ParkingLotWebAppFixtureBase
{
    public CreateTests(ParkingLotWebAppFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Action_CreatesNewEntity_WhenModelStateIsValid()
    {
        // Arrange
        var inputModel = new CreateSpaceInputModel
        {
            LevelId = 1,
            Number = new Random().Next(50000, 100000),
            Status = SpaceStatus.Unoccupied,
        };
        var uri = "Spaces/Create";
        using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
        using var request = new HttpRequestMessage(HttpMethod.Post, uri)
        {
            Content = content,
        };
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.SendAsync(request);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task Action_ReturnsBadRequest_WhenModelStateIsInvalid()
    {
        // Arrange
        var inputModel = new CreateSpaceInputModel();
        var uri = "Spaces/Create";
        using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
        using var request = new HttpRequestMessage(HttpMethod.Post, uri)
        {
            Content = content,
        };
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.SendAsync(request);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }
}
