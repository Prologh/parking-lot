﻿using FluentAssertions;
using ParkingLot.Application.Logging.InputModels;
using ParkingLot.Infrastructure.Http.Content.Builders;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace ParkingLot.IntegrationTests.Controllers.Logging;

public class DestroyTests : ParkingLotWebAppFixtureBase
{
    public DestroyTests(ParkingLotWebAppFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Action_DeletesExistingEntity_WhenModelStateIsValid()
    {
        // Arrange
        var inputModel = new DeleteRequestLogInputModel { Id = 1 };
        var uri = "Logging/Destroy";
        using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
        using var request = new HttpRequestMessage(HttpMethod.Post, uri)
        {
            Content = content,
        };
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.SendAsync(request);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task Action_ReturnsBadRequest_WhenModelStateIsInvalid()
    {
        // Arrange
        var ipnutModel = new DeleteRequestLogInputModel();
        var uri = "Logging/Destroy";
        using var content = FormUrlEncodedContentBuilder.BuildContent(ipnutModel);
        using var request = new HttpRequestMessage(HttpMethod.Post, uri)
        {
            Content = content,
        };
        using var client = Factory.CreateHttpsClient(options => options.AllowAutoRedirect = false);

        // Act
        using var response = await client.SendAsync(request);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }
}
