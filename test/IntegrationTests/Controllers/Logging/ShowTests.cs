﻿using FluentAssertions;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace ParkingLot.IntegrationTests.Controllers.Logging;

public class ShowTests : ParkingLotWebAppFixtureBase
{
    public ShowTests(ParkingLotWebAppFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Action_ReturnsSuccessfulStatusCode_WhenMatchingRequestLogIsFound()
    {
        // Arrange
        var id = 1;
        var uri = $"Logging/{id}";
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.GetAsync(uri);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task Action_ReturnsNotFound_WhenNoMatchingRequestLogIsFound()
    {
        // Arrange
        var id = 0;
        var uri = $"Logging/{id}";
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.GetAsync(uri);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }
}
