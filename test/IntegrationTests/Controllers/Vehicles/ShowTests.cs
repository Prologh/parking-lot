﻿using FluentAssertions;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace ParkingLot.IntegrationTests.Controllers.Vehicles;

public class ShowTests : ParkingLotWebAppFixtureBase
{
    public ShowTests(ParkingLotWebAppFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Action_ReturnsSuccessfulStatusCode_WhenMatchingEntityIsFound()
    {
        // Arrange
        var vehicleId = 1;
        var uri = $"Vehicles/{vehicleId}";
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.GetAsync(uri);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task Action_ReturnsNotFound_WhenNoMatchingEntityIsFound()
    {
        // Arrange
        var vehicleId = 0;
        var uri = $"Vehicles/{vehicleId}";
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.GetAsync(uri);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }
}
