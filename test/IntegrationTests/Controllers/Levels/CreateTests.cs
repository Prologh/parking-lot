﻿using FluentAssertions;
using ParkingLot.Application.Levels.InputModels;
using ParkingLot.Infrastructure.Http.Content.Builders;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace ParkingLot.IntegrationTests.Controllers.Levels;

public class CreateTests : ParkingLotWebAppFixtureBase
{
    public CreateTests(ParkingLotWebAppFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Action_CreatesNewEntity_WhenModelStateIsValid()
    {
        // Arrange
        var inputModel = new CreateLevelInputModel { Position = 4, SpacesAmount = 2, };
        var uri = "Levels/Create";
        using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
        using var request = new HttpRequestMessage(HttpMethod.Post, uri)
        {
            Content = content,
        };
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.SendAsync(request);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task Action_ReturnsBadRequest_WhenModelStateIsInvalid()
    {
        // Arrange
        var inputModel = new CreateLevelInputModel { SpacesAmount = -1 };
        var uri = "Levels/Create";
        using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
        using var request = new HttpRequestMessage(HttpMethod.Post, uri)
        {
            Content = content,
        };
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.SendAsync(request);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }
}
