﻿using FluentAssertions;
using ParkingLot.Application.Levels.InputModels;
using ParkingLot.Application.Spaces.InputModels;
using ParkingLot.Domain.Spaces;
using ParkingLot.Infrastructure.Http.Content.Builders;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace ParkingLot.IntegrationTests.Controllers.Levels;

public class DestroyTests : ParkingLotWebAppFixtureBase
{
    public DestroyTests(ParkingLotWebAppFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Action_DeletesExistingEntity_WhenModelStateIsValid()
    {
        int createdLevelId;

        // Create
        {
            // Arrange
            var inputModel = new CreateLevelInputModel { Position = 4 };
            var uri = "Levels/Create";
            using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
            using var request = new HttpRequestMessage(HttpMethod.Post, uri)
            {
                Content = content,
            };
            using var client = Factory.CreateHttpsClient();

            // Act
            using var response = await client.SendAsync(request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            createdLevelId = int.Parse(response.RequestMessage.RequestUri.AbsoluteUri.Split('/').Last());
        }

        // Delete
        {
            // Arrange
            var inputModel = new DeleteLevelInputModel { Id = createdLevelId };
            var uri = "Levels/Destroy";
            using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
            using var request = new HttpRequestMessage(HttpMethod.Post, uri)
            {
                Content = content,
            };
            using var client = Factory.CreateHttpsClient();

            // Act
            using var response = await client.SendAsync(request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
    }

    [Fact]
    public async Task Action_ReturnsBadRequest_WhenLevelHaveAtLeastOneOccupiedSpace()
    {
        int createdLevelId;

        // Create level
        {
            // Arrange
            var inputModel = new CreateLevelInputModel { Position = 4 };
            var uri = "Levels/Create";
            using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
            using var request = new HttpRequestMessage(HttpMethod.Post, uri)
            {
                Content = content,
            };
            using var client = Factory.CreateHttpsClient();

            // Act
            using var response = await client.SendAsync(request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            createdLevelId = int.Parse(response.RequestMessage.RequestUri.AbsoluteUri.Split('/').Last());
        }

        // Add occupied space
        {
            // Arrange
            var inputModel = new UpdateSpaceInputModel
            {
                IsForHandicapped = false,
                LevelId = createdLevelId,
                Number = new Random().Next(50000, 100000),
                Status = SpaceStatus.Occupied,
            };
            var uri = "Spaces/Create";
            using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
            using var request = new HttpRequestMessage(HttpMethod.Post, uri)
            {
                Content = content,
            };
            using var client = Factory.CreateHttpsClient();

            // Act
            using var response = await client.SendAsync(request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        // Delete level
        {
            // Arrange
            var inputModel = new DeleteLevelInputModel { Id = createdLevelId };
            var uri = "Levels/Destroy";
            using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
            using var request = new HttpRequestMessage(HttpMethod.Post, uri)
            {
                Content = content,
            };
            using var client = Factory.CreateHttpsClient();

            // Act
            using var response = await client.SendAsync(request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
    }

    [Fact]
    public async Task Action_ReturnsNotFound_WhenModelIsNotFound()
    {
        // Arrange
        var inputModel = new DeleteLevelInputModel { Id = int.MaxValue };
        var uri = "Levels/Destroy";
        using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
        using var request = new HttpRequestMessage(HttpMethod.Post, uri)
        {
            Content = content,
        };
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.SendAsync(request);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }

    [Fact]
    public async Task Action_ReturnsBadRequest_WhenModelStateIsInvalid()
    {
        // Arrange
        var inputModel = new DeleteLevelInputModel();
        var uri = "Levels/Destroy";
        using var content = FormUrlEncodedContentBuilder.BuildContent(inputModel);
        using var request = new HttpRequestMessage(HttpMethod.Post, uri)
        {
            Content = content,
        };
        using var client = Factory.CreateHttpsClient(options => options.AllowAutoRedirect = false);

        // Act
        using var response = await client.SendAsync(request);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }
}
