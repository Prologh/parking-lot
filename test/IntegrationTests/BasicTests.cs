﻿using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using ParkingLot.Persistance;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace ParkingLot.IntegrationTests;

public class BasicTests : ParkingLotWebAppFixtureBase
{
    public BasicTests(ParkingLotWebAppFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Get_EndpointsAcceptHttpV2()
    {
        // Arrange
        var uri = string.Empty;
        using var client = Factory.CreateHttpsClient();
        using var request = new HttpRequestMessage(HttpMethod.Get, uri)
        {
            Version = new Version(2, 0),
        };

        // Act
        using var response = await client.SendAsync(request);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task Get_EndpointsRedirectHttpRequests()
    {
        // Arrange
        var uri = string.Empty;
        using var client = Factory.CreateDefaultClient();

        // Act
        using var response = await client.GetAsync(uri);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.TemporaryRedirect);
        response.Headers.Location.Scheme.Should().Be(Uri.UriSchemeHttps);
    }

    [Fact]
    public async Task Get_EndpointsReturnCorrectCharset()
    {
        // Arrange
        var uri = string.Empty;
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.GetAsync(uri);

        // Assert
        response.Content.Headers.ContentType.ToString().Should().Contain("charset=utf-8");
    }

    [Fact]
    public async Task Get_EndpointsReturnCorrectContentType()
    {
        // Arrange
        var uri = string.Empty;
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.GetAsync(uri);

        // Assert
        response.Content.Headers.ContentType.ToString().Should().Contain("text/html");
    }

    [Fact]
    public async Task Get_EndpointsReturnSuccessCode()
    {
        // Arrange
        var uri = string.Empty;
        using var client = Factory.CreateHttpsClient();

        // Act
        using var response = await client.GetAsync(uri);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task Application_Can_Connect_With_Database()
    {
        // Arrange
        using var scope = Factory.Services.CreateScope();
        var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

        // Act
        var canConnect = await context.Database.CanConnectAsync();

        // Assert
        canConnect.Should().BeTrue();
    }

    [Fact]
    public void Application_Uses_In_Memory_Database_Provider_For_Integration_Tests()
    {
        // Arrange
        using var scope = Factory.Services.CreateScope();
        var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

        // Act
        var databaseProviderName = context.Database.ProviderName;

        // Assert
        databaseProviderName.Should().Be("Microsoft.EntityFrameworkCore.InMemory");
    }
}
